include(ExternalProject)
MESSAGE ("Installing Boost libraries!")

SET (Boost_Bootstrap_Command ./bootstrap.sh)
SET (Boost_b2_Command ./b2)
SET (Boost_URL "http://dl.bintray.com/boostorg/release/1.69.0/source/boost_1_69_0.tar.gz")
SET (HASH 9a2c2819310839ea373f42d69e733c339b4e9a19deab6bfec448281554aa4dbb)

ExternalProject_Add(boost
    URL ${Boost_URL}
    DOWNLOAD_DIR ${CMAKE_BINARY_DIR}/downloaded
    BUILD_IN_SOURCE 1
    URL_HASH_SHA256=${HASH}}
    UPDATE_COMMAND ""
    PATCH_COMMAND ""
    CONFIGURE_COMMAND ${Boost_Bootstrap_Command}
    BUILD_COMMAND ${Boost_b2_Command} install
    --with-thread
    --with-program_options
    --prefix=${CMAKE_BINARY_DIR}/INSTALL
    --threading=single,multi
    --link=shared
    --variant=release
    -j8
    INSTALL_COMMAND ""
    INSTALL_DIR ${CMAKE_BINARY_DIR}/INSTALL
)

SET (Boost_INCLUDE ${CMAKE_BINARY_DIR}/INSTALL/include)
SET (Boost_LIBRARY ${CMAKE_BINARY_DIR}/INSTALL/lib)
