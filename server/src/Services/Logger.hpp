#ifndef SERVICES_LOGGER_HPP
#define SERVICES_LOGGER_HPP

#include <Logging/Logger.hpp>

namespace androlecture
{
namespace services
{
    using Logger = BaseLogger<EComponent::EComponent_Services>;
}  // namespace services
}  // namespace androlecture

#endif  // SERVICES_LOGGER_HPP