
#include "Translation.hpp"
#include <functional>

#include <Protocol/Protocol.hpp>

namespace androlecture
{
namespace services
{

using namespace protocol;

Translation::Translation(const unsigned id, const std::string language, const std::string description,
         const unsigned char codec, const unsigned char samplingRate, const unsigned char voicePacketLength, 
         const unsigned timeToSendStatistics, const unsigned ipAddress)
: 
id_(std::move(id)),
language_(std::move(language)),
description_(std::move(description)), 
codec_(std::move(codec)),
samplingRate_(std::move(samplingRate)),
voicePacketLength_(std::move(voicePacketLength)),
timeToSendStatistics_(std::move(timeToSendStatistics)),
ipGroupAddress_(std::move(ipAddress)),
destinationPort_(udpServer_.getDestinationPort()),
receivePort_(RECEIVE_UDP_PORT),
udpServer_(*this)
{}

const unsigned& Translation::getId() const
{
    return id_;
}

const std::string& Translation::getLanguage() const
{
    return language_;
}

const std::string& Translation::getDescription() const
{
    return description_;
}

const unsigned char& Translation::getCodec() const
{
    return codec_;
}

const unsigned char& Translation::getSamplingRate() const
{
    return samplingRate_;
}

const unsigned char& Translation::getVoicePacketLength() const
{
    return voicePacketLength_;
}

const unsigned& Translation::getTimeToSendStatistics() const
{
    return timeToSendStatistics_;
}

const unsigned& Translation::getIpGroupAddress() const
{
    return ipGroupAddress_;
}

const unsigned short& Translation::getDestinationPort() const
{
    return destinationPort_;
}

const unsigned short& Translation::getReceivePort() const
{
    return receivePort_;
}

const TranslationInfoForUsers Translation::getTranslationInfo() const
{
    return {
        id_, language_, description_, codec_, samplingRate_, voicePacketLength_,
        timeToSendStatistics_, ipGroupAddress_, receivePort_
    };
}

bool operator<(const Translation& lhs, const Translation& rhs)
{
    return lhs.getId() < rhs.getId();
}

bool operator<(const TranslationKey& lhs, const Translation& rhs)
{
    return lhs.id_ < rhs.getId();
}

bool operator<(const Translation& lhs, const TranslationKey& rhs)
{
    return lhs.getId() < rhs.id_;
}

}  // namespace services
}  // namespace androlecture
