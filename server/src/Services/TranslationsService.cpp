
#include "TranslationsService.hpp"
#include "Logger.hpp"
#include <algorithm>

namespace androlecture
{
namespace services
{

using namespace server;

namespace
{
    Logger logger{"TranslationsService"};
}


TranslationsService& TranslationsService::getInstance()
{
    static TranslationsService instance;
    return instance;
}

const boost::optional<const Translation&> TranslationsService::addTranslation(const TranslationInfo translationInfo,
    TcpSession& tcpSession)
{
    std::lock_guard<std::mutex> lock(mu_);

    if (tcpSession.getTranslation())
    {
        return boost::none;
    }

    const auto& translation = tcpSession.setTranslation(idAssigner_.getId(), std::move(translationInfo.language_),
        std::move(translationInfo.description_), std::move(translationInfo.codec_), std::move(translationInfo.samplingRate_),
        std::move(translationInfo.voicePacketLength_), std::move(translationInfo.timeToSendStatistics_),
        ipGroupAddressAssigner_.getIpGroupAddress().getDecimalAddress());

    auto addingResult = translations_.insert(translation);
    if (addingResult.second)
    {
        return addingResult.first->get();
    }
    return boost::none;
}

void TranslationsService::deleteTranslation(const Translation& translation)
{
    std::lock_guard<std::mutex> lock(mu_);
    const auto&& id = std::move(translation.getId());
    const auto&& ipGroupAddress = std::move(translation.getIpGroupAddress());
    translations_.erase(translation);
    idAssigner_.addFreeId(id);
    ipGroupAddressAssigner_.addFreeIpGroupAddress(ipGroupAddress);
    logger << info << "Deleted translation!";
}

const std::vector<TranslationInfoForUsers> TranslationsService::getTranslationsInfo() const
{
    std::lock_guard<std::mutex> lock(mu_);
    std::vector<TranslationInfoForUsers> transInfos;
    transInfos.reserve(translations_.size());
    std::transform(translations_.begin(), translations_.end(), std::back_inserter(transInfos), [](const auto& translation)
    {
        return translation.get().getTranslationInfo();
    });
    return transInfos;
}

bool TranslationsService::doesTranslationExists(const TranslationKey& translationKey) const
{
    std::lock_guard<std::mutex> lock(mu_);
    return translations_.find(translationKey) != translations_.cend();
}

}  // namespace services
}  // namespace androlecture

