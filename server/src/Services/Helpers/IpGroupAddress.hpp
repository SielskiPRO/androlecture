#ifndef SERVICES_HELPERS_IPGROUPADDRESS_HPP
#define SERVICES_HELPERS_IPGROUPADDRESS_HPP

#include <vector>
#include <string>

namespace androlecture
{
namespace services
{
namespace helpers
{

class IpGroupAddress
{
public:
    IpGroupAddress();
    IpGroupAddress(const unsigned decimalAddress);

    IpGroupAddress operator++();
    IpGroupAddress operator++(int);

    std::string toString() const;
    const unsigned& getDecimalAddress() const;

private:
    unsigned decimalAddress_;
};

}  // namespace helpers
}  // namespace services
}  // namespace androlecture


#endif  // SERVICES_HELPERS_IPGROUPADDRESS_HPP