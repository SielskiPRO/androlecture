
#include "IpGroupAddress.hpp"
#include <sstream>
#include <algorithm>
#include <iterator>
#include <functional>

namespace androlecture
{
namespace services
{
namespace helpers
{

IpGroupAddress::IpGroupAddress()
: decimalAddress_(4009754624)
{}

IpGroupAddress::IpGroupAddress(const unsigned decimal)
: decimalAddress_(std::move(decimal))
{}

IpGroupAddress IpGroupAddress::operator++()
{
    ++decimalAddress_;
    return *this;
}

IpGroupAddress IpGroupAddress::operator++(int)
{
    const IpGroupAddress old(*this);
    ++(*this);
    return old;
}


std::string IpGroupAddress::toString() const
{
    std::vector<unsigned char> ipAddress = {
        (unsigned char)((decimalAddress_) & 0xFF),
        (unsigned char)((decimalAddress_ >> 8) & 0xFF),
        (unsigned char)((decimalAddress_ >> 16) & 0xFF),
        (unsigned char)((decimalAddress_ >> 24) & 0xFF)
    };

    std::ostringstream oss;
    std::copy(ipAddress.rbegin(), ipAddress.rend(), std::ostream_iterator<unsigned>(oss, "."));
    std::string stringAddress = oss.str();
    stringAddress.pop_back();
    return stringAddress;
}

const unsigned& IpGroupAddress::getDecimalAddress() const
{
    return decimalAddress_;
}

}  // namespace helpers
}  // namespace services
}  // namespace androlecture

