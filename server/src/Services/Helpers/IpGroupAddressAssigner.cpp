
#include "IpGroupAddressAssigner.hpp"

namespace androlecture
{
namespace services
{
namespace helpers
{


IpGroupAddress IpGroupAddressAssigner::getIpGroupAddress()
{
    if (!freeGroupAddresses_.empty())
    {
        const auto ipGroupAddress = std::move(freeGroupAddresses_.back());
        freeGroupAddresses_.pop_back();
        return ipGroupAddress;
    }
    return ipGroupaddress_++;
}

void IpGroupAddressAssigner::addFreeIpGroupAddress(const IpGroupAddress ipGroupAddress)
{
    freeGroupAddresses_.push_back(std::move(ipGroupAddress));
}

bool IpGroupAddressAssigner::isAnyIpGroupAddressAvailable() const
{
    if (ipGroupaddress_.getDecimalAddress() == MAX_DECIMAL_IP_GROUP_ADDRESS)
    {
        return false;
    }
    return true;
}

    // std::vector<IpGroupAddress> freedGroupAddresses_;
    // IpGroupAddress address_;

}  // namespace helpers
}  // namespace services
}  // namespace androlecture
