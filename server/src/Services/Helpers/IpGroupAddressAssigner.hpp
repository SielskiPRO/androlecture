#ifndef SERVICES_HELPERS_IPGROUPADDRESSASSIGNER_HPP
#define SERVICES_HELPERS_IPGROUPADDRESSASSIGNER_HPP

#include <vector>

#include <Services/Translation.hpp>

#include "IpGroupAddress.hpp"

namespace androlecture
{
namespace services
{
namespace helpers
{

class IpGroupAddressAssigner
{
public:
    static constexpr unsigned MAX_DECIMAL_IP_GROUP_ADDRESS = 4026531839U;
    
    IpGroupAddress getIpGroupAddress();
    void addFreeIpGroupAddress(const IpGroupAddress ipGroupAddress);


private:
    bool isAnyIpGroupAddressAvailable() const;

    std::vector<IpGroupAddress> freeGroupAddresses_;
    IpGroupAddress ipGroupaddress_;
};

}  // namespace helpers
}  // namespace services
}  // namespace androlecture


#endif  // SERVICES_HELPERS_IPGROUPADDRESSASSIGNER_HPP