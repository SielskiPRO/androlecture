#ifndef SERVICES_TRANSLATIONSERVICE_HPP
#define SERVICES_TRANSLATIONSERVICE_HPP

#include <string>
#include <set>
#include <vector>
#include <mutex>
#include <functional>

#include <boost/optional.hpp>

#include <Services/Helpers/IpGroupAddressAssigner.hpp>

#include <Server/TcpSession.hpp>

#include <Common/Utils/IdAssigner.hpp>

#include "Translation.hpp"

namespace androlecture
{
namespace services
{

class TranslationsService
{
public:
    
    static TranslationsService& getInstance();

    const boost::optional<const Translation&> addTranslation(const TranslationInfo translationInfo,
        server::TcpSession& tcpSession);
    void deleteTranslation(const Translation& translation);
    const std::vector<TranslationInfoForUsers> getTranslationsInfo() const;
    bool doesTranslationExists(const TranslationKey& translationKey) const;

private:
    TranslationsService() = default;
    ~TranslationsService() = default;
    TranslationsService(const TranslationsService&) = delete;
    TranslationsService& operator=(const TranslationsService&) = delete;

    mutable std::mutex mu_;
    std::set<std::reference_wrapper<const Translation>, std::less<>> translations_;
    common::utils::IdAssigner idAssigner_;
    helpers::IpGroupAddressAssigner ipGroupAddressAssigner_;
};

}  // namespace services
}  // namespace androlecture

#endif  // SERVICES_TRANSLATIONSERVICE_HPP