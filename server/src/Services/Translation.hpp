#ifndef SERVICES_TRANSLATION_HPP
#define SERVICES_TRANSLATION_HPP

#include <string>
#include <Server/UdpServer.hpp>

namespace androlecture
{
namespace services
{

struct TranslationInfo
{
    std::string language_;
    std::string description_;
    unsigned char codec_;
    unsigned char samplingRate_;
    unsigned char voicePacketLength_; 
    unsigned int timeToSendStatistics_;
};

struct TranslationInfoForUsers
{
    unsigned id_;
    std::string language_;
    std::string description_;
    unsigned char codec_;
    unsigned char samplingRate_;
    unsigned char voicePacketLength_; 
    unsigned timeToSendStatistics_;
    unsigned ipGroupAddress_;
    unsigned short receivePort_;
};

struct TranslationKey
{
    unsigned id_;
};

class Translation
{
public:
    Translation(const unsigned id, const std::string language, const std::string description,
         const unsigned char codec, const unsigned char samplingRate, const unsigned char voicePacketLength, 
         const unsigned timeToSendStatistics, const unsigned ipAddress);

    const unsigned& getId() const;
    const std::string& getLanguage() const;
    const std::string& getDescription() const;
    const unsigned char& getCodec() const;
    const unsigned char& getSamplingRate() const;
    const unsigned char& getVoicePacketLength() const;
    const unsigned& getTimeToSendStatistics() const;
    const unsigned& getIpGroupAddress() const;
    const unsigned short& getDestinationPort() const;
    const unsigned short& getReceivePort() const;

    friend bool operator<(const Translation& lhs, const Translation& rhs);
    friend bool operator<(const TranslationKey& lhs, const Translation& rhs);
    friend bool operator<(const Translation& lhs, const TranslationKey& rhs);

    const TranslationInfoForUsers getTranslationInfo() const;

private:
    unsigned id_;
    std::string language_;
    std::string description_;
    unsigned char codec_;
    unsigned char samplingRate_;
    unsigned char voicePacketLength_; 
    unsigned timeToSendStatistics_;
    unsigned ipGroupAddress_;
    const unsigned short& destinationPort_;
    unsigned short receivePort_;
    server::UdpServer udpServer_;
    
};

bool operator<(const Translation& lhs, const Translation& rhs);
bool operator<(const TranslationKey& lhs, const Translation& rhs);
bool operator<(const Translation& lhs, const TranslationKey& rhs);

}  // namespace services
}  // namespace androlecture

#endif  // SERVICES_TRANSLATION_HPP