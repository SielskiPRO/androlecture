#ifndef COMMON_LOGGER_HPP
#define COMMON_LOGGER_HPP

#include <Logging/Logger.hpp>

namespace androlecture
{
namespace common
{
    using Logger = BaseLogger<EComponent::EComponent_Common>;
}  // namespace common
}  // namespace androlecture

#endif  // COMMON_LOGGER_HPP