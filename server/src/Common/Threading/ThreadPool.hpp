#ifndef COMMON_THREADING_THREADPOOL_HPP
#define COMMON_THREADING_THREADPOOL_HPP

#include <thread>
#include <vector>
#include <condition_variable>
#include <functional>
#include <queue>
#include <future>
#include <memory>

#include <Common/Logger.hpp>

namespace androlecture
{
namespace common
{
namespace threading
{

class ThreadPool
{
using Task = std::function<void()>;
public:
    ~ThreadPool();
    static ThreadPool& getInstance()
    {
        static ThreadPool instance;
        return instance;
    }
    template <typename T>
    auto add_task(T task) -> std::future<decltype(task())>;

private:
    ThreadPool();
    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    void stop();
    bool stopping_;
    unsigned freeThreadsNum_;
    Task threadFun_;
    std::mutex eventMutex_;
    std::condition_variable event_;
    std::queue<Task> tasks_;
    std::vector<std::thread> threads_;
    Logger logger_;
};

template <typename T>
auto ThreadPool::add_task(T task) -> std::future<decltype(task())>
{
    auto wrapper = 
    std::make_shared<std::packaged_task<decltype(task()) ()>>(std::move(task));

    {
        std::unique_lock<std::mutex> lock(eventMutex_);
        tasks_.emplace([=]{
            (*wrapper)();
        });
        if (!freeThreadsNum_)
            threads_.emplace_back(threadFun_);
    }
    event_.notify_one();
    return wrapper->get_future();
}

}  // namespace threading
}  // namespace common
}  // namespace androlecture

#endif  // COMMON_THREADING_THREADPOOL_HPP