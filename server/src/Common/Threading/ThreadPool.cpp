#include "ThreadPool.hpp"

namespace androlecture
{
namespace common
{
namespace threading
{

ThreadPool::ThreadPool()
: stopping_(false), freeThreadsNum_(0u), logger_("ThreadPool")
{
    threadFun_ = [=](){
        while(true)
        {
            Task task;
            {
                std::unique_lock<std::mutex> lock(eventMutex_);
                ++freeThreadsNum_;
                event_.wait(lock, [=]{ return stopping_ || !tasks_.empty(); });
                --freeThreadsNum_;
                if (stopping_ && tasks_.empty())
                    break;
                task = std::move(tasks_.front());
                tasks_.pop();
            }
            task();
        }
    };
}

ThreadPool::~ThreadPool()
{
    stop();
}

void ThreadPool::stop() 
{
    {
        std::lock_guard<std::mutex> lock(eventMutex_);
        stopping_ = true;
    }
    event_.notify_all();
    for (auto& thread : threads_)
        thread.join();
}

}  // namespace threading
}  // namespace common
}  // namespace androlecture
