

#include "IdAssigner.hpp"

namespace androlecture
{
namespace common
{
namespace utils
{

IdAssigner::IdAssigner()
: currentId_(0),
freeIds_({})
{}

void IdAssigner::addFreeId(unsigned id)
{
    freeIds_.push_back(std::move(id));
}

unsigned IdAssigner::getId()
{
    if (!freeIds_.empty())
    {
        const auto id = std::move(freeIds_.back());
        freeIds_.pop_back();
        return id;
    }
    return currentId_++;
}


}  // namespace utils
}  // namespace common
}  // namespace androlecture
