#ifndef COMMON_UTILS_ENCODING_HPP
#define COMMON_UTILS_ENCODING_HPP

#include <iostream>
#include <cstring>
#include <memory>
#include <utility>
#include <string>

#include <Services/Translation.hpp>

namespace androlecture
{
namespace common
{
namespace utils
{

template <typename T>
void encode(unsigned char* buffer, unsigned& position, const T& value)
{
    memcpy(buffer + position, &value, sizeof(value));
    position += sizeof(value);
}

template<typename Last>
void encodeValues(unsigned char* buffer, unsigned& position, const Last& value)
{
    encode<Last>(buffer, position, value);
}

template <typename First, typename ...Tail>
void encodeValues(unsigned char* buffer, unsigned& position ,const First& first, const Tail& ...tail)
{
    encode<First>(buffer, position, first);
    encodeValues(buffer, position, tail...);
}

template <typename ...Params>
auto encodeValues(const unsigned bufferSize, const Params& ...args)
{
    auto buffer = std::make_unique<unsigned char[]>(bufferSize);
    unsigned position = 0;
    encodeValues(buffer.get(), position, args...);
    return buffer;
}

template<>
inline void encode(unsigned char* buffer, unsigned& position, const std::string& value)
{
    const auto stringLength = value.length();
    memcpy(buffer + position, value.c_str(), stringLength);
    position += stringLength;
}

template<>
inline void encode(unsigned char* buffer, unsigned& position, const bool& value)
{
    std::cout << "Serializing boolean value" << std::endl;
    *(buffer + position) = static_cast<unsigned char>(value);
    position += 1;
}

template<>
inline void encode(unsigned char* buffer, unsigned& position, const services::TranslationInfoForUsers& value)
{
    encodeValues(buffer, position, 
        value.id_,
        static_cast<uint8_t>(value.language_.length()),
        static_cast<uint8_t>(value.description_.length()),
        value.language_,
        value.description_,
        value.codec_,
        value.samplingRate_,
        value.voicePacketLength_,
        value.timeToSendStatistics_,
        value.ipGroupAddress_,
        value.receivePort_
    );
}

}  // namespace utils
}  // namespace common
}  // namespace androlecture

#endif  // COMMON_UTILS_ENCODING_HPP
