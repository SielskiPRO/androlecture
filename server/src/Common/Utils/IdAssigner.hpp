#ifndef COMMON_UTILS_IDASSIGNER_HPP
#define COMMON_UTILS_IDASSIGNER_HPP

#include <vector>

namespace androlecture
{
namespace common
{
namespace utils
{

class IdAssigner
{
public:
    IdAssigner();
    unsigned getId();
    void addFreeId(unsigned id);
private:
    unsigned currentId_;
    std::vector<unsigned> freeIds_;
};

}  // namespace utils
}  // namespace common
}  // namespace androlecture


#endif  // COMMON_UTILS_IDASSIGNER_HPP