#ifndef COMMON_UTILS_STATISTICENTRY_HPP
#define COMMON_UTILS_STATISTICENTRY_HPP

#include <iostream>
#include <string>

#include "CsvFileWriter.hpp"

class CsvFileWriter;

namespace androlecture
{
namespace common
{
namespace utils
{

struct StatisticEntry
{
    unsigned int numOfEntry_;
    int wifiSignalStrength_;

    std::string avgLatency_;
    unsigned int maxLatency_;
    unsigned int minLatency_;
    unsigned int temporalLatency_;
    
    std::string avgJitter_;
    unsigned int maxJitter_;
    unsigned int minJitter_;
    unsigned int temporalJitter_;

    unsigned int packetLoss_;
    unsigned int packetDrop_;

    friend std::ostream& operator << (std::ostream& os, const StatisticEntry& entry);
};

inline std::ostream& operator << (std::ostream& os, const StatisticEntry& entry)
{
    os << entry.numOfEntry_ << " " << entry.wifiSignalStrength_ << " " << entry.avgLatency_ << " " << entry.maxLatency_ << " " << entry.minLatency_ << " "
    << entry.temporalLatency_ << " "  << entry.avgJitter_ << " "  << entry.maxJitter_ << " "  << entry.minJitter_ << " "
    << entry.temporalJitter_ << " "   << entry.packetLoss_ << " " << entry.packetDrop_ << " " << std::endl;
    return os; 
}

}  // namespace utils
}  // namespace common
}  // namespace androlecture


#endif  // COMMON_UTILS_STATISTICENTRY_HPP
