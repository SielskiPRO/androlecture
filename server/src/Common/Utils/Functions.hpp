#ifndef COMMON_UTILS_FUNCTIONS_HPP
#define COMMON_UTILS_FUNCTIONS_HPP

#include <utility>
#include <string>
#include <iostream>

#include <Services/Translation.hpp>
#include <Protocol/Protocol.hpp>

namespace androlecture
{
namespace common
{
namespace utils
{

template <typename T>
auto sum(const T& value)
{
    return sizeof(value);   
}

template <typename Last>
auto sumBytes(const Last& last)
{
    return sum<Last>(last);
}

template <typename First, typename ...Tail>
auto sumBytes(const First& first, const Tail& ...tail)
{
    return sum<First>(first) + sumBytes(tail...);
}

template<>
inline auto sum(const std::string& value)
{
    return value.length();
}

template<>
inline auto sum(const bool& value)
{
    std::cout << "Summing boolean!" << std::endl;
    return 1u;
}

template<>
inline auto sum(const services::TranslationInfoForUsers& value)
{
    return sumBytes(value.id_, value.language_,
        value.description_,
        protocol::TRANSLATION_INFO_PADDING, value.codec_, value.samplingRate_,
        value.voicePacketLength_, value.timeToSendStatistics_, value.ipGroupAddress_, value.receivePort_
    );
}

}
}
}

#endif  // COMMON_UTILS_FUNCTIONS_HPP