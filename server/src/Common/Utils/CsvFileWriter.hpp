#ifndef COMMON_UTILS_CSVFILEWRITER_HPP
#define COMMON_UTILS_CSVFILEWRITER_HPP

#include <fstream>
#include <string>
#include <vector>
#include <mutex>

#include "StatisticEntry.hpp"

namespace androlecture
{
namespace common
{
namespace utils
{

class CsvFileWriter
{
public:
    CsvFileWriter(const std::string fileName, const std::string separator = ",");
    ~CsvFileWriter();

    void flush();
    void endrowImpl();

    void printEntries(const std::vector<StatisticEntry>& entries);

    CsvFileWriter& operator << (CsvFileWriter& (* val)(CsvFileWriter&));
    CsvFileWriter& operator << (const char* val);
    CsvFileWriter& operator << (const std::string& val);
    template <typename T>
    CsvFileWriter& operator << (const T& val);

private:
    template <typename T>
    CsvFileWriter& write(const T& val);

    template <typename T>
    CsvFileWriter& write(const T* val);

    template <typename T>
    void specificWrite(const T& val);

    template <typename T>
    void specificWrite(const T* val);

    std::ofstream fs_;
    bool isFirst_;
    const std::string separator_;
    std::mutex mu_;
};

template <typename T>
CsvFileWriter& CsvFileWriter::operator << (const T& val)
{
    write(val);
    return *this;
}

template <typename T>
CsvFileWriter& CsvFileWriter::write(const T& val)
{
    if (!isFirst_)
    {
        fs_ << separator_;
    } 
    else
    {
        isFirst_ = false;
    }
    fs_ << val;
    return *this;
}

template <typename T>
CsvFileWriter& CsvFileWriter::write(const T* val)
{
    if (!isFirst_)
    {
        fs_ << separator_;
    } 
    else
    {
        isFirst_ = false;
    }
    specificWrite(val);
    return *this;
}

template <typename T>
void CsvFileWriter::specificWrite(const T& val)
{
    fs_ << val;
}

template <typename T>
void CsvFileWriter::specificWrite(const T* val)
{
    fs_ << *val;
}

template <>
inline void CsvFileWriter::specificWrite(const std::string& val)
{
    fs_ << '"' << val << '"';
}

template <>
inline void CsvFileWriter::specificWrite(const char* val)
{
    fs_ << '"' << val << '"';
}

inline CsvFileWriter& endrow(CsvFileWriter& csvFileWriter)
{
    csvFileWriter.endrowImpl();
    return csvFileWriter;
}

}  // namespace utils
}  // namespace common
}  // namespace androlecture


#endif  // COMMON_UTILS_CSVFILEWRITER_HPP