#include "CsvFileWriter.hpp"

#include <Common/Logger.hpp>

namespace androlecture
{
namespace common
{
namespace utils
{
namespace
{
    Logger logger{"CsvFileWriter"};
}  // namespace 

CsvFileWriter::CsvFileWriter(const std::string fileName, const std::string separator)
: 
fs_(),
isFirst_(true),
separator_(separator)
{
    fs_.exceptions(std::ios::failbit | std::ios::badbit);
    fs_.open(fileName);
    *this << "ENTRY_NUM" << "WIFI_SIGNAL_STRENGTH" << "AVG_LATENCY" << "MAX_LATENCY" << "MIN_LATENCY" << "TEMP_LATENCY" 
          << "AVG_JITTER" << "MAX_JITTER" << "MIN_JITTER" << "TEMP_JITTER" << "PACKET_LOSS" << "PACKET_DROP" << endrow;
}

CsvFileWriter::~CsvFileWriter()
{
    flush();
    fs_.close();
}

void CsvFileWriter::flush()
{
    fs_.flush();
}

void CsvFileWriter::endrowImpl()
{
    fs_ << std::endl;
    isFirst_ = true;
}

void CsvFileWriter::printEntries(const std::vector<StatisticEntry>& entries)
{
    std::lock_guard<std::mutex> lock(mu_);
    for (const auto& entry : entries)
    {
        *this << entry.numOfEntry_ << entry.wifiSignalStrength_ << entry.avgLatency_ << entry.maxLatency_ << entry.minLatency_ << entry.temporalLatency_
              << entry.avgJitter_ << entry.maxJitter_ << entry.minJitter_ << entry.temporalJitter_ << entry.packetLoss_ << entry.packetDrop_ << endrow;
    }
    *this << endrow;
}

CsvFileWriter& CsvFileWriter::operator << (CsvFileWriter& (* val)(CsvFileWriter&))
{
    return val(*this);
}

CsvFileWriter& CsvFileWriter::operator << (const char* val)
{
    write(val);
    return *this;
}

CsvFileWriter& CsvFileWriter::operator << (const std::string& val)
{
    write(val);
    return *this;
}

}  // namespace utils
}  // namespace common
}  // namespace androlecture