#ifndef LOGGING_CLOGREDIRECTER_HPP
#define LOGGING_CLOGREDIRECTER_HPP
#include <fstream>
#include <streambuf>
#include <iostream>

namespace androlecture
{

class ClogRedirecter
{
public:
    ClogRedirecter();
    ~ClogRedirecter();
private:
    void openLogFile();

    static ClogRedirecter clogRedirecter;
    std::streambuf* clogBuf;
    std::ofstream logFile;
};

}  // namespace androlecture

#endif  // LOGGING_CLOGREDIRECTER_HPP