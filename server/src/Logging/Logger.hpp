#ifndef LOGGING_BASELOGGER_HPP
#define LOGGING_BASELOGGER_HPP

#include <type_traits>
#include <mutex>
#include "CLogRedirecter.hpp"
#include "EComponent.hpp"
#include "ESeverity.hpp"
#include "LogBuffer.hpp"
#include "Flusher.hpp"

namespace androlecture
{
template <EComponent Component, unsigned BufSize = 255>
class BaseLogger
{
    typedef ThreadLocalBuffer<BufSize> ThisThreadBuffer;
    template <typename F>
    using Manipulator = F(*)(EComponent const, const std::string&,
                            ILogBuffer* const, std::ostream&);

    public:
        BaseLogger() = default;
        explicit BaseLogger(const std::string& prefix);
        BaseLogger(const BaseLogger&) = delete;
        BaseLogger& operator=(const BaseLogger&) = delete;
        BaseLogger(BaseLogger&&) = default;
        BaseLogger& operator=(BaseLogger&&) = delete;

        template <typename Severity>
        Severity operator<<(Manipulator<Severity> manipulator) const;
    
    private:
        std::string prefix_;
};

#define ADD_SEVERITY(name, severity) FLUSHER_FACTORY(name, severity)
} // namespace androlecture 

ADD_SEVERITY(info, androlecture::ESeverity_Info)
ADD_SEVERITY(debug, androlecture::ESeverity::ESeverity_Debug)
ADD_SEVERITY(error, androlecture::ESeverity::ESeverity_Error)
ADD_SEVERITY(warning, androlecture::ESeverity::ESeverity_Warning)

#undef ADD_SEVERITY

namespace androlecture
{

template <EComponent Component, unsigned BufSize>
BaseLogger<Component, BufSize>::BaseLogger(const std::string& prefix)
: prefix_("[" + prefix + "] ") {}

template <EComponent Component, unsigned BufSize>
template <typename Severity>
Severity BaseLogger<Component, BufSize>::operator<<(
    Manipulator<Severity> manipulator) const
{
    typename ThisThreadBuffer::LogStorage* storage = 
        ThisThreadBuffer::instance.storage();
    return manipulator(Component, prefix_, &storage->buffer, storage->stream);
}

}  // namespace androlecture

#endif  // LOGGING_BASELOGGER_HPP