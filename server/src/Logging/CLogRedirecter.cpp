#include "CLogRedirecter.hpp"
#include <cmake_constants.hpp>

namespace androlecture
{

ClogRedirecter ClogRedirecter::clogRedirecter;

ClogRedirecter::~ClogRedirecter()
{
    std::clog.rdbuf(clogBuf);
    if (logFile.is_open())
    {
        logFile.close();
    }
}

ClogRedirecter::ClogRedirecter()
{
    clogBuf = std::clog.rdbuf();
    openLogFile();
}

void ClogRedirecter::openLogFile()
{
    if (!logFile.is_open())
    {
        logFile.open(LOG_DIR,
            std::ios_base::out);
        std::clog.rdbuf(logFile.rdbuf());
    }
}

}  // namespace androlecture