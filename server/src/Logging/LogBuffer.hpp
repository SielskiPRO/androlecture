#ifndef LOGGING_LOGBUFFER_HPP
#define LOGGING_LOGBUFFER_HPP

#include <string>
#include <ostream>
#include <streambuf>
#include <functional>
#include <thread>
#include <boost/thread/tss.hpp>

namespace androlecture
{

struct ILogBuffer
{
    typedef std::function<void()> Flush;

    virtual ~ILogBuffer() {}

    virtual void sink(Flush flush) = 0;
    virtual void sink() = 0;
    virtual std::streamsize in_avail() const = 0;
    virtual const char* c_str() const = 0;
    virtual void reset() = 0;
};

template <unsigned Size>
struct LogBuffer : ILogBuffer ,std::streambuf
{
    LogBuffer()
    {
        setp(buffer, buffer + Size);
    }

    void sink(Flush flush)
    {
        flush_ = flush;
    }

    void sink()
    {
        flush_ = 0;
    }

    std::streamsize in_avail() const
    {
        return (pptr() - pbase());
    }

    const char* c_str() const
    {
        *pptr() = '\0';
        return pbase();
    }

    int_type overflow(int_type c = std::char_traits<char>::eof())
    {
        if ( flush_ )
        {
            flush_();
            sputc(c);
        }
        return c;
    }

    void reset()
    {
        pbump(pbase() - pptr());
    }

    char buffer[Size + 1];

private:
    std::function<void()> flush_;
};  //  struct LogBuffer

template <unsigned BufSize>
struct ThreadLocalBuffer
{
    struct LogStorage
    {
        LogStorage()
        : stream(&buffer)
        {
        }

        typedef LogBuffer<BufSize> Buffer;
        typedef std::ostream                    Stream;

        Buffer  buffer;
        Stream  stream;
    };  // struct LogStorage

    typedef typename LogStorage::Buffer             Buffer;
    typedef typename LogStorage::Stream             Stream;
    typedef boost::thread_specific_ptr<LogStorage>  ThreadLogStorage;

    LogStorage* storage() const
    {

        if ( storage_.get() == static_cast<LogStorage*>(0) )
        {
            storage_.reset(new LogStorage);
        }
        return storage_.get();
    }

    static ThreadLocalBuffer<BufSize> instance;

private:
    mutable ThreadLogStorage storage_;
};  // struct ThreadLocalBuffer

template <unsigned BufSize>
ThreadLocalBuffer<BufSize> ThreadLocalBuffer<BufSize>::instance;

}

#endif  // LOGGING_LOGBUFFER_HPP