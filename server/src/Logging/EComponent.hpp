#ifndef LOGGING_COMPONENTS_HPP
#define LOGGING_COMPONENTS_HPP

#include <string>

namespace androlecture{

enum class EComponent
{
    EComponent_Common = 0,
    EComponent_App,
    EComponent_Server,
    EComponent_Protocol,
    EComponent_Services
};

inline std::string componentToString(const EComponent& component)
{
    switch (component)
    {
        case EComponent::EComponent_Common:
            return "Common";
            break;
        case EComponent::EComponent_App:
            return "App";
            break;
        case EComponent::EComponent_Server:
            return "Server";
            break;
        case EComponent::EComponent_Protocol:
            return "Protocol";
            break;
        case EComponent::EComponent_Services:
            return "Services";
            break;
        default:
            return "UNKNOWN COMPONENT";
            break;
    }
}

}  // namespace androlecture






#endif  // LOGGING_COMPONENTS_HPP