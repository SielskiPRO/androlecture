#ifndef LOGGING_FLUSHER_HPP
#define LOGGING_FLUSHER_HPP

#include <mutex>
#include <string>
#include <iostream>
#include <ctime>
#include <iomanip>
#include <sstream>
#include "LogBuffer.hpp"
#include "ESeverity.hpp"
#include "EComponent.hpp"

#define FLUSHER_FACTORY_IMPL(name, severity)                                \
    inline androlecture::Flusher<severity> name(                            \
        const androlecture::EComponent component, const std::string& prefix,\
        androlecture::ILogBuffer* const buffer, std::ostream& stream) {     \
        return androlecture::Flusher<severity>(                             \
            component, prefix, buffer, stream); }

#define FLUSHER_FACTORY(name, severity)                                     \
    FLUSHER_FACTORY_IMPL(name, severity)

namespace
{

std::string getTimestamp()
{
    auto t = std::time(nullptr);
    auto localTime = *std::localtime(&t);

    std::ostringstream oss;
    oss << std::put_time(&localTime, "%d-%m-%Y %H-%M-%S");
    return oss.str() + " ";
}

}  // namespace

namespace androlecture
{

inline void PrintToSyslog(const ESeverity severity,
    const EComponent component, const char* const prefix,
    const char* const message)
{
    static std::mutex mu;
    std::lock_guard<std::mutex> guard(mu);

    auto timestamp = getTimestamp();
    auto componentStr = componentToString(component);
    auto severityStr = severityToString(severity);

    if (ESeverity::ESeverity_Debug != severity)
    {
        std::cout << timestamp << severityStr << componentStr << prefix << message << std::endl;
        std::clog << timestamp << severityStr << componentStr << prefix << message << std::endl;
    }
    else
        std::cout << timestamp << severityStr << componentStr << prefix << message << std::endl;   
}

class BaseFlusher
{
public:
    explicit BaseFlusher(const EComponent component, std::ostream& stream)
    : component(component)
    , stream(stream) {}

    BaseFlusher(BaseFlusher const&) = delete;
    BaseFlusher(BaseFlusher&&) = default;


    operator std::ostream& ()
    {
        return this->stream;
    }

    template <typename T>
    BaseFlusher& operator<<(const T& t)
    {
        this->stream << t;
        return *this;
    }

protected:
    const EComponent component;
    std::ostream&  stream;
};

template <ESeverity Severity>
class Flusher : public BaseFlusher
{
public:
    Flusher(const EComponent component,
        const std::string& prefix,
        ILogBuffer* const buffer,
        std::ostream& stream)
    : BaseFlusher(component, stream)
    , prefix_(prefix)
    , buffer_(buffer)
    {
        buffer_->sink([this]{ flush(); });
    }

    Flusher(Flusher const&) = delete;
    Flusher(Flusher&&) = default;

    ~Flusher()
    {
        if ( buffer_->in_avail() > 0 )
        {
            this->flush();
        }
        buffer_->sink();
    }

    void flush()
    {
        PrintToSyslog(Severity, component,
            prefix_.c_str(), buffer_->c_str());
        buffer_->reset();
    }

private:
    const std::string& prefix_;
    ILogBuffer* const buffer_;
};

}  // namespace androlecture

#endif  // LOGGING_FLUSHER_HPP