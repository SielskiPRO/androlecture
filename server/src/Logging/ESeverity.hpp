#ifndef LOGGING_ESEVERITY_HPP
#define LOGGING_ESEVERITY_HPP

namespace androlecture
{

enum ESeverity
{
    ESeverity_Info = 0,
    ESeverity_Debug,
    ESeverity_Error,
    ESeverity_Warning
};

inline std::string severityToString(const ESeverity& severity)
{
    switch (severity)
    {
        case ESeverity::ESeverity_Info:
            return "<INFO> ";
            break;
        case ESeverity::ESeverity_Debug:
            return "<DEBUG> ";
            break;
        case ESeverity::ESeverity_Error:
            return "<ERROR> ";
            break;
        case ESeverity::ESeverity_Warning:
            return "<WARNING> ";
            break;
        default:
            return "UNKNOWN ";
            break;
    }
}   

}  // namespace androlecture

#endif  // LOGGING_ESEVERITY_HPP