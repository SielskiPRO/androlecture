#ifndef SERVER_LOGGER_HPP
#define SERVER_LOGGER_HPP

#include <Logging/Logger.hpp>

namespace androlecture
{
namespace server
{
    using Logger = BaseLogger<EComponent::EComponent_Server>;
}  // namespace server
}  // namespace androlecture

#endif  // SERVER_LOGGER_HPP