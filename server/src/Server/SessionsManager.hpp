#ifndef SERVER_SESSIONSMANAGER_HPP
#define SERVER_SESSIONSMANAGER_HPP

#include <set>
#include <vector>
#include <future>
#include <mutex>

#include <Common/Utils/IdAssigner.hpp>

#include "TcpSession.hpp"

namespace androlecture
{
namespace server
{

class SessionsManager
{
public:
    ~SessionsManager();
    void createSession(const int clientSocket, const std::string ipAddress, const bool& collectStatistics);
    std::future<void> removeSession(const TcpSession& tcpSession);
    void remove(const TcpSession& tcpSession);

private:
    
    std::mutex mu_;
    common::utils::IdAssigner idAssigner_;
    std::set<TcpSession> sessions_;
};

}  // namespace server
}  // namespace androlecture

#endif  // SERVER_SESSIONSMANAGER_HPP