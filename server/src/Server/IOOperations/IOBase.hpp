#ifndef SERVER_IOOPERATIONS_IOBASE_HPP
#define SERVER_IOOPERATIONS_IOBASE_HPP

#include <sys/types.h>

#include "IO.hpp"

namespace androlecture
{
namespace server
{
namespace iooperations
{

class IOBase : public IO
{
public:
    explicit IOBase(int socket);
    virtual void closeSocket();
protected:
    int socket_;
};

}  // namespace iooperations
}  // namespace server
}  // namespace androlecture

#endif  // SERVER_IOOPERATIONS_IOBASE_HPP