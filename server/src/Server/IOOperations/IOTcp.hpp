#ifndef SERVER_IOOPERATIONS_IOTCP_HPP
#define SERVER_IOOPERATIONS_IOTCP_HPP

#include "IOBase.hpp"

namespace androlecture
{
namespace server
{
namespace iooperations
{

class IOTcp : public IOBase
{
public:
    explicit IOTcp(int socket);
    virtual ssize_t sendMessage(const unsigned char* message, const unsigned& size) const;
    virtual ssize_t readMessage(unsigned char* buffer, const unsigned& size) const;
};

}  // namespace iooperations
}  // namespace server
}  // namespace androlecture

#endif  // SERVER_IOOPERATIONS_IOTCP_HPP