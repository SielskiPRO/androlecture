
#include "IOBase.hpp"

#include <unistd.h>
#include <sys/socket.h>

namespace androlecture
{
namespace server
{
namespace iooperations
{

IOBase::IOBase(int socket)
: socket_(socket)
{}

void IOBase::closeSocket()
{
    shutdown(socket_, SHUT_RDWR);
    close(socket_);
}

}  // namespace iooperations
}  // namespace server
}  // namespace androlecture
