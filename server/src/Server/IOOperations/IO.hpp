#ifndef SERVER_IOOPERATIONS_IO_HPP
#define SERVER_IOOPERATIONS_IO_HPP

#include <sys/types.h>

namespace androlecture
{
namespace server
{
namespace iooperations
{

class IO
{
public:
    virtual ~IO() = default;
    virtual ssize_t sendMessage(const unsigned char* message, const unsigned& size) const = 0;
    virtual ssize_t readMessage(unsigned char* buffer, const unsigned& size) const = 0;
    virtual void closeSocket() = 0;
};

}  // namespace iooperations
}  // namespace server
}  // namespace androlecture

#endif  // SERVER_IOOPERATIONS_IO_HPP