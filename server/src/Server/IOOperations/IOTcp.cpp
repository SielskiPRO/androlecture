
#include <unistd.h>
#include <sys/socket.h>

#include "IOTcp.hpp"
#include <Server/Logger.hpp>

namespace androlecture
{
namespace server
{
namespace iooperations
{

namespace
{
    Logger logger{"IOTcp"};
}

IOTcp::IOTcp(int socket) 
: IOBase(socket)
{}

ssize_t IOTcp::sendMessage(const unsigned char* message, const unsigned& size) const
{
    auto bytesSended = send(socket_, message, size, 0);
    if (bytesSended == -1)
    {
        logger << error << "Error on sending data!";
    }
    return bytesSended;
}

ssize_t IOTcp::readMessage(unsigned char* buffer, const unsigned& size) const
{
    unsigned receivedBytes = 0;
    ssize_t readedBytes = 0;
    unsigned howManyMoreToRead = size;
    do
    {
        readedBytes = read(socket_, buffer + receivedBytes, howManyMoreToRead);
        if (readedBytes == 0)
        {
            return 0;
        }
        if (readedBytes > 0)
        {
            receivedBytes += readedBytes;
            howManyMoreToRead -= readedBytes;
        }
        if (readedBytes < 0)
        {
            logger << error << "Received negative number, some sort of error!";
        }
    } while (readedBytes != 0 && receivedBytes != size);
    return receivedBytes;
}

}  // namespace iooperations
}  // namespace server
}  // namespace androlecture
