
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <strings.h>
#include <netinet/tcp.h>

#include <memory>
#include <string>
#include <iostream>
#include <functional>
#include <cstring>

#include <Common/Threading/ThreadPool.hpp>

#include "TcpSession.hpp"
#include "TcpServer.hpp"
#include "Logger.hpp"

namespace androlecture
{
namespace server
{

using namespace common::threading;

namespace
{
    Logger logger{"TcpServer"};
}  //  namespace 

TcpServer::TcpServer(const unsigned& port, const bool& collectStatistics)
: BaseServer(port), keepAlive_(10), keepIdle_(10), keepCnt_(10), keepIntvl_(10), collectStatistics_(collectStatistics)
{
    initialize();
    future_ = ThreadPool::getInstance().add_task(std::bind(&TcpServer::process, this));
}

void TcpServer::process()
{
    start();
    logger << info << "TcpServer is waiting for clients connections";
    while(isActive())
    {
        auto newSock = accept(serverDescriptor_, 
            (struct sockaddr*)&cli_addr_, &clilen_);

    
        if (newSock < 0){
            if (errno == EINVAL)
            {
                logger << info << "Accepting on server aborted due to socket shutdown";
            }
            else
                logger << error << "Could not accept incoming connection";
        }
        else
        {
            if (setsockopt(newSock, SOL_SOCKET, SO_KEEPALIVE, 
                (void *)&keepAlive_, sizeof(keepAlive_))){
                throw std::runtime_error("Error SO_KEEPALIVE");
            }

            const std::string ipAddress(inet_ntoa(cli_addr_.sin_addr));
            const unsigned portNum = ntohs(cli_addr_.sin_port);

            logger << info << "New user connected! (" << ipAddress << ", " << portNum << ")";

            sessionsManager_.createSession(std::move(newSock), std::move(ipAddress), collectStatistics_);
        }
    }
    logger << info << "Ending server application!";
}

TcpServer::~TcpServer()
{
    stop();
    future_.wait();
}

void TcpServer::initialize()
{
    clilen_ = sizeof(cli_addr_);
    if ((serverDescriptor_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == 0)
    {
        throw std::runtime_error("Could not create TCP server socket!");
    }

    if (setsockopt(serverDescriptor_, SOL_TCP, TCP_KEEPIDLE, 
        (void *)&keepIdle_, sizeof(keepIdle_))){
            throw std::runtime_error("Error TCP_KEEPIDLE");
        }

    if (setsockopt(serverDescriptor_, SOL_TCP, TCP_KEEPCNT, 
        (void *)&keepCnt_, sizeof(keepCnt_))){
            throw std::runtime_error("Error TCP_KEEPCNT");
        }

    if (setsockopt(serverDescriptor_, SOL_TCP, TCP_KEEPINTVL, 
        (void *)&keepIntvl_, sizeof(keepIntvl_))){
            throw std::runtime_error("Error TCP_KEEPINTVL");
        }

    if (setsockopt(serverDescriptor_, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
        (void *)&keepAlive_, sizeof(keepAlive_))){
            throw std::runtime_error("Error SO_REUSEADDR | SO_REUSEPORT");
        }

    if (bind(serverDescriptor_, (struct sockaddr*)&address_, sizeof(address_)))
    {
        throw std::runtime_error("Could not bind TCP server!");
    }

    if (listen(serverDescriptor_, 10) < 0)
    {
        throw std::runtime_error("Could not listen on TCP server!");
    }        
}

}  // namespace server
}  // namespace androlecture
