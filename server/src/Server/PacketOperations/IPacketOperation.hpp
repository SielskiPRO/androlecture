#ifndef SERVER_PACKETOPERATIONS_IPACKETRECEIVER_HPP
#define SERVER_PACKETOPERATIONS_IPACKETRECEIVER_HPP

#include <memory>

#include <Protocol/Packet/Packet.hpp>

namespace androlecture
{
namespace server
{
namespace packetoperation
{

class IPacketOperation
{
public:
    virtual ~IPacketOperation() = default;
    virtual std::unique_ptr<protocol::packets::Packet> receivePacket() const = 0;
    virtual int sendPacket(const protocol::packets::Packet& packet) const = 0;
    virtual void stop() const = 0;
};

}  // namespace packetoperation
}  // namespace server
}  // namespace androlecture

#endif  // SERVER_PACKETOPERATIONS_IPACKETRECEIVER_HPP