
#include <functional>
#include <cstring>
#include <unistd.h>

#include "TcpPacketOperation.hpp"

#include <Server/IOOperations/IOTcp.hpp>

#include <Server/Logger.hpp>

namespace androlecture
{
namespace server
{
namespace packetoperation
{

using namespace protocol;
using namespace protocol::packets;
using namespace iooperations;

namespace
{
    Logger logger{"TcpPacketOperation"};
}  // namespace

TcpPacketOperation::TcpPacketOperation(int clientSocket)
: PacketOperationBase(std::make_unique<IOTcp>(clientSocket))
{}

std::unique_ptr<Packet> TcpPacketOperation::receivePacket() const
{
    auto header = receiveHeader();
    if (header == nullptr)
    {
        return nullptr;
    }
    if (header->getPayloadSize() == 0)
    {
        return std::make_unique<Packet>(std::move(header), nullptr);
    }
    auto payload = receivePayload(header->getPayloadSize());

    if (payload != nullptr)
    {
        return std::make_unique<Packet>(std::move(header), std::move(payload));
    }
    logger << debug << "Packet was not received correctly!";   
    return nullptr;
}

int TcpPacketOperation::sendPacket(const Packet& packet) const
{
    const auto byteArray = packet.toByteArray();
    const auto messageSize = packet.getHeader().getPayloadSize() + HEADER_SIZE;
    const auto bytesSend = io_->sendMessage(byteArray.get(), messageSize);
    return bytesSend;
}

std::unique_ptr<TcpHeader> TcpPacketOperation::receiveHeader() const
{
    unsigned char buffer[HEADER_SIZE];
    if (io_->readMessage(buffer, sizeof(buffer)) < 1)
        return nullptr;

    uint64_t payloadSize = 0;
    uint8_t command = 0;

    std::memcpy(&payloadSize, buffer, PAYLOAD_SIZE_FIELD_SIZE);
    command = static_cast<uint8_t>(buffer[8]);

    return std::make_unique<TcpHeader>(std::move(payloadSize), std::move(cast(command)));
}

std::unique_ptr<unsigned char[]> TcpPacketOperation::receivePayload(const uint64_t& payloadSize) const
{
    auto payload = std::make_unique<unsigned char[]>(payloadSize);
    
    if (io_->readMessage(payload.get(), payloadSize) < 1)
        payload = nullptr;

    return payload;
}


}  // namespace packetoperation
}  // namespace server
}  // namespace androlecture