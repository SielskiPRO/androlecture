#ifndef SERVER_PACKETOPERATIONS_TCPPACKETOPERATION_HPP
#define SERVER_PACKETOPERATIONS_TCPPACKETOPERATION_HPP

#include <memory>
#include <cstdint>
#include <functional>

#include <Protocol/Packet/TcpHeader.hpp>

#include "PacketOperationBase.hpp"


namespace androlecture
{
namespace server
{
namespace packetoperation
{

class TcpPacketOperation : public PacketOperationBase
{
public:
    TcpPacketOperation(int clientSocket);
    virtual std::unique_ptr<protocol::packets::Packet> receivePacket() const;
    virtual int sendPacket(const protocol::packets::Packet& packet) const;
private:
    std::unique_ptr<protocol::packets::TcpHeader> receiveHeader() const;
    std::unique_ptr<unsigned char[]> receivePayload(const uint64_t& payloadSize) const;
};

}  // namespace packetoperation
}  // namespace server
}  // namespace androlecture

#endif  // SERVER_PACKETOPERATIONS_TCPPACKETOPERATION_HPP
