#ifndef SERVER_PACKETOPERATIONS_PACKETOPERATIONRBASE_HPP
#define SERVER_PACKETOPERATIONS_PACKETOPERATIONRBASE_HPP

#include <memory>

#include <Server/IOOperations/IO.hpp>

#include "IPacketOperation.hpp"

namespace androlecture
{
namespace server
{
namespace packetoperation
{

class PacketOperationBase : public IPacketOperation
{
public:
    explicit PacketOperationBase(std::unique_ptr<iooperations::IO> io);
    virtual void stop() const;
protected:
    std::unique_ptr<iooperations::IO> io_;
};

}  // namespace packetoperation
}  // namespace server
}  // namespace androlecture

#endif  // SERVER_PACKETOPERATIONS_PACKETOPERATIONRBASE_HPP