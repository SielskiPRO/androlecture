
#include "PacketOperationBase.hpp"

namespace androlecture
{
namespace server
{
namespace packetoperation
{

using namespace iooperations;

PacketOperationBase::PacketOperationBase(std::unique_ptr<iooperations::IO> io)
: io_(std::move(io))
{}

void PacketOperationBase::stop() const
{
    io_->closeSocket();
}

}  // namespace packetoperation
}  // namespace server
}  // namespace androlecture
