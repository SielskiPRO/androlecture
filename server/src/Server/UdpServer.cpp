
#include "UdpServer.hpp"
#include <Protocol/Protocol.hpp>
#include <Common/Threading/ThreadPool.hpp>
#include <Services/Translation.hpp>
#include <cstring>

#include "Logger.hpp"

namespace androlecture
{
namespace server
{

using namespace protocol;
using namespace services;
using namespace common::threading;

namespace
{
    Logger logger{"UdpServer"};
}

UdpServer::UdpServer(const services::Translation& translation)
: translation_(translation)
{
    initialize();
    future_ = ThreadPool::getInstance().add_task(std::bind(&UdpServer::process, this));
}

UdpServer::~UdpServer()
{
    stop();
    future_.wait();
}

void UdpServer::process()
{
    start();
    std::unique_ptr<unsigned char[]> bufferPtr_;
    int len = sizeof(cliaddr_);
    bufferPtr_ = std::make_unique<unsigned char[]>(MAX_PACKET_SIZE);
    std::memset(bufferPtr_.get(), 0, MAX_PACKET_SIZE);
    while (isActive())
    {
        
        auto receivedBytes = recvfrom(serverDescriptor_ , bufferPtr_.get(), 
            MAX_PACKET_SIZE, MSG_WAITALL, (struct sockaddr *)&cliaddr_, (socklen_t *)&len);

        if (receivedBytes < 0)
        {
            logger << error << "Error while receiving datagram!";
            continue;
        }
        else
        {
            if (receivedBytes == 0)
            {
                continue;
            }

            auto bytesSended = sendto(serverDescriptor_, bufferPtr_.get(), receivedBytes, 0,
                (struct sockaddr*)&groupSock_, sizeof(groupSock_));

            if (bytesSended < 0)
            {
                logger << error << "Error while sending multicast!";
            }
        } 
    }
}

const unsigned short& UdpServer::getDestinationPort() const
{
    return destinationPort_;
}

void UdpServer::initialize()
{
    memset(&cliaddr_, 0, sizeof(cliaddr_));

    if ((serverDescriptor_ = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        logger << error << "Socket creation failed";
        throw std::runtime_error("Socket creation failed");
    }

    if (bind(serverDescriptor_, (const struct sockaddr *)&address_, sizeof(address_)) < 0)
    {
        logger << error << "Socket binding failed";
        throw std::runtime_error("Socket binding failed");
    }

    struct sockaddr_in sin;
    socklen_t len = sizeof(sin);

    if (getsockname(serverDescriptor_, (struct sockaddr *)&sin, &len) == -1)
    {
        logger << error << "getsockname error";
        perror("getsockname");
        throw std::runtime_error("getsockname");
    }

    destinationPort_ = ntohs(sin.sin_port);

    logger << debug << "Assigned UDP dest port: " << destinationPort_;

    std::memset(&groupSock_, 0, sizeof(groupSock_));
    groupSock_.sin_family = AF_INET;
    groupSock_.sin_addr.s_addr = htonl(translation_.getIpGroupAddress());
    groupSock_.sin_port = htons(translation_.getReceivePort());

    {
        char loopch = 0;
        if (setsockopt(serverDescriptor_, IPPROTO_IP, IP_MULTICAST_LOOP, 
        &loopch, sizeof(loopch))< 0)
        {
            close(serverDescriptor_);
            throw std::runtime_error("IP_MULTICAST_LOOP");
        }
    }

    localInterface_.s_addr = INADDR_ANY;
    if (setsockopt(serverDescriptor_, IPPROTO_IP, IP_MULTICAST_IF,
        &localInterface_, sizeof(localInterface_)) < 0)
        {
            throw std::runtime_error("IP_MULTICAST_IF");
        }
}

}  // namespace server
}  // namesapce androlecture
