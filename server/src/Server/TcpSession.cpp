#include <unistd.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <cstring>
#include <functional>

#include <Protocol/Protocol.hpp>
#include <Protocol/Packet/Packet.hpp>
#include <Protocol/Transactions/Transactions.hpp>

#include <Services/TranslationsService.hpp>

#include <Common/Threading/ThreadPool.hpp>

#include "TcpSession.hpp"
#include "SessionsManager.hpp"
#include "Logger.hpp"

namespace androlecture
{
namespace server
{

using namespace packetoperation;
using namespace protocol::packets;
using namespace protocol::transactions;
using namespace common::threading;
using namespace services;

namespace
{
    Logger logger{"TcpSession"};
}

TcpSession::TcpSession(const int clientSock, const std::string ipAddress,
    const unsigned id, const bool& collectStatistics, SessionsManager& sessionsManager)
: packetOperation_(clientSock), ipAddress_(std::move(ipAddress_)),
    id_(std::move(id)), collectStatistics_(collectStatistics), isAuthenticated_(false), isStoppedByManager_(false), sessionsManager_(sessionsManager)
{
    future_ = ThreadPool::getInstance().add_task(std::bind(&TcpSession::serve, this));
}

TcpSession::~TcpSession()
{
    packetOperation_.stop();
    deleteTranslation();
}

void TcpSession::setAuthenticated()
{
    isAuthenticated_ = true;
}

bool TcpSession::isAuthenticated() const
{
    return isAuthenticated_;
}

const boost::optional<const services::Translation>& TcpSession::getTranslation() const
{
    return translation_;
}

const std::future<void>& TcpSession::getFuture() const
{
    return future_;
}

void TcpSession::deleteTranslation()
{
    if (translation_)
    {
        TranslationsService::getInstance().deleteTranslation(*translation_);
        translation_ = boost::none;
    }
}

const bool& TcpSession::collectStatistics() const
{
    return collectStatistics_;
}

bool TcpSession::operator<(const TcpSession& rhs) const
{
    return id_ < rhs.id_;
}

void TcpSession::serve()
{
    std::unique_ptr<Packet> packet = nullptr;
    do
    {
        packet = packetOperation_.receivePacket();
        if (packet)
        {
            const auto& transaction = Transactions::getTransaction(*packet);
            transaction.startTransaction(packetOperation_, *packet, *this);
        }
    } while (packet != nullptr);
    
    if (!isStoppedByManager_)
    {
        sessionsManager_.removeSession(*this);
    } 
}

const unsigned& TcpSession::getId() const
{
    return id_;
}

void TcpSession::stopByManager() const
{
    isStoppedByManager_ = true;
    packetOperation_.stop();
}


}  // namespace server
}  // namespace androlecture

