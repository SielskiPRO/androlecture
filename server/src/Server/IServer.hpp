#ifndef SERVER_ISERVER_HPP
#define SERVER_ISERVER_HPP

namespace androlecture
{
namespace server
{

class IServer
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual bool isActive() = 0;
    virtual ~IServer() = default;
    virtual void process() = 0;
};

}  // namespace server
}  // namespace androlecture


#endif  // SERVER_ISERVER_HPP