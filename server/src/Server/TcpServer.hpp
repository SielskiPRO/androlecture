#ifndef SERVER_TCPSERVER_HPP
#define SERVER_TCPSERVER_HPP

#include <vector>
#include <memory>

#include <Common/Threading/ThreadPool.hpp>

#include "BaseServer.hpp"
#include "TcpSession.hpp"
#include "SessionsManager.hpp"


namespace androlecture
{
namespace server
{

class TcpServer : public BaseServer
{
public:
    explicit TcpServer(const unsigned& port, const bool& collectStatistics);
    ~TcpServer();

    TcpServer(const TcpServer&) = delete;
    TcpServer& operator=(const TcpServer&) = delete;
    TcpServer(TcpServer&&) = delete;
    TcpServer& operator=(TcpServer&&) = delete; 

    virtual void process() override;
private:
    void initialize();
    struct sockaddr_in cli_addr_;
    socklen_t clilen_;
    int keepAlive_;
    int keepIdle_;
    int keepCnt_;
    int keepIntvl_;
    const bool& collectStatistics_;
    std::future<void> future_;
    SessionsManager sessionsManager_;
};

}  // namespace server
}  // namespace androlecture

#endif  // SERVER_TCPSERVER_HPP