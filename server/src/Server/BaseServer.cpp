#include <unistd.h>

#include "BaseServer.hpp"
#include "Logger.hpp"


namespace androlecture
{
namespace server
{

namespace
{
    Logger logger{"BaseServer"};
}  //  namespace

BaseServer::BaseServer()
{
    address_.sin_family = AF_INET;
    address_.sin_addr.s_addr = INADDR_ANY;
    address_.sin_port = 0;
}

BaseServer::BaseServer(const unsigned& port)
{
    address_.sin_family = AF_INET;
    address_.sin_addr.s_addr = INADDR_ANY;
    address_.sin_port = htons(port);  
}

void BaseServer::stop()
{
    isRunning = false;
    shutdown(serverDescriptor_, SHUT_RDWR);
    close(serverDescriptor_);
}

void BaseServer::start()
{
    isRunning = true;
}

bool BaseServer::isActive()
{
    return isRunning;
}

BaseServer::~BaseServer()
{
    stop();
}

}  // namespace server
}  // namespace androlecture
