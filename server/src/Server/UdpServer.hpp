#ifndef SERVER_UDPSERVER_HPP
#define SERVER_UDPSERVER_HPP

#include "BaseServer.hpp"
#include <memory>
#include <future>


namespace androlecture
{

namespace services
{
class Translation;
}  // namespace services

namespace server
{
class UdpServer : public BaseServer
{
public:
    explicit UdpServer(const services::Translation& translation);
    ~UdpServer();
    virtual void process() override;
    const unsigned short& getDestinationPort() const;

    UdpServer(const UdpServer&) = delete;
    UdpServer& operator=(const UdpServer&) = delete;
    UdpServer(UdpServer&&) = delete;
    UdpServer& operator=(UdpServer&&) = delete;
private:
    void initialize();
    struct sockaddr_in cliaddr_;
    struct in_addr localInterface_;
    struct sockaddr_in groupSock_;
    unsigned short destinationPort_;
    const services::Translation& translation_;
    std::future<void> future_;
};

}  // namespace server
}  // namesapce androlecture

#endif  // SERVER_UDPSERVER_HPP