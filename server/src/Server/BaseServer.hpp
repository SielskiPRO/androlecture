#ifndef SERVER_BASESERVER_HPP
#define SERVER_BASESERVER_HPP

#include <thread>
#include <iostream>
#include <memory>
#include <netinet/in.h>

#include <Common/Threading/ThreadPool.hpp>

#include "IServer.hpp"

namespace androlecture
{
namespace server
{

class BaseServer : public IServer
{
public:
    BaseServer();
    explicit BaseServer(const unsigned& port);
    virtual void stop() override;
    virtual void start() override;
    virtual bool isActive() override;
    ~BaseServer();
protected:
    int serverDescriptor_;
    struct sockaddr_in address_;
private:
    bool isRunning;
};

}  // namespace server
}  // namespace androlecture

#endif  // SERVER_BASESERVER_HPP