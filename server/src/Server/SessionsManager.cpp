
#include <functional>
#include "SessionsManager.hpp"
#include <Common/Threading/ThreadPool.hpp>

#include "Logger.hpp"

namespace androlecture
{
namespace server
{

using namespace common::threading;

namespace
{
    Logger logger{"SessionsManager"};
}

SessionsManager::~SessionsManager()
{
    for (auto& session : sessions_)
    {
        session.stopByManager();
        if (session.getFuture().valid())
            session.getFuture().wait();
    }
    sessions_.clear();
}

void SessionsManager::createSession(const int clientSocket, const std::string ipAddress, const bool& collectStatistics)
{
    std::lock_guard<std::mutex> lock(mu_);
    sessions_.emplace(std::move(clientSocket), std::move(ipAddress), idAssigner_.getId(), collectStatistics, *this);
}

std::future<void> SessionsManager::removeSession(const TcpSession& tcpSession)
{
    return ThreadPool::getInstance().add_task(std::bind(&SessionsManager::remove, this, std::ref(tcpSession)));
}

void SessionsManager::remove(const TcpSession& tcpSession)
{
    const auto id = tcpSession.getId();
    tcpSession.getFuture().wait();
    {
        std::lock_guard<std::mutex> lock(mu_);
        auto i = sessions_.erase(tcpSession);
    }
    idAssigner_.addFreeId(std::move(id));
}

}  // namespace server
}  // namespace androlecture

