#ifndef SERVER_TCPSESSION_HPP
#define SERVER_TCPSESSION_HPP

#include <future>

#include <boost/optional.hpp>

#include <Server/PacketOperations/TcpPacketOperation.hpp>
#include <Services/Translation.hpp>

namespace androlecture
{
namespace server
{

class SessionsManager;

class TcpSession
{
public:
    TcpSession(int clientSock, const std::string ipAddress, const unsigned id, const bool& collectStatistics,
        SessionsManager& sessionsManager);
    ~TcpSession();

    TcpSession(const TcpSession&) = delete;
    TcpSession& operator=(const TcpSession&) = delete;
    TcpSession(TcpSession&&) = delete;
    TcpSession& operator=(TcpSession&&) = delete; 

    void serve();
    const unsigned& getId() const;
    void stopByManager() const;
    void setAuthenticated();
    bool isAuthenticated() const;
    const boost::optional<const services::Translation>& getTranslation() const;
    const std::future<void>& getFuture() const;
    template <typename ...Args>
    const services::Translation& setTranslation(Args&& ...args);

    void deleteTranslation();
    const bool& collectStatistics() const;

    bool operator<(const TcpSession& rhs) const;

private:
    const packetoperation::TcpPacketOperation packetOperation_;
    std::string ipAddress_;
    unsigned id_;
    bool isAuthenticated_;
    mutable bool isStoppedByManager_;
    std::future<void> future_;
    boost::optional<const services::Translation> translation_;
    const bool& collectStatistics_;
    SessionsManager& sessionsManager_;
};

template <typename ...Args>
const services::Translation& TcpSession::setTranslation(Args&& ...args)
{
    translation_.emplace(std::forward<Args>(args)...);
    return translation_.get();
}

}  // namespace server
}  // namespace androlecture


#endif  // SERVER_TCPSESSION_HPP