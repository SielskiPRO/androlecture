#ifndef PROTOCOL_TRANSACTIONS_GETTRANSLATIONSTRANSACTION_HPP
#define PROTOCOL_TRANSACTIONS_GETTRANSLATIONSTRANSACTION_HPP

#include "ITransaction.hpp"


namespace androlecture
{
namespace protocol
{
namespace transactions
{

class GetTranslationsTransaction : public ITransaction
{
public:
    GetTranslationsTransaction();
    virtual void startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
    const packets::Packet& initialPacket, server::TcpSession& tcpSession) const override;
};


}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure

#endif  // PROTOCOL_TRANSACTIONS_GETTRANSLATIONSTRANSACTION_HPP