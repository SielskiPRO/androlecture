#ifndef PROTOCOL_TRANSACTIONS_ITRANSACTION_HPP
#define PROTOCOL_TRANSACTIONS_ITRANSACTION_HPP

#include <Server/PacketOperations/TcpPacketOperation.hpp>
#include <Server/TcpSession.hpp>
#include <Protocol/Packet/Packet.hpp>

#include <mutex>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

class ITransaction
{
public:
    virtual ~ITransaction() = default;
    virtual void startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
        const protocol::packets::Packet& initialPacket, server::TcpSession& tcpSession) const = 0;
};

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure

#endif  // PROTOCOL_TRANSACTIONS_ITRANSACTION_HPP
