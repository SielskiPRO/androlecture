
#include "AuthenticateTransaction.hpp"
#include <Protocol/Protocol.hpp>
#include <Protocol/Logger.hpp>
#include <Protocol/Packet/Packet.hpp>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

using namespace services;
using namespace packets;
using namespace server;

namespace
{
    Logger logger{"AuthenticateTransaction"};
}


AuthenticateTransaction::AuthenticateTransaction()
{}

void AuthenticateTransaction::startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
    const packets::Packet& initialPacket, server::TcpSession& tcpSession) const
{
    const auto payload = reinterpret_cast<const char*>(initialPacket.getPayload());
    const auto payloadSize = initialPacket.getHeader().getPayloadSize();

    std::string passwordString(payload, payloadSize);

    if (passwordString == PASSWORD)
    {
        tcpSession.setAuthenticated();
        packetOperation.sendPacket(createPacket(Command::RESPONSE_SUCCESS));
    }
    else
    {
        std::string errorMessage = "Invalid password!";
        packetOperation.sendPacket(createPacket(Command::RESPONSE_FAILURE, errorMessage));
    }
}

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure
