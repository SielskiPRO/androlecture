
#include "PingTransaction.hpp"


namespace androlecture
{
namespace protocol
{
namespace transactions
{

using namespace server::packetoperation;
using namespace packets;
using namespace server;

PingTransaction::PingTransaction()
{}

void PingTransaction::startTransaction(const TcpPacketOperation& packetOperation,
const Packet& initialPacket, TcpSession& tcpSession) const
{
    std::cout << "Collect statistics : " << tcpSession.collectStatistics() << std::endl;
    packetOperation.sendPacket(createPacket(Command::RESPONSE_SUCCESS, tcpSession.collectStatistics()));
}

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure
