#ifndef PROTOCOL_TRANSACTIONS_PINGTRANSACTION_HPP
#define PROTOCOL_TRANSACTIONS_PINGTRANSACTION_HPP

#include "ITransaction.hpp"


namespace androlecture
{
namespace protocol
{
namespace transactions
{

class PingTransaction : public ITransaction
{
public:
    PingTransaction();
    virtual void startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
        const packets::Packet& initialPacket, server::TcpSession& tcpSession) const override;
};


}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure

#endif  // PROTOCOL_TRANSACTIONS_PINGTRANSACTION_HPP