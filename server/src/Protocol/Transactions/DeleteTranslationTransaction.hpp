#ifndef PROTOCOL_TRANSACTIONS_DELETETRANSLATIONTRANSACTION_HPP
#define PROTOCOL_TRANSACTIONS_DELETETRANSLATIONTRANSACTION_HPP

#include "ITransaction.hpp"

namespace androlecture
{
namespace protocol
{
namespace transactions
{

class DeleteTranslationTransaction : public ITransaction
{
public:
    virtual void startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
        const packets::Packet& initialPacket, server::TcpSession& tcpSession) const override;
private:
};

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecture

#endif  // PROTOCOL_TRANSACTIONS_DELETETRANSLATIONTRANSACTION_HPP