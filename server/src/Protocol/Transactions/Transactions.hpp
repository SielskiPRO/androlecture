#ifndef PROTOCOL_TRANSACTIONS_TRANSACTIONS_HPP
#define PROTOCOL_TRANSACTIONS_TRANSACTIONS_HPP

#include "ITransaction.hpp"
#include <Protocol/Protocol.hpp>

#include <Protocol/Packet/Packet.hpp>

#include <map>
#include <memory>
#include <mutex>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

class Transactions
{
public:
    static ITransaction& getTransaction(const packets::Packet& packet);
private:
    typedef std::map<Command, std::unique_ptr<ITransaction>> TransactionsMap;
    static TransactionsMap transactions_;
    static std::mutex mu_;
};

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecture

#endif  // PROTOCOL_TRANSACTIONS_TRANSACTIONS_HPP