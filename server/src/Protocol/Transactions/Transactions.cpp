
#include "Transactions.hpp"

#include "AddTranslationTransaction.hpp"
#include "AuthenticateTransaction.hpp"
#include "GetTranslationsTransaction.hpp"
#include "DeleteTranslationTransaction.hpp"
#include "GetTimestampTransaction.hpp"
#include "RequestTranslationTransaction.hpp"
#include "SaveStatisticsTransaction.hpp"
#include "PingTransaction.hpp"

namespace androlecture
{
namespace protocol
{
namespace transactions
{

using namespace packets;

Transactions::TransactionsMap Transactions::transactions_;
std::mutex Transactions::mu_;

ITransaction& Transactions::getTransaction(const Packet& packet)
{
    std::lock_guard<std::mutex> lock(mu_);
    const auto& header = dynamic_cast<const TcpHeader&>(packet.getHeader());
    const auto command = header.getCommand();
    auto findResult = transactions_.find(command);
    if (findResult == transactions_.end())
    {
        std::pair<TransactionsMap::iterator, bool> emplaceResult;
        switch (command)
        {
        case Command::ADD_TRANSLATION:
            emplaceResult = transactions_.emplace(
                std::make_pair(command, std::make_unique<AddTranslationTransaction>())
            );
            break;
        case Command::AUTHENTICATE_REQUEST:
            emplaceResult = transactions_.emplace(
                std::make_pair(command, std::make_unique<AuthenticateTransaction>())
            );
            break;

        case Command::GET_TRANSLATIONS:
            emplaceResult = transactions_.emplace(
                std::make_pair(command, std::make_unique<GetTranslationsTransaction>())
            );
            break;

        case Command::DELETE_TRANSLATION:
            emplaceResult = transactions_.emplace(
                std::make_pair(command, std::make_unique<DeleteTranslationTransaction>())
            );
            break;
        case Command::GET_TIMESTAMP:
            emplaceResult = transactions_.emplace(
                std::make_pair(command, std::make_unique<GetTimestampTransaction>())
            );
            break;
        case Command::REQUEST_TRANSLATION:
            emplaceResult = transactions_.emplace(
                std::make_pair(command, std::make_unique<RequestTranslationTransaction>())
            );
            break;
        case Command::SAVE_STATISTICS:
            emplaceResult = transactions_.emplace(
                std::make_pair(command, std::make_unique<SaveStatisticsTransaction>())
            );
            break;
        case Command::PING:
            emplaceResult = transactions_.emplace(
                std::make_pair(command, std::make_unique<PingTransaction>())
            );
            break;
        }
        return *((emplaceResult.first->second).get());
    }
    return *((findResult->second).get());
}

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecture