#ifndef PROTOCOL_TRANSACTIONS_SAVESTATISTICSTRANSACTION_HPP
#define PROTOCOL_TRANSACTIONS_SAVESTATISTICSTRANSACTION_HPP

#include "ITransaction.hpp"
#include <Server/TcpSession.hpp>
#include <Common/Utils/CsvFileWriter.hpp>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

class SaveStatisticsTransaction : public ITransaction
{
public:
    SaveStatisticsTransaction();
    virtual void startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
        const packets::Packet& initialPacket, server::TcpSession& tcpSession) const override;
private:
    mutable common::utils::CsvFileWriter csvFileWriter_;
};

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure

#endif  // PROTOCOL_TRANSACTIONS_SAVESTATISTICSTRANSACTION_HPP