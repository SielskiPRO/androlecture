
#include "RequestTranslationTransaction.hpp"
#include <cstring>
#include <Protocol/Protocol.hpp>
#include <Services/TranslationsService.hpp>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

using namespace server;
using namespace packets;
using namespace services;
using namespace packetoperation;

void RequestTranslationTransaction::startTransaction(const TcpPacketOperation& packetOperation,
        const Packet& initialPacket, TcpSession& tcpSession) const
{
    const auto payload = initialPacket.getPayload();

    unsigned translationId = 0;
    std::memcpy(&translationId, payload, TRANSLATION_ID_SIZE);

    if (TranslationsService::getInstance().doesTranslationExists({translationId}))
    {
        packetOperation.sendPacket(createPacket(Command::RESPONSE_SUCCESS));
        return;
    }
    packetOperation.sendPacket(createPacket(Command::RESPONSE_FAILURE));
}


}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure
