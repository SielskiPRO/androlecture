#ifndef PROTOCOL_TRANSACTIONS_REQUESTTRANSLATIONTRANSACTION_HPP
#define PROTOCOL_TRANSACTIONS_REQUESTTRANSLATIONTRANSACTION_HPP

#include "ITransaction.hpp"
#include <Server/TcpSession.hpp>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

class RequestTranslationTransaction : public ITransaction
{
public:
    virtual void startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
        const packets::Packet& initialPacket, server::TcpSession& tcpSession) const override;
};

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure

#endif  // PROTOCOL_TRANSACTIONS_REQUESTTRANSLATIONTRANSACTION_HPP