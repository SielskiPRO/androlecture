#ifndef PROTOCOL_TRANSACTIONS_ADDTRANSLATIONTRANSACTION_HPP
#define PROTOCOL_TRANSACTIONS_ADDTRANSLATIONTRANSACTION_HPP

#include <string>
#include <Services/Translation.hpp>
#include <Server/TcpSession.hpp>

#include "ITransaction.hpp"

namespace androlecture
{
namespace protocol
{
namespace transactions
{

class AddTranslationTransaction : public ITransaction
{
public:
    AddTranslationTransaction();
    virtual void startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
        const packets::Packet& initialPacket, server::TcpSession& tcpSession) const override;
        
private:
    const services::TranslationInfo decode(const packets::Packet& packet) const;
};

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure

#endif  // PROTOCOL_TRANSACTIONS_ADDTRANSLATIONTRANSACTION_HPP
