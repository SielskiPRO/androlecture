#include "AddTranslationTransaction.hpp"

#include <string>
#include <cstring>

#include <Services/TranslationsService.hpp>

#include <Protocol/Protocol.hpp>

#include <Protocol/Logger.hpp>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

using namespace services;
using namespace packets;
using namespace server;
using namespace protocol;
using namespace server::packetoperation;

namespace
{
    Logger logger{"AddTranslationTransaction"};
}  // namespace


AddTranslationTransaction::AddTranslationTransaction()
{}

void AddTranslationTransaction::startTransaction(const TcpPacketOperation& packetOperation,
    const Packet& initialPacket, TcpSession& tcpSession) const
{
    if (!tcpSession.isAuthenticated())
    {
        logger << error << "Client can't add translation because he is not authenticated";
        packetOperation.sendPacket(createPacket(Command::RESPONSE_FAILURE, "User is not allowed to add translation"));
        return;
    }

    auto& translationsService = TranslationsService::getInstance();
    const auto translationInfo = decode(initialPacket);

    const auto translation = translationsService.addTranslation(std::move(translationInfo), tcpSession);
    if (translation)
    {
        logger << debug << "Added translation! ID of translation: " << translation->getId();
        logger << debug << "Destination port which will be send : " << translation->getDestinationPort();
        logger << info << "Successful translation add!";  
        packetOperation.sendPacket(createPacket(Command::RESPONSE_SUCCESS, translation->getId(), translation->getDestinationPort()));
        return;
    }
    
    logger << warning << "Translation has not been added!";
    packetOperation.sendPacket(createPacket(Command::RESPONSE_FAILURE, "Translation has not been added!"));

}

const TranslationInfo AddTranslationTransaction::decode(const Packet& packet) const
{
    const auto payload = reinterpret_cast<const unsigned char*>(packet.getPayload());

    auto position = 0u;

    unsigned languageLength = static_cast<unsigned>(payload[position++]);
    unsigned descriptionLength = static_cast<unsigned>(payload[position++]);

    std::string language(reinterpret_cast<const char*>(payload + position), languageLength);
    position += languageLength;
    std::string description(reinterpret_cast<const char*>(payload + position), descriptionLength);
    position += descriptionLength;

    unsigned char codec;
    std::memcpy(&codec, payload + position, TRANSLATION_CODEC_SIZE);
    position += TRANSLATION_CODEC_SIZE;

    unsigned char samplingRate;
    std::memcpy(&samplingRate, payload + position, TRANSLATION_SAMPLING_RATE_SIZE);
    position += TRANSLATION_SAMPLING_RATE_SIZE;

    unsigned char voicePacketLength;
    std::memcpy(&voicePacketLength, payload + position, TRANSLATION_VOICE_PACKET_SIZE);
    position += TRANSLATION_VOICE_PACKET_SIZE;

    unsigned int timeToSendStatistics;
    std::memcpy(&timeToSendStatistics, payload + position, TRANSLATION_TIME_TO_STATISTICS);

    return {
        std::move(language), std::move(description),
        std::move(codec), std::move(samplingRate),
        std::move(voicePacketLength), std::move(timeToSendStatistics)
    };
}


}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure

