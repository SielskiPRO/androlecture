
#include "SaveStatisticsTransaction.hpp"

#include <Protocol/Logger.hpp>

#include <Common/Utils/StatisticEntry.hpp>
#include <cmake_constants.hpp>
#include <vector>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

using namespace common::utils;

namespace
{
    Logger logger{"SaveStatisticsTransaction"};
}

SaveStatisticsTransaction::SaveStatisticsTransaction()
: csvFileWriter_(STATS_DIR)
{

}

void SaveStatisticsTransaction::startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
        const packets::Packet& initialPacket, server::TcpSession& tcpSession) const
{
    const auto payload = reinterpret_cast<const char*>(initialPacket.getPayload());
    logger << debug << "Payload size : " << initialPacket.getHeader().getPayloadSize();

    auto position = 0U;

    unsigned numOfStatisticEntries = 0;
    std::memcpy(&numOfStatisticEntries, payload, 4);
    position += 4;

    logger << debug << "Num of statistics entries : " << numOfStatisticEntries;

    if (numOfStatisticEntries == 0)
    {
        logger << debug << "No entries so end of transaction";
        return;
    }

    std::vector<StatisticEntry> entries = {};
    entries.reserve(numOfStatisticEntries);

    for (auto i = 0; i < numOfStatisticEntries; ++i)
    {
        unsigned int numOfEntry = 0;
        std::memcpy(&numOfEntry, payload + position, 4);
        position += 4;

        int wifiSignalStrength = 0;
        std::memcpy(&wifiSignalStrength, payload + position, 4);
        position += 4;

        unsigned avgLatencyStringLength = static_cast<unsigned>(payload[position++]);

        std::string avgLatency(payload + position, avgLatencyStringLength);
        position += avgLatencyStringLength;

        unsigned maxLatency = 0;
        std::memcpy(&maxLatency, payload + position, 4);
        position += 4;

        unsigned minLatency = 0;
        std::memcpy(&minLatency, payload + position, 4);
        position += 4;

        unsigned temporalLatency = 0;
        std::memcpy(&temporalLatency, payload + position, 4);
        position += 4;

        unsigned avgJitterStringLength = static_cast<unsigned>(payload[position++]);

        std::string avgJitter(payload + position, avgJitterStringLength);
        position += avgJitterStringLength;

        unsigned maxJitter = 0;
        std::memcpy(&maxJitter, payload + position, 4);
        position += 4;

        unsigned minJitter = 0;
        std::memcpy(&minJitter, payload + position, 4);
        position += 4;

        unsigned temporalJitter = 0;
        std::memcpy(&temporalJitter, payload + position, 4);
        position += 4;

        unsigned packetLoss = 0;
        std::memcpy(&packetLoss, payload + position, 4);
        position += 4;

        unsigned packetDrop = 0;
        std::memcpy(&packetDrop, payload + position, 4);
        position += 4;

        entries.push_back(
            {
                numOfEntry, wifiSignalStrength, std::move(avgLatency),
                maxLatency, minLatency, temporalLatency, std::move(avgJitter),
                maxJitter, minJitter, temporalJitter, packetLoss, packetDrop
            }
        );
    }

    for (const auto& entry : entries)
    {
        logger << debug << "Entry : " << entry;
    }

    csvFileWriter_.printEntries(entries);

}

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure
