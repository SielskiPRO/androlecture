#ifndef PROTOCOL_TRANSACTIONS_AUTHENTICATETRANSACTION_HPP
#define PROTOCOL_TRANSACTIONS_AUTHENTICATETRANSACTION_HPP

#include "ITransaction.hpp"
#include <Server/TcpSession.hpp>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

class AuthenticateTransaction : public ITransaction
{
public:
    AuthenticateTransaction();
    virtual void startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
        const packets::Packet& initialPacket, server::TcpSession& tcpSession) const override;
};

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure

#endif  // PROTOCOL_TRANSACTIONS_AUTHENTICATETRANSACTION_HPP