
#include "DeleteTranslationTransaction.hpp"

#include <Protocol/Packet/Packet.hpp>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

using namespace packets;

void DeleteTranslationTransaction::startTransaction(const server::packetoperation::TcpPacketOperation& packetOperation,
    const packets::Packet& initialPacket, server::TcpSession& tcpSession) const
{
    tcpSession.deleteTranslation();
    packetOperation.sendPacket(createPacket(Command::RESPONSE_SUCCESS));
}

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecture
