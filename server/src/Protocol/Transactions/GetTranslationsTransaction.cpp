
#include "GetTranslationsTransaction.hpp"
#include <Protocol/Logger.hpp>
#include <Services/TranslationsService.hpp>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

using namespace server::packetoperation;
using namespace services;
using namespace packets;
using namespace server;

namespace
{
    Logger logger{"GetTranslationsTransaction"};
}  // namespace 

GetTranslationsTransaction::GetTranslationsTransaction()
{}

void GetTranslationsTransaction::startTransaction(const TcpPacketOperation& packetOperation,
    const Packet& initialPacket, TcpSession& tcpSession) const
{
    auto& translationsService = TranslationsService::getInstance();
    const auto availableTranslations = translationsService.getTranslationsInfo();
    const auto numOfTranslations = availableTranslations.size();

    packetOperation.sendPacket(createPacket(Command::TRANSLATIONS_COUNT,
        static_cast<unsigned int>(numOfTranslations)));

    if (numOfTranslations == 0)
    {
        return;
    }

    for (const auto& translation : availableTranslations)
    {
        packetOperation.sendPacket(createPacket(Command::TRANSLATION, translation));
    }
}

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure
