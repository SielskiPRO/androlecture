

#include "GetTimestampTransaction.hpp"
#include <Protocol/Logger.hpp>

#include <chrono>

namespace androlecture
{
namespace protocol
{
namespace transactions
{

using namespace std::chrono;
using namespace server::packetoperation;
using namespace packets;
using namespace server;

namespace
{
    Logger logger{"GetTimestampTransaction"};
}  // namespace

GetTimestampTransaction::GetTimestampTransaction()
{}
    
void GetTimestampTransaction::startTransaction(const TcpPacketOperation& packetOperation,
    const Packet& initialPacket, TcpSession& tcpSession) const
{
    unsigned long long ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    packetOperation.sendPacket(createPacket(Command::RESPONSE_SUCCESS, ms));
}

}  // namespace transactions
}  // namespace protocol
}  // namespace androlecure
