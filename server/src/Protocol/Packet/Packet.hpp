#ifndef PROTOCOL_PACKETS_PACKET_HPP
#define PROTOCOL_PACKETS_PACKET_HPP

#include <memory>
#include <utility>

#include <Common/Utils/Functions.hpp>
#include <Common/Utils/Encoding.hpp>

#include <Protocol/Protocol.hpp>
#include "IHeader.hpp"
#include "TcpHeader.hpp"

namespace androlecture
{
namespace protocol
{
namespace packets
{

class Packet
{
public:
    Packet(std::unique_ptr<IHeader> header, std::unique_ptr<unsigned char[]> payload);
    const IHeader& getHeader() const;
    const unsigned char* getPayload() const;
    std::unique_ptr<unsigned char[]> toByteArray() const;
private:
    std::unique_ptr<IHeader> header_;
    std::unique_ptr<unsigned char[]> payload_;
};

inline Packet createPacket(const Command& command)
{
    return Packet(std::make_unique<TcpHeader>(0, command), nullptr);
}

template <typename ...PayloadMembers>
Packet createPacket(const Command& command, PayloadMembers&& ...args)
{
    const auto payloadSize = common::utils::sumBytes(args...);
    auto payload = common::utils::encodeValues(payloadSize, std::forward<PayloadMembers>(args)...);
    return Packet(std::make_unique<TcpHeader>(payloadSize, command), std::move(payload));
}

}  // namespace packets
}  // namespace protocol
}  // namespace androlecture

#endif  // PROTOCOL_PACKETS_IPACKET_HPP
