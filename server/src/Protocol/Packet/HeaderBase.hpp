#ifndef PROTOCOL_PACKETS_HEADERBASE_HPP
#define PROTOCOL_PACKETS_HEADERBASE_HPP

#include "IHeader.hpp"

namespace androlecture
{
namespace protocol
{
namespace packets
{

class HeaderBase : public IHeader
{
public:
    HeaderBase(unsigned long long payloadSize);
    virtual const unsigned long long& getPayloadSize() const override;
protected:
    unsigned long long payloadSize_;
};

}  // namespace packets
}  // namespace protocol
}  // namespace androlecture

#endif  // PROTOCOL_PACKETS_HEADERBASE_HPP
