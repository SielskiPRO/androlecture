#ifndef PROTOCOL_PACKETS_TCPHEADER_HPP
#define PROTOCOL_PACKETS_TCPHEADER_HPP

#include <Protocol/Protocol.hpp>

#include "HeaderBase.hpp"

namespace androlecture
{
namespace protocol
{
namespace packets
{

class TcpHeader : public HeaderBase
{
public:
    TcpHeader(unsigned long long payloadSize, protocol::Command command);
    const protocol::Command& getCommand() const;
    virtual std::unique_ptr<unsigned char[]> toByteArray() const override;
private:
    protocol::Command command_;
};

}  // namespace packets
}  // namespace protocol
}  // namespace androlecture

#endif  // PROTOCOL_PACKETS_TCPHEADER_HPP
