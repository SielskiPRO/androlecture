#ifndef PROTOCOL_PACKETS_IHEADER_HPP
#define PROTOCOL_PACKETS_IHEADER_HPP

#include <memory>

namespace androlecture
{
namespace protocol
{
namespace packets
{

class IHeader
{
public:
    virtual ~IHeader() = default;
    virtual const unsigned long long& getPayloadSize() const = 0;
    virtual std::unique_ptr<unsigned char[]> toByteArray() const = 0;
};

}  // namespace packets
}  // namespace protocol
}  // namespace androlecture

#endif  // PROTOCOL_PACKETS_IHEADER_HPP
