#include <memory>
#include <cstring>

#include <Protocol/Protocol.hpp>

#include <Protocol/Logger.hpp>
#include <Common/Utils/Encoding.hpp>

#include "TcpHeader.hpp"

namespace androlecture
{
namespace protocol
{
namespace packets
{
namespace
{
    Logger logger{"TcpHeader"};
}  // namespace


TcpHeader::TcpHeader(unsigned long long payloadSize, protocol::Command command)
: HeaderBase(payloadSize), command_(std::move(command))
{}

const protocol::Command& TcpHeader::getCommand() const
{
    return command_;
}

std::unique_ptr<unsigned char[]> TcpHeader::toByteArray() const
{
    return common::utils::encodeValues(HEADER_SIZE, static_cast<uint64_t>(payloadSize_), command_);
}

}  // namespace packets
}  // namespace protocol
}  // namespace androlecture

