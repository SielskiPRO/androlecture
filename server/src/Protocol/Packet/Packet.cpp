
#include "Packet.hpp"

#include <cstring>

namespace androlecture
{
namespace protocol
{
namespace packets
{

Packet::Packet(std::unique_ptr<IHeader> header, std::unique_ptr<unsigned char[]> payload)
: header_(std::move(header)), payload_(std::move(payload))
{}

const IHeader& Packet::getHeader() const
{
    return *header_.get();
}

const unsigned char* Packet::getPayload() const
{
    return payload_.get();
}

std::unique_ptr<unsigned char[]> Packet::toByteArray() const
{
    const auto headerByteArray = header_->toByteArray();

    const auto headerByteArrayPtr = headerByteArray.get();
    const auto payloadByteSize = header_->getPayloadSize();
    const auto byteArraySize = HEADER_SIZE + payloadByteSize;
    auto byteArray = std::make_unique<unsigned char[]>(byteArraySize);


    const auto payloadByteArrayPtr = payload_.get();


    auto byteArrayPtr = byteArray.get();

    memcpy(byteArrayPtr, headerByteArrayPtr, HEADER_SIZE);
    
    if (payloadByteArrayPtr != nullptr)
        memcpy(byteArrayPtr + HEADER_SIZE, payloadByteArrayPtr, payloadByteSize);

    return byteArray;
}

}  // namespace packets
}  // namespace protocol
}  // namespace androlecture

