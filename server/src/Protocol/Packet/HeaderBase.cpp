#include <functional>

#include "HeaderBase.hpp"

namespace androlecture
{
namespace protocol
{
namespace packets
{

HeaderBase::HeaderBase(unsigned long long payloadSize)
: payloadSize_(payloadSize)
{}

const unsigned long long& HeaderBase::getPayloadSize() const
{
    return payloadSize_;
}


}  // namespace packets
}  // namespace protocol
}  // namespace androlecture

