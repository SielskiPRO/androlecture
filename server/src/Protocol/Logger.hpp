#ifndef PROTOCOL_LOGGER_HPP
#define PROTOCOL_LOGGER_HPP

#include <Logging/Logger.hpp>

namespace androlecture
{
namespace protocol
{
    using Logger = BaseLogger<EComponent::EComponent_Protocol>;
}  // namespace protocol
}  // namespace androlecture

#endif  // PROTOCOL_LOGGER_HPP