#ifndef PROTOCOL_HPP
#define PROTOCOL_HPP

#include <cstdint>
#include <string>

namespace androlecture
{
namespace protocol
{
 
const std::string PASSWORD = "test";

constexpr unsigned short TCP_PORT = 49152;
constexpr unsigned short DESTINATION_UDP_PORT = 49153;
constexpr unsigned short RECEIVE_UDP_PORT = 49154;
constexpr unsigned short MAX_PACKET_SIZE = 65507;

constexpr unsigned HEADER_SIZE = 9;
constexpr unsigned PAYLOAD_SIZE_FIELD_SIZE = 8;
constexpr unsigned COMMAND_TYPE_FIELD_SIZE = 1;

constexpr unsigned TRANSLATION_INFO_PADDING = 2;
constexpr unsigned TRANSLATION_CODEC_SIZE = 1;
constexpr unsigned TRANSLATION_SAMPLING_RATE_SIZE = 1;
constexpr unsigned TRANSLATION_VOICE_PACKET_SIZE = 1;

constexpr unsigned TRANSLATION_TIME_TO_STATISTICS = 4;
constexpr unsigned TRANSLATION_ID_SIZE = 4;

enum class Command : uint8_t
{
    ADD_TRANSLATION         = 0x01,
    DELETE_TRANSLATION      = 0x02,
    GET_TRANSLATIONS        = 0x03,
    TRANSLATIONS_COUNT      = 0x04,
    TRANSLATION             = 0x05,
    REQUEST_TRANSLATION     = 0x06,
    AUTHENTICATE_REQUEST    = 0x07,
    PING                    = 0x08,
    GET_TIMESTAMP           = 0x09,
    SAVE_STATISTICS         = 0x0A,
    RESPONSE_SUCCESS        = 0x0B,
    RESPONSE_FAILURE        = 0x0C
};

template <typename Src>
inline Command cast(Src&& src)
{
    return static_cast<Command>(src);
}

inline std::string commandToString(const Command& command)
{
    switch (command)
    {
        case Command::ADD_TRANSLATION:
            return "ADD_TRANSLATION";
            break;
        case Command::DELETE_TRANSLATION:
            return "DELETE_TRANSLATION";
            break;
        case Command::GET_TRANSLATIONS:
            return "GET_TRANSLATIONS";
            break;
        case Command::TRANSLATIONS_COUNT:
            return "TRANSLATIONS_COUNT";
            break;
        case Command::REQUEST_TRANSLATION:
            return "REQUEST_TRANSLATION";
            break;
        case Command::AUTHENTICATE_REQUEST:
            return "AUTHENTICATE_REQUEST";
            break;
        case Command::PING:
            return "PING";
            break;
        case Command::GET_TIMESTAMP:
            return "GET_TIMESTAMP";
            break;
        case Command::SAVE_STATISTICS:
            return "SAVE_STATISTICS";
            break;
        case Command::RESPONSE_SUCCESS:
            return "RESPONSE_SUCCESS";
            break;
        case Command::RESPONSE_FAILURE:
            return "RESPONSE_FAILURE";
            break;
        default:
            return "UNKNOWN COMMAND";
            break;
    }
}

}  // namespace protocol
}  // namespace androlecture

#endif  // PROTOCOL_HPP
