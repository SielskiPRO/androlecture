
#include "command_line_parser.hpp"

namespace androlecture
{

using namespace boost::program_options;

Parser prepareParser(const int& argc, const char** argv)
{
    options_description description{"Options"};
    description.add_options()
        ("help,h", "Help screen")
        ("collect_stats", value<bool>()->default_value(false), "Collect statistics");

    variables_map vm;
    store(parse_command_line(argc, argv, description), vm);
    notify(vm);
    return {vm, description};
}

}  // namespace androlecture
