
#include <Server/TcpServer.hpp>
#include <Protocol/Protocol.hpp>
#include "Logger.hpp"
#include "command_line_parser.hpp"

using namespace androlecture;
using namespace androlecture::server;
using namespace androlecture::protocol;

namespace
{
    Logger logger{"main"};

    void loop()
    {
        auto c = 0;
        do
        {
            std::cout << "Press ESC to end program!" << std::endl;
            c = getchar();
        } while (c != 27);
    }
}  // namespace

int main(int argc, const char** argv)
{  
    try {
        const auto parser = prepareParser(argc, argv);
        if (parser.vm.count("help"))
            logger << debug << parser.description;
        else if (parser.vm.count("collect_stats"))
        {
            TcpServer tcpServer(TCP_PORT, parser.vm["collect_stats"].as<bool>());
            loop();
        }
    } catch (const boost::program_options::error& ex) {
        logger << error << ex.what();
        return 1;
    }
    return 0;
}


