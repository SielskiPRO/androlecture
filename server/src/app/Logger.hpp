#ifndef APP_LOGGER_HPP
#define APP_LOGGER_HPP

#include <Logging/Logger.hpp>

namespace androlecture
{
    using Logger = BaseLogger<EComponent::EComponent_App>;
}  // namespace androlecture

#endif  // APP_LOGGER_HPP