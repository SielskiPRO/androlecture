#ifndef APP_COMMAND_LINE_PARSER_HPP
#define APP_COMMAND_LINE_PARSER_HPP

#include <boost/program_options.hpp>

namespace androlecture
{

void onCollectStatistics(const bool& collectStatistics);

struct Parser
{
    boost::program_options::variables_map vm;
    boost::program_options::options_description description;
};

Parser prepareParser(const int& argc, const char** argv);

}  // namespace androlecture


#endif  // APP_COMMAND_LINE_PARSER_HPP