\chapter{Design of the communication protocol}

The second chapter devoted to the theoretical aspects contains description of transport protocols used in computer networks. It describes what the communication protocol is. Due to the fact that this chapter is devoted to the description of the created protocol for the purpose of enabling communication between devices that are part of the created system, the definition of the communication protocol will be presented again. 

A communication protocol is a set of rules on the basis of which devices participating in the communication process between each other are able to correctly interpret incoming data and transmit data in a format that allows interpretation of data by a second device. Without any specific rules as to the format and meaning of individual data that is sent, incoming data is the usual bit sequence for the device. 

The communication protocol should clearly define the structure of the sent message along with the intended use of individual message segments. Defining the message structure consists of defining: 
\begin{enumerate}[label=\textbullet]
	\item the number of required fields in the message structure,
	\item the size of each required information field,
	\item values which each information field can store.
\end{enumerate}

An example of a message structure can be an IP protocol header. It is divided into many fields that store a specific type of information being transferred. Each field in the IP protocol header also has its own size defined.

In addition to defining the structure of messages sent between devices, the protocol should also define the appropriate course of communication. Communication between devices is aimed at calling the right actions in applications that communicate with each other. The execution of the target action on devices can be preceded by the exchange of many messages between devices which configures the application to perform a given task.

The fourth chapter is devoted to the protocol description which was created for the purpose of communication between mobile devices and the server application in the system of simultaneous speech translation. The chapter contains a description of:
\begin{enumerate}[label=\textbullet]
	\item requirements that a created protocol must meet,
	\item protocol structure,
	\item information fields included in the message structure,
\end{enumerate}

\section{Communication protocol requirements}

This section is devoted to the description of requirements that the created protocol must meet in order to enable the system to function properly. Protocol requirements can be classified as non-functional requirements, because the requirements that the protocol must meet describe how the system works. The requirements are presented in the table \ref{tab:protocol_requirements}.

\begin{table}[htb] \small
\centering
\caption{Table describing requirements of the created protocol}
\label{tab:protocol_requirements}
\begin{tabularx}{\textwidth}{|L|X|}\hline
\rowcolor{Snow}
Non-Functional Requirement No. & \multicolumn{1}{|c|}{Non-Functional Requirement Description} \\ \hline
NFR28						   & The protocol should allow mobile applications and server application to send information packets in accordance with specific rules \\ \hline
NFR29						   & The protocol should support responding to customer requests with information about result of request or with potential errors on the server application side \\ \hline
NFR30						   & The protocol should support sending a password necessary for user authorization in the system \\ \hline
NFR31						   & The protocol should support sending information about the translation which is going to be added in the system \\ \hline
NFR32						   & The protocol should support the removal of the translation if the client request's this behaviour \\ \hline
NFR33						   & The protocol should support the retrieval of informations about available translations in the system \\ \hline
NFR34						   & The protocol should support the transmission of messages describing the client's request regarding the desire to receive the selected transmission \\ \hline
NFR35						   & The protocol should support messages used to collect and send statistics to the server from mobile applications.\\ \hline
NFR36 						   & The protocol should support sending messages containing voice data \\ \hline
NFR37 						   & The protocol should be independent of the protocol used for transport purposes (TCP or UDP) \\ \hline
NFR38 						   & The data should be packed in the message in a little endian byte order \\ \hline
\end{tabularx}
\end{table}

The designed protocol operates at the top of the protocols belonging to the transport layer. It serves to create an unambiguous system of interpretation of transmitted data from the mobile application to the server application.

\section{Protocol message structure}
This section describes the message structure that uses the designed protocol to define the rules for its interpretation by the device intended to receive this message. The description of the message structure consists of its graphical representation and the description of fields that make up the entire message.

The data sent in the system using a defined protocol is represented as a packet. The packet consists of a protocol header and a data segment which contains data intended for the application which should receive it. This structure shows similarity with the message structures used in other protocols such as TCP and UDP, which also add their headers. The defined protocol adds another layer of abstraction to provide mechanisms for the interpretation of bit sequences sent by the devices.

\subsection{General message structure}
The figure \ref{fig:general_protocol_structure} shows the general graphic form of the message which structure is composed using rules of the created protocol.

\begin{figure}[ht]
	\centering 
	\includegraphics[width=1\linewidth]{rys04/general_protocol_structure}
	\caption{General structure of message defined by the protocol rules}
	\label{fig:general_protocol_structure}
\end{figure}

Description of general message structure:
\begin{description}
	\item[Protocol header] \hfill \\
	9-byte part of the message used to inform the application about the size of the data to be received and the type of command sent by the application.\\
	\item[Payload size] \hfill \\
	The 8-byte portion of the message header informing the application that receives the message of the size of the message to be received. Messages transported 		using the TCP protocol can be received by the application in the form of several segments or in the form of the whole message. This is due to the fact that the TCP protocol sends data in the form of a data stream. The receiving application must know the size of the entire message to be received.\\
	\item[Command type] \hfill \\
	8-bit field storing the type of message being sent. The application receiving the message must recognize the type of data contained in the message to start the proper processing of the message in relation to its content. The table \ref{tab:command_field_description} shows all permitted values with their explanation that this information field can accept.
\clearpage
\begin{table}[htb] \small
\centering
\caption{Allowed values of Command Type information field}
\label{tab:command_field_description}
\begin{tabularx}{\textwidth}{|c|c|X|}\hline
\rowcolor{Snow}
Value & Name of the flag & Purpose of the flag\\ \hline
0x01 & ADD\textunderscore TRANSLATION & The flag informs the server application that the message is a request to add a new sound transmission service\\ \hline
0x02 & DELETE\textunderscore TRANSLATION	& 
The flag informs the server application that the client has sent a request to delete the sound transmission service. A message with this flag does not have a data segment	\\ \hline
0x03 & GET\textunderscore TRANSLATIONS		&  The flag informs the server application that the client has sent a request to download information from the server about the available sound transmission services.	\\ \hline
0x04 & TRANSLATIONS\textunderscore COUNT	& The flag informs the mobile application about the number of available audio transmissions\\ \hline
0x05 & TRANSLATION			& Thef flag informs the mobile application that the message contains information about the available translation\\ \hline
0x06 & REQUEST\textunderscore TRANSLATION & The flag informs the server application that the message contains a user's query about the availability of the application \\ \hline
0x07 & AUTHENTICATE\textunderscore REQUEST	&	The flag informs the server application that the message contains a password entered by the user for authorization purpose\\ \hline
0x08 & PING					& The flag informs the server application that the message is a packet checking the connection with the server\\ \hline
0x09 & GET\textunderscore TIMESTAMP		&	The flag informs the server application that the message is a mobile application request in order to obtain a timestamp from the server application\\ \hline
0x0A & SAVE\textunderscore STATISTICS		& The flag informs the server application that the message contains statistics collected by the mobile application\\ \hline
0x0B & RESPONSE\textunderscore SUCCESS		& The flag informs the mobile application that the message is a confirmation of a successful operation performed in the server application.	\\ \hline
0x0C & RESPONSE\textunderscore FAILURE		& The flag informs the mobile application that the message is the response of the server which signals that the server application could not perform certain actions.\\ \hline
\end{tabularx}
\end{table}

	\item[Data] \hfill  \\
	It is a field containing data related to a given type of request. This field may contain the original message that has been sent, or it may contain further information fields in the case of complex types of requests. Requests that store the original message under this field or does not have payload at all are those which use following flags:
\begin{enumerate}[label=\textbullet]
	\item DELETE\textunderscore TRANSLATION,
	\item GET\textunderscore TRANSLATIONS,
	\item TRANSLATIONS\textunderscore COUNT,
	\item REQUEST\textunderscore TRANSLATION
	\item AUTHENTICATE\textunderscore REQUEST
	\item PING,
	\item GET\textunderscore TIMESTAMP,
	\item RESPONSE\textunderscore SUCCESS,
	\item RESPONSE\textunderscore FAILURE.
\end{enumerate}
Other types of messages use a more complex message structure. In their description, description of header structure have been skipped because it contains the same fields as in the general message structure which is described above.
\end{description}


\subsection{Message structure of client request which adds new translation}
The figure \ref{fig:add_translation_protocol_structure} shows the structure of the message used to start the procedure of adding a new sound transmission service on the server application side.
\begin{figure}[ht]
	\centering 
	\includegraphics[width=1\linewidth]{rys04/add_translation_protocol_structure}
	\caption{Structure of message containing informations about translation which is going to be added}
	\label{fig:add_translation_protocol_structure}
\end{figure}

Description of message structure containing informations required for adding new voice transmission service:
\begin{description}
	\item[Language size] \hfill \\
	8-bit field containing information about the number of characters describing the language of translation.\\
	\item[Description size] \hfill \\
	8-bit field containing information about the number of characters describing the optional description of translation.\\
	\item[Langague and description data] \hfill \\
	A sequence of 8 to 510 bits representing characters forming a representation of the language of translation and its optional description.\\
	\item[Codec Type] \hfill \\
	8-bit field representing the type of codec used for speech compression or its absence. The value of this field is mapped by the mobile application as shown in the table \ref{tab:codec_mapping}
\begin{table}[htb] \small
\centering
\caption{Mapping of Codec Type field value to codec type}
\label{tab:codec_mapping}
\begin{tabular}{|c|c|}\hline
\rowcolor{Snow}
Field value & Codec\\ \hline
0 & No codec\\ \hline
1 &	G711 µlaw\\ \hline
2 & G711 alaw\\ \hline
3 &	OPUS\\ \hline
\end{tabular}
\end{table}
	\item[Sampling Rate] \hfill \\
	8-bit field representing the sampling frequency used in the transmission. The value of this field is mapped by the mobile application as shown in the table \ref{tab:frequency_mapping}
\begin{table}[htb] \small
\centering
\caption{Mapping of Sampling Rate field value to frequency value}
\label{tab:frequency_mapping}
\begin{tabular}{|c|c|}\hline
\rowcolor{Snow}
Field value & Frequency [kHz]\\ \hline
0 & 8\\ \hline
1 &	12\\ \hline
2 & 16\\ \hline
3 &	24\\ \hline
4 &	48\\ \hline
\end{tabular}
\end{table}	
	\item[Voice length] \hfill \\
	8-bit field representing the length of the sound frame sent in one voice packet.T he value of this field is mapped by the mobile application as shown in the table \ref{tab:voice_length_mapping}
\begin{table}[htb] \small
\centering
\caption{Mapping of Voice length field value to voice frame length}
\label{tab:voice_length_mapping}
\begin{tabular}{|c|c|}\hline
\rowcolor{Snow}
Field value & Voice frame length [ms]\\ \hline
0 & 20\\ \hline
1 &	40\\ \hline
2 & 60\\ \hline
\end{tabular}
\end{table}		
	\item[Time To Send Statistics] \hfill \\
	4-byte field representing the time in seconds after which statistics collected from the phones receiving this transmission should be sent to the server 				application
\end{description}

\subsection{Message structure of server response containing information about available voice transmission service}
The figure \ref{fig:transmission_service_information_protocol_structure} shows the structure of the message which is the reponse of the server containing information about specific voice transmission service.
\begin{figure}[ht]
	\centering 
	\includegraphics[width=1\linewidth]{rys04/transmission_service_information_protocol_structure}
	\caption{Structure of message containing informations about available translation}
	\label{fig:transmission_service_information_protocol_structure}
\end{figure}

Description of message structure containing information about available voice transmission service:
\begin{description}
	\item[Translation ID] \hfill \\
	A 4-byte field representing the ordinal number of a given audio transmission.\\
	\item[IP Group Address] \hfill \\
	A 4-byte field representing decimal value od IP multicast group address to which all voice packets of this transmission will be send.\\
	\item[Receive Port] \hfill \\
	A 2-byte field representing decimal value of port on which voice packets will be send.\\
\end{description}
Fields which have been described in sections above were not described again.

\subsection{Justification of the selected message structure}

The message structures presented above that are sent in the system in the form of internet packages ensure their correct reception and interpretation by devices included in the system. 

Each message starts with the protocol header, which defines the payload size and the number indicating the message type. This form of header structure is required for two reasons:
\begin{description}
	\item[Correct receipt of the packet] \hfill \\
	The TCP communication protocol is used to send messages containing user commands. The TCP protocol sends data streams, so that the message can be received in several portions. The application receiving TCP packets must know the size of the whole message in order to receive it in its entirety. Thanks to the information about the size of the entire packet in the header, the application can receive data and in case the entire message is not received, it will wait for the rest of the message to arrive.\\
	\item[Correct message processing] \hfill \\
	After receiving the entire packet containing the message, the application that received the packet must specify how to process the received data. By including such a message in the form of one byte information in the message header, the application that received the packet can correctly process the data contained in the received packet.
\end{description}

