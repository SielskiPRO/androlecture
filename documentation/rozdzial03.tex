\chapter{Model of the system}

Before proceeding with the implementation of any IT system, it is good practice to prepare a system model. Its purpose is to prepare a clear, legible description of the project, specifying its purpose and requirements that it must meet. A well-designed system plan avoids any confusion during the project implementation process, because the tasks that the system should carry out are defined in advance. For this purpose, a special modeling languages were created which are described in this chapter of the master thesis.

In large corporations dealing with the creation of IT solutions, there are special teams of people who for a long period of time prepare a detailed project plan including the preparation of a list of functionalities that the system should provide. For this purpose, they have constant contact with the client, which defines the requirements of the given application from the point of view of the future user of the system. The process of preparing a system plan can take more time than just implementing it.

After collecting a number of requirements for the system being created, the next point in creating the system plan is to create a model of the IT system operation from the point of view of creating software that is to determine the operation of the entire project.

This chapter describes the model of the system created, taking into account the requirements set for it. In order to create the model, commonly used tools were used to describe business processes and describe the operation of the system in terms of the programming approach that was used to create individual modules of the system.

\section{Description of the created system with its application in the real world}

An IT system that was created to implement the subject of the master thesis is used for simultaneous translations sending through a wireless computer network using Wi-Fi technology. The entire system consists of two independent applications:
\begin{enumerate}[label=\textbullet]
	\item a server application operating on the Raspberry Pi 3 model B+ platform,
	\item a mobile application that works on devices based on the Android platform.
\end{enumerate}

The idea of the created system is presented in the figure \ref{fig:system_concept}.
\begin{figure}[ht]
	\centering 
	\includegraphics[width=0.8\linewidth]{rys03/system_concept}
	\caption{Graphical representation of a simultaneous translation system}
	\label{fig:system_concept}
\end{figure}
The created system provides audio transmission between devices included in the system. Input and output devices for sound and user actions are mobile devices such as smartphones and tablets. The Raspberry Pi device serves as a server device on which the server application works. The tasks of the server application is to receive and send responses to client requests and voice retransmission. The actions provided for system users include adding new voice transmissions and receiving existing ones. An important requirement of the system is the possibility of simultaneous transmission of many translations, which resulted into a high degree of complexity of the server application. Mobile applications also serve as a source of data used for research purposes of this master thesis. In order to enable communication between mobile devices and the mobile application, a communication protocol has been created that defines the way in which data sent in the system should be interpreted.

The created system can be used during lectures, during which the participants of the lecture may not know the language in which the lecture is delivered. In this case, language interpreters using their smartphones can transmit their language interpretations using their smartphones to the server application. The server application will retransmit these voice streams in a real time to the smartphones which requested specific translation. In this way, the people participating in the lecture who do not know the language in which the lecture is conducted can understand it with the help of an interpreter who gives a final translation.

The advantage of this system is that there may be many translators who run independent translations in different languages. The lecture participant can choose any translation and listen to it using his mobile device with the use of headphones. Without such a system, the lecture can be maximally translated into one chosen language, because simultaneous speech is not possible without any system transmitting sound in a way that does not dissipate other translations.

\section{Description of system requirements}

One of the IT system planning processes is gathering and writing down the requirements that should be met by the system that has to be created. Requirements for the system are divided into:
\begin{description}
	\item[Functional requirements (FR)] \hfill \\
	Functional requirements determine the functionalities of the created software. They can be divided into functional requirements, requirements for system behavior and data requirements. A functional requirement is a requirement regarding the result of system behavior that should be provided by a system function.
	\item[Non-functional requirements (NFR)] \hfill \\
	Non-functional requirements determine the desired features of the created system and often have a more significant impact on the choice of architecture for the system being created than the functional requirements. These requirements apply to how the system should perform its tasks.
\end{description}

This section lists the functional and non-functional requirements for the created system. Separately, lists of requirements for the server application, the mobile application and the communication protocol have been prepared. Functional and non-functional requirements are presented in the form of tables. Each entry in the table represents a single requirement and consists of a ordinal number and a description of the requirement.

\subsection{Description of server application and device requirements}

This section describes functional and non-functional requirements for a server application and the host device for this application. The server application's task is management of translations including adding new ones, deleting existing translations and forwarding voice streams from users who broadcast their voice transmissions. It is an intermediary application between end devices in the system which are mobile devices. The table \ref{tab:server_functional_requirements} lists the functional requirements that the server application should implement.

\begin{table}[htb] \small
\centering
\caption{Table describing the functional requirements of the server application}
\label{tab:server_functional_requirements}
\begin{tabularx}{\textwidth}{|L|X|}\hline
\rowcolor{Snow}
Functional Requirement No. & \multicolumn{1}{|c|}{Functional Requirement Description} \\ \hline
FR1 					   &		The server application should allow mobile applications to connect to it using a network connection\\ \hline
FR2						   &		The server application should be able to support many clients at the same time\\ \hline
FR3						   &	    The server application should send responses appropriate to the types of client requests\\ \hline
FR4                        &        The server application should authorize the user who wants to add their translation service\\ \hline
FR5					       &		The server application should allow the addition of new transmissions by authorized users\\ \hline
FR6						   &        The server application should enable the removal of available translations by their owners\\ \hline
FR7						   &		The server application should send information describing available translations\\ \hline
FR8					   &		Server-based application should allow clients to choose an available translation\\ \hline
FR9						   &		The server application should support concurrently many audio transmissions\\ \hline
FR10                           &        The server application should detect broken connections with mobile applications of users transmitting translations\\ \hline
FR11						   &        The server application should delete translations whose users have lost connection with the application\\ \hline
FR12						   &		The server application should receive sent statistics from the mobile applications of users who receive broadcast transmissions\\ \hline
FR13						   &		The server application should save the collected statistics in the non-volatile memory of the device hosting this application\\ \hline
FR14 						   &        The server should have a configurable option that indicates whether statistics should be collected\\ \hline
FR15						   &        The server application should implement a logger used to debug the application\\ \hline
\end{tabularx}
\end{table}

The table \ref{tab:server_non_functional_requirements} lists non-functional requirements that should be satisfied during the program implementation process of the application and device configuration. Some of non-functional requirements also apply to the Raspberry Pi device, which is the host for the server application.
\begin{table}[htb] \small
\centering
\caption{Table describing the non-functional requirements of the server application and device}
\label{tab:server_non_functional_requirements}
\begin{tabularx}{\textwidth}{|L|X|}\hline
\rowcolor{Snow}
Non-Functional Requirement No. & \multicolumn{1}{|c|}{Non-Functional Requirement Description} \\ \hline 
NFR1					   &    The device which hosts the server application should be configured in the standalone Wi-Fi LAN network\\ \hline
NFR2					   &    The host device should be configured as a DHCP server\\ \hline
NFR3					   &	IP addresses assigned to mobile devices connected to the network should be allocated for one hour\\ \hline
NFR4					   &    Support for many clients on the server application side should be implemented using multiple threads\\ \hline
NFR5					   &    The server application should communicate with mobile devices in accordance with the rules of the designed communication protocol\\ \hline
NFR6					   &    User authorization is done by verifying the password which is sent by the user\\ \hline
NFR7					   &    Detection of a broken connection from the client should be performed by sending "keepalive" packets via the TCP protocol\\ \hline
NFR8					   & 	The server application should save the received statistics in the form of a text file\\ \hline
NFR9					   &    Setting the option enabling the collection of statistics by the server should be carried out by the argument of the program call\\ \hline
NFR10 					   &    The logger's functionality should contain four levels of message types (INFO, ERROR, WARNING, DEBUG). Messages with the INFO type should be saved in a text file \\ \hline
\end{tabularx}
\end{table}

\subsection{Description of mobile application requirements}
The above section describes functional and non-functional requirements for a server-side application. An equally important part of the created system is a mobile application that has two roles in the created system:
\begin{description}
	\item[The role of the interface between the system and the user] \hfill \\
The mobile application provides a simple graphical interface for the user to communicate with the server application. With this graphical interface, the user can enter data into the system required for connection, authorization and adding a new translation service. Using the graphic interface, the user is also able to choose one of the available translations.
	\item[The role of the data source in the system] \hfill \\
The mobile application is also the source of all information used in the system. Each addition of a new translation involves sending the corresponding data 	via a computer network.
\end{description}
The table \ref{tab:mobile_functional_requirements} presents a list of functional requirements relating to the mobile application.
\clearpage
\begin{table}[htb] \small
\centering
\caption{Table describing the functional requirements of the mobile application}
\label{tab:mobile_functional_requirements}
\begin{tabularx}{\textwidth}{|L|X|}\hline
\rowcolor{Snow}
Functional Requirement No. & \multicolumn{1}{|c|}{Functional Requirement Description} \\ \hline
FR16 					   &	The mobile application should be able to establish a connection with the server application	\\ \hline
FR17						   &	The mobile application should enable the user to enter the data necessary to create a connection	\\ \hline
FR18						   &	The mobile application should make it possible to choose the listener mode or the interpreter mode    \\ \hline
FR19                        &    The mobile application should enable the user to download informations about currently available translations from the server application  \\ \hline
FR20					       &	The mobile application should clearly present the available translations to the user\\ \hline
FR21						   &    The mobile application should provide the user with the method of selecting any available translation service\\ \hline
FR22						   &	The mobile application should provide an interface intended to enter the data necessary for authorization purposes	\\ \hline
FR23					       &	The mobile application should provide an interface for entering data about the added translation service \\ \hline
FR24					   &	The mobile application should provide an interface to manage the process of receiving voice transmissions \\ \hline
FR25                       &    The mobile application should provide an interface for managing the translation.    \\ \hline
FR26					   &    The mobile application should provide sound compression mechanisms. \\ \hline
FR27					   &	The mobile application should be able to transmit voice to the server application \\ \hline
FR28					   &	Mobile application should inform the user in case of application errors and significant events	\\ \hline
FR29 					   &    The mobile application in the listener mode should collect data for the purpose of producing statistics for research purposes  \\ \hline
FR15					   &    The mobile application should send collected statistics to the server \\ \hline
\end{tabularx}
\end{table}

The table \ref{tab:mobile_non_functional_requirements} presents a list of non-functional requirements relating to the mobile application and the device.
\clearpage
\begin{table}[htb] \small
\centering
\caption{Table describing non-functional requirements of the mobile application and device}
\label{tab:mobile_non_functional_requirements}
\begin{tabularx}{\textwidth}{|L|X|}\hline
\rowcolor{Snow}
Non-Functional Requirement No. & \multicolumn{1}{|c|}{Non-Functional Requirement Description} \\ \hline
NFR11					   &	The version of the Android software on which the application runs should be greater than or equal to 4.1 (Jelly Bean)\\ \hline
NFR12					   &    To connect to the server, the user must provide the IP address and port number on which the server application works\\ \hline
NFR13					   &    For authorization purposes, the user must enter the correct password\\ \hline
NFR14					   &    When adding a new transmission service, the user must specify the transmission language (max 255 characters) and the optional description (max 255 characters)\\ \hline
NFR15					   &  In order to add a transmission with the statistics collection mode enabled, the user must provide the following data:
\begin{enumerate}[label=\textbullet]
	\item type of speech compression codec (no codec, G711 or OPUS),
	\item speech sampling frequency (8, 12, 16, 24, 48) [kHz]
	\item the length of the sound frame (5ms, 10ms, 20 ms, 60ms) [ms]
	\item time after which the mobile device sends the collected data to the server [s]
\end{enumerate}  \\ \hline
NFR16					   &	If the option to collect statistics is enabled, data should be collected in intervals of one second. The following data should be collected:
\begin{enumerate}[label=\textbullet]
	\item the average value of voice packet delays [ms],
	\item the minimal value of the voice packet delay [ms],
	\item the maximal value of the voice packet delay [ms],
	\item the minimal jiter value between voice packets [ms],
	\item the maximal jiter value between voice packets [ms],
	\item number of lost packets,
	\item the number of skipped packets.
\end{enumerate}

\\ \hline
NFR17					   &   The sound transmission service management interface should provide options to:
\begin{enumerate}[label=\textbullet]
	\item start the sound transmission,
	\item pause the sound transmission,
	\item remove the translation service.
\end{enumerate}
\\ \hline
NFR18					   &    The sound receive service management interface should provide option to stop listening currently choosen translation\\ \hline
NFR19					   &    A mobile application in the event of a phone call should stop the process of transmitting the translation\\ \hline
NFR20					   &    The process of broadcasting speech transmission should be active after minimizing the application and when the mobile device goes into sleep mode\\ \hline
\end{tabularx}
\end{table}

\subsection{Description of common system requirements}
This section contains requirements that are common for both server and mobile applications. Common requirements are non-functional requirements that describe how the system should work. The described common system requirements define the basic assumptions of the system operation. Requirements define, among others, the method by which the devices that make up the system communicate with each other and for what purpose the transport layer protocols were used.. These non-functional requirements are described in the table \ref{tab:common_non_functional_requirements}.
\clearpage
\begin{table}[htb] \small
\centering
\caption{Table describing common non-functional requirements for the whole system}
\label{tab:common_non_functional_requirements}
\begin{tabularx}{\textwidth}{|L|X|}\hline
\rowcolor{Snow}
Non-Functional Requirement No. & \multicolumn{1}{|c|}{Non-Functional Requirement Description} \\ \hline
NFR21					   &	The connection between the server application and the mobile application should be made using the TCP protocol\\ \hline
NFR22					   &    The sound transmission should be transported using the UDP protocol\\ \hline
NFR23					   &    Network communication should be done using a Wi-Fi network\\ \hline
NFR24					   &    The system user can simultaneously select only one transmission of translation\\ \hline
NFR25					   &    The system user can be the owner of only one translation\\ \hline
NFR26					   &	The user should be informed about errors and notifications of the application using the pop-up information window\\ \hline
NFR27 					   &    All operations using data transmission over the network should be executed in separate threads \\ \hline
\end{tabularx}
\end{table}

\section{Business model of the system}

One of the ways to present the scheme of operation of a given system is to present it using the business process modeling method. Business process modeling is a graphical way of presenting a business process or workflow. For the graphic presentation, most types of diagrams are used. 

The use of the business process modeling technique allows system developers to better understand how processes work within the system. Graphical representation of the processes included in a given system allows for possible improvements of the entire process, which in the end may translate into increased efficiency and the reliability of the entire system. Using this method, standardization of presenting the principle of system operation is achieved. When a large number of people work on the system, the standardization of the presentation of the system's operation is an indispensable requirement. This prevents misunderstandings during the creation of the system resulting from the heterogeneous interpretation of the various ways of presenting the flow of processes.

In this master thesis, Business Process Modeling Notation (BPMN) was used to present the flowchart of the most key processes present in the created system.

BPMN is a graphical notation used to describe business processes. The owner of this method of representation of business processes is the Object Management Group (OMG) consortium. The current version of the BPMN standard is version 2.0 and it is constantly developed by the OMG consortium.

The advantage of using BPMN notation is that it uses specific elements to represent the course of business processes. The standard defines many elements that can be used in the description of the process, which makes it homogeneous.

In order to create diagrams in the BPMN notation, an open-source software called Camunda Modeller was used.

\subsection{Diagrams of business processes present in the system}

This section presents business models of processes occurring in the created system of simultaneous speech translation. The following business processes were prepared using diagrams prepared in the BPMN notation:
\begin{enumerate}[label=\textbullet]
	\item the process of establishing a connection between a mobile application and a simultaneous speech translation server,
	\item the process of user authorization
	\item the process of adding a new translation service in the system,
	\item the process of requesting a specific available translation service,
\end{enumerate}


\subsubsection{Process of enstablishing the TCP connection between the mobile application and the server application}
The figure \ref{fig:connection_process_bpmn} shows a business process of establishing a connection between a mobile application and a server application using the TCP protocol.
\begin{figure}[ht]
	\centering 
	\includegraphics[width=1\linewidth ,height=10cm]{rys03/connection_process_bpmn}
	\caption{Business process of enstablishing the connection between mobile application and the server application}
	\label{fig:connection_process_bpmn}
\end{figure}

The process of establishing a connection between mobile applications and a server application is implemented via the TCP protocol. The connection obtained in this way is used to send commands in the system. The TCP protocol ensures that every command sent from the mobile application will be delivered to the server and that each server refresh will be delivered to the client. In order to establish a connection with the server, the client must provide the IP address and port on which the server application on the Raspberry Pi device operates. After entering this data in the appropriate fields of the graphical interface, the mobile application waits for two seconds to obtain a connection to the server. If the connection is established, the mobile application will move the user to the application mode selection panel. If the application does not receive an answer from the server within two seconds, the application will inform the user about problems with the connection using the dialog window.

\subsubsection{Process of user authentication}
The figure \ref{fig:authentication_process_bpmn} shows a business process of authentication process of a user who wants to add its own translation service.
\begin{figure}[ht]
	\centering 
	\includegraphics[width=1\linewidth ,height=10cm]{rys03/authentication_process_bpmn}
	\caption{Business process of authorization of a user}
	\label{fig:authentication_process_bpmn}
\end{figure}

The user's authentication process consists of entering the password into the intended text field in the graphical interface. This password is then sent via the network to the server application. The server application compares the transferred password with the saved password in the application code. The user's authentication result is sent in the response packet by the server to the mobile application. In case of a valid password, the user is transferred by the application to the add translation screen. Otherwise, the user is informed about entering the wrong password.

\subsubsection{Process of adding the new translation service to the system}
The figure \ref{fig:add_translation_process_bpmn} shows a business process of adding a new translation service to the system.
\begin{figure}[ht]
	\centering 
	\includegraphics[width=1\linewidth, ,height=10cm]{rys03/add_translation_process_bpmn}
	\caption{Business process of adding a new translation}
	\label{fig:add_translation_process_bpmn}
\end{figure}

The process of adding a new translation service is much more complicated than the two previously described processes. The user who wants to add a new sound transmission must fill in the appropriate fields. A detailed description of the fields is provided in the table. The data describing the translation to be added are packaged in the form of a packet in accordance with the created communication protocol, which was described later in the master thesis.

After receiving this package by the server, it checks whether the user who wants to add this translation has been previously authorized. The next step is that the server assigns the original multicast address to the translating service being added. The last step on the server side is to associate the added translation with the TCP session of the user who added the translation. As a response, the server sends information about the multicast address that has been assigned to the added translation and the information about the port on which the server expects to receive packets containing the transmitted voice.

If the translation service is correctly added, the mobile application moves the user to the sound transmission control panel. The mobile application creates a special thread, whose task is to record the sound from the microphone, its compression if the codec has been selected and the transmission of packets containing voice data. In case of failure, the user is informed about it by a dialog window.

\subsubsection{Process of requesting availabe translation service by a user}
The figure \ref{fig:choose_translation_bpmn} shows a business process consisting in requesting choosen translation service by user
\begin{figure}[ht]
	\centering 
	\includegraphics[width=1\linewidth, ,height=10cm]{rys03/choose_translation_bpmn}
	\caption{Business process of choosing translation service by a user}
	\label{fig:choose_translation_bpmn}
\end{figure}

The process of selecting the translation whose transmission the user wants to receive consists of downloading information about available translations from the server application. In the absence of available translations, the user will be informed about it using the dialog. In the case of available sound transmission services, they are presented in the form of a list presenting the language of the translation and an optional description if it exists.

The user can choose any translation by clicking on it. The mobile application sends a packet containing the appropriate sequence number of the translation that has been selected for the server. The server checks if the selected translation still exists and sends a response containing this information. In the case where a given translation exists, the user is transferred to the screen for managing the process of receiving the translation. The mobile application creates a special thread that receives incoming voice data in the form of packets. Then, if the sent voice data has been subjected to the compression process, it decompresses them and plays them to the user.

If the selected transmission does not exist on the server, the application informs the user about this fact with a special dialog.

\subsection{Use case diagram of created system}

Use case diagrams are used to graphically represent system functionalities, including system users which are called actors. Use case diagrams describe which system functionalities are available to individual actors and show the dependencies between individual functionalities. Use case diagrams are part of the Unified Modelling Language (UML).

Unified Modeling Language is a semi-formal language used to model various types of systems. Like the BPMN notation, it is developed by the OMG consortium. UML is very often used in IT system modeling due to the large number of different types of diagrams that can be used to describe the operation of algorithms and the description of functionalities provided by information systems. 

The use of the UML language for system modeling purposes makes it possible to standardize the graphic presentation of systems, thus ensuring unambiguous interpretation of the created models.

The tool that was used in order to prepare use case diagrams is Visual Paradigm. It is the environment which can be used to prepare a lot of different graphs for the modelling structures of different systems.

In the created system of simultaneous speech translation, three actors can be distinguished:
\begin{enumerate}[label=\textbullet]
	\item the user of the system, hereinafter referred to as the \textbf{System User}
	\item the user receiving the sound transmission, hereinafter referred to as the \textbf{Listener},
	\item the user using the application in the audio transmission mode, hereinafter referred to as the \textbf{Translator},
	\item the server application , hereinafter referred to as the \textbf{Server Application}.
\end{enumerate}


The figure \ref{fig:usecase_diagram} shows a created scheme of use cases. It describes the functionality of the entire system along with the relationships between the actors and the functionalities which are available in the created system.
\clearpage
\begin{figure}[ht]
	\centering 
	\includegraphics[width=1\linewidth, height=10cm]{rys03/uml/usecase_diagram}
	\caption{Use case diagram presenting functionalities provided for user by mobile application}
	\label{fig:usecase_diagram}
\end{figure}