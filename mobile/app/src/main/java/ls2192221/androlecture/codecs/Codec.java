package ls2192221.androlecture.codecs;

public interface Codec {
    int encode(short input[], byte[] output);
    int decode(byte input[], short[] output);
}
