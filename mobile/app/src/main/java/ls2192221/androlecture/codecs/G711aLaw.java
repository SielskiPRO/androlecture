package ls2192221.androlecture.codecs;

public class G711aLaw extends G711 implements Codec {
    @Override
    public int encode(short[] input, byte[] output) {
        int i = 0;
        for (short inputSample : input) {
            output[i++] = (byte)linear2alaw(inputSample);
        }
        return input.length;
    }

    @Override
    public int decode(byte[] input, short[] output) {
        int i = 0;
        for (byte inputSample : input) {
            output[i++] = (short)alaw2linear(inputSample);
        }
        return 2 * output.length;
    }
}
