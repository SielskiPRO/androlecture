package ls2192221.androlecture.codecs;

import ls2192221.androlecture.jniwrappers.OpusDecoder;
import ls2192221.androlecture.jniwrappers.OpusEncoder;

public class Opus implements Codec {

    private OpusEncoder encoder;
    private OpusDecoder decoder;

    public Opus(int samplingFrequency, int numOfChannels, int frameSize) {
        encoder = new OpusEncoder();
        encoder.init(samplingFrequency, numOfChannels, frameSize);
        decoder = new OpusDecoder();
        decoder.init(samplingFrequency, numOfChannels, frameSize);
    }


    @Override
    public int encode(short[] input, byte[] output) {
        return encoder.encode(input, output);
    }

    @Override
    public int decode(byte[] input, short[] output) {
        return decoder.decode(input, output);
    }
}
