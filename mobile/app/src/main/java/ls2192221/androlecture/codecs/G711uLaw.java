package ls2192221.androlecture.codecs;

public class G711uLaw extends G711 implements Codec {
    @Override
    public int encode(short[] input, byte[] output) {
        int i = 0;
        for (short inputSample : input) {
            output[i++] = (byte)linear2ulaw(inputSample);
        }
        return input.length;
    }

    @Override
    public int decode(byte[] input, short[] output) {
        int i = 0;
        for (byte inputSample : input) {
            output[i++] = (short)ulaw2linear(inputSample);
        }
        return 2 * output.length;
    }
}
