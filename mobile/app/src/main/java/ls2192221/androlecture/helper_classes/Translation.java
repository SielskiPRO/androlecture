package ls2192221.androlecture.helper_classes;

import android.os.Parcel;
import android.os.Parcelable;

import ls2192221.androlecture.enums.Codec;
import ls2192221.androlecture.enums.SamplingFrequency;
import ls2192221.androlecture.enums.VoicePacketLength;

public class Translation implements Parcelable {
    public static final String TAG = "Translation";
    private int id;
    private int destinationPort;
    private int receivePort;
    private String ipGroupAddress;
    private String translationLanguage;
    private String translationDescription;
    private String codec;
    private String samplingRate;
    private String voicePacketLength;
    private int timeToSendStatistics;


    public Translation(int id, int receivePort, String ipGroupAddress, String translationLanguage,
                       String translationDescription, int codec, int samplingRate, int voicePacketLength ,int timeToSendStatistics) {
        this.id = id;
        this.receivePort = receivePort;
        this.ipGroupAddress = ipGroupAddress;
        this.translationLanguage = translationLanguage;
        this.translationDescription = translationDescription;
        this.codec = Codec.valueOf(codec).toString();
        this.samplingRate = SamplingFrequency.valueOf(samplingRate).toString();
        this.voicePacketLength = VoicePacketLength.valueOf(voicePacketLength).toString();
        this.timeToSendStatistics = timeToSendStatistics;
    }

    public Translation(int id, int destinationPort, String translationLanguage,
                       String translationDescription, String codec, String samplingRate, String voicePacketLength ,int timeToSendStatistics) {
        this.id = id;
        this.destinationPort = destinationPort;
        this.translationLanguage = translationLanguage;
        this.translationDescription = translationDescription;
        this.codec = codec;
        this.samplingRate = samplingRate;
        this.voicePacketLength = voicePacketLength;
        this.timeToSendStatistics = timeToSendStatistics;
    }

    public int getId() {
        return id;
    }

    public int getDestinationPort() {
        return destinationPort;
    }

    public int getReceivePort() {
        return receivePort;
    }

    public String getIpGroupAddress() {
        return ipGroupAddress;
    }

    public String getTranslationLanguage() {
        return translationLanguage;
    }

    public String getTranslationDescription() {
        return translationDescription;
    }

    public String getCodec() {
        return codec;
    }

    public String getSamplingRate() {
        return samplingRate;
    }

    public String getVoicePacketLength() {
        return voicePacketLength;
    }

    public int getTimeToSendStatistics() {
        return timeToSendStatistics;
    }

    @Override
    public String toString() {
        return "Translation{" +
                "id=" + id +
                ", destinationPort=" + destinationPort +
                ", receivePort=" + receivePort +
                ", ipGroupAddress='" + ipGroupAddress + '\'' +
                ", translationLanguage='" + translationLanguage + '\'' +
                ", translationDescription='" + translationDescription + '\'' +
                ", codec='" + codec + '\'' +
                ", samplingRate=" + samplingRate +
                ", voicePacketLength=" + voicePacketLength +
                ", timeToSendStatistics=" + timeToSendStatistics +
                '}';
    }

    protected Translation(Parcel in) {
        id = in.readInt();
        destinationPort = in.readInt();
        receivePort = in.readInt();
        ipGroupAddress = in.readString();
        translationLanguage = in.readString();
        translationDescription = in.readString();
        codec = in.readString();
        samplingRate = in.readString();
        voicePacketLength = in.readString();
        timeToSendStatistics = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(destinationPort);
        dest.writeInt(receivePort);
        dest.writeString(ipGroupAddress);
        dest.writeString(translationLanguage);
        dest.writeString(translationDescription);
        dest.writeString(codec);
        dest.writeString(samplingRate);
        dest.writeString(voicePacketLength);
        dest.writeInt(timeToSendStatistics);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Translation> CREATOR = new Parcelable.Creator<Translation>() {
        @Override
        public Translation createFromParcel(Parcel in) {
            return new Translation(in);
        }

        @Override
        public Translation[] newArray(int size) {
            return new Translation[size];
        }
    };
}