package ls2192221.androlecture.helper_classes;

public class StatisticEntry implements Sendable{

    private static int instanceCounter = 0;

    private int numOfEntry;

    private int wifiSignalStrength;

    private double avgLatency;
    private int maxLatency;
    private int minLatency;
    private int temporalLatency;

    private double avgJitter;
    private int maxJitter;
    private int minJitter;
    private int temporalJitter;

    private int packetLoss;
    private int packetDrop;

    public static void resetCounter() {
        instanceCounter = 0;
    }

    public StatisticEntry(int wifiSignalStrength, double avgLatency, int maxLatency, int minLatency, int temporalLatency, double avgJitter, int maxJitter, int minJitter, int temporalJitter, int packetLoss, int packetDrop) {
        ++instanceCounter;
        this.numOfEntry = instanceCounter;
        this.wifiSignalStrength = wifiSignalStrength;
        this.avgLatency = avgLatency;
        this.maxLatency = maxLatency;
        this.minLatency = minLatency;
        this.temporalLatency = temporalLatency;
        this.avgJitter = avgJitter;
        this.maxJitter = maxJitter;
        this.minJitter = minJitter;
        this.temporalJitter = temporalJitter;
        this.packetLoss = packetLoss;
        this.packetDrop = packetDrop;
    }

    @Override
    public byte[] toByteArray() {
        byte serialized [] = new byte [
            4     // size of observation num
            +   4 // size of wifi signal strength
            +   1 // size of field for representing length of stringified average latency
            +   String.valueOf(avgLatency).length() // size of stringified avg latency
            +   4 // size of max latency
            +   4 // size of min latency
            +   4 // size of temporal latency
            +   1 // size of field for representing length of stringified average jitter
            +   String.valueOf(avgJitter).length()
            +   4
            +   4
            +   4
            +   4
            +   4
        ];

        int i = 0;

        /* SERIZALIZATION OF ENTRY NUMBER*/
        serialized[i++] = (byte)numOfEntry;
        serialized[i++] = (byte)(numOfEntry >> 8);
        serialized[i++] = (byte)(numOfEntry >> 16);
        serialized[i++] = (byte)(numOfEntry >> 24);

        /* SERIZALIZATION OF WIFI SIGNAL*/
        serialized[i++] = (byte)(wifiSignalStrength);
        serialized[i++] = (byte)(wifiSignalStrength >> 8);
        serialized[i++] = (byte)(wifiSignalStrength >> 16);
        serialized[i++] = (byte)(wifiSignalStrength >> 24);

        /* SERIALIZATION OF AVERAGE LATENCY */
        String avgLatencyString = String.valueOf(avgLatency);
        int avgLatencyLength = avgLatencyString.length();
        serialized[i++] = (byte)(avgLatencyLength);
        System.arraycopy(avgLatencyString.getBytes(), 0, serialized, i, avgLatencyLength);

        i += avgLatencyLength;

        /* SERIALIZATION OF MAX LATENCY */
        serialized[i++] = (byte)(maxLatency);
        serialized[i++] = (byte)(maxLatency >> 8);
        serialized[i++] = (byte)(maxLatency >> 16);
        serialized[i++] = (byte)(maxLatency >> 24);

        /* SERIALIZATION OF MIN LATENCY */
        serialized[i++] = (byte)(minLatency);
        serialized[i++] = (byte)(minLatency >> 8);
        serialized[i++] = (byte)(minLatency >> 16);
        serialized[i++] = (byte)(minLatency >> 24);

        /* SERIALIZATION OF TEMPORAL LATENCY */
        serialized[i++] = (byte)(temporalLatency);
        serialized[i++] = (byte)(temporalLatency >> 8);
        serialized[i++] = (byte)(temporalLatency >> 16);
        serialized[i++] = (byte)(temporalLatency >> 24);

        /* SERIALIZATION OF AVERAGE JITTER */
        String avgJitterString = String.valueOf(avgJitter);
        int avgJitterLength = avgJitterString.length();
        serialized[i++] = (byte)(avgJitterLength);
        System.arraycopy(avgJitterString.getBytes(), 0, serialized, i, avgJitterLength);

        i += avgJitterLength;

        /* SERIALIZATION OF MAX JITTER */
        serialized[i++] = (byte)(maxJitter);
        serialized[i++] = (byte)(maxJitter >> 8);
        serialized[i++] = (byte)(maxJitter >> 16);
        serialized[i++] = (byte)(maxJitter >> 24);

        /* SERIALIZATION OF MIN JITTER */
        serialized[i++] = (byte)(minJitter);
        serialized[i++] = (byte)(minJitter >> 8);
        serialized[i++] = (byte)(minJitter >> 16);
        serialized[i++] = (byte)(minJitter >> 24);

        /* SERIALIZATION OF TEMPORAL JITTER */
        serialized[i++] = (byte)(temporalJitter);
        serialized[i++] = (byte)(temporalJitter >> 8);
        serialized[i++] = (byte)(temporalJitter >> 16);
        serialized[i++] = (byte)(temporalJitter >> 24);

        /* SERIALIZATION OF PACKET LOSS */
        serialized[i++] = (byte)(packetLoss);
        serialized[i++] = (byte)(packetLoss >> 8);
        serialized[i++] = (byte)(packetLoss >> 16);
        serialized[i++] = (byte)(packetLoss >> 24);

        /* SERIALIZATION OF PACKET DROP */
        serialized[i++] = (byte)(packetDrop);
        serialized[i++] = (byte)(packetDrop >> 8);
        serialized[i++] = (byte)(packetDrop >> 16);
        serialized[i] = (byte)(packetDrop >> 24);


        return serialized;
    }

    public int getWifiSignalStrength() {
        return wifiSignalStrength;
    }

    public double getAvgLatency() {
        return avgLatency;
    }

    public int getMaxLatency() {
        return maxLatency;
    }

    public int getMinLatency() {
        return minLatency;
    }

    public int getTemporalLatency() {
        return temporalLatency;
    }

    public double getAvgJitter() {
        return avgJitter;
    }

    public int getMaxJitter() {
        return maxJitter;
    }

    public int getMinJitter() {
        return minJitter;
    }

    public int getTemporalJitter() {
        return temporalJitter;
    }

    public int getPacketLoss() {
        return packetLoss;
    }

    public int getPacketDrop() {
        return packetDrop;
    }
}
