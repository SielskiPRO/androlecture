package ls2192221.androlecture.helper_classes;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import ls2192221.androlecture.fragments.ManageListeningFragment;

public class StatisticsCollector {
    public static final String TAG = "StatisticsCollector";

    private double avgLatency;
    private int maxLatency;
    private int minLatency;
    private int previousPacketLatency;
    private long sumOfLatencies;

    private boolean isUpdatingStatistics = false;

    private double avgJitter;
    private int maxJitter;
    private int minJitter;
    private int previousJitter;
    private long sumOfJitters;

    private long previousPacketId;
    private int packetLoss;
    private int packetDrop;
    private Handler handler;
    private WifiManager wifiManager;
    private WifiInfo wifiInfo;

    private long numOfObservation;

    private ArrayList<StatisticEntry> entries = new ArrayList<>();

    public StatisticsCollector(Context applicationContext) {
        wifiManager = (WifiManager) applicationContext.getSystemService(Context.WIFI_SERVICE);
        wifiInfo = wifiManager.getConnectionInfo();
    }


    public ArrayList<StatisticEntry> getEntries() {
        synchronized (entries) {
            ArrayList<StatisticEntry> returnEntries;
            returnEntries = new ArrayList<>(entries);
            entries.clear();
            StatisticEntry.resetCounter();
            return returnEntries;
        }
    }

    public void stopUpdatingStatistics() {
        isUpdatingStatistics = false;
    }

    public void startUpdatingStatistics(long packetId) {

        if (!isUpdatingStatistics) {
            previousPacketId = packetId;
            isUpdatingStatistics = true;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (isUpdatingStatistics) {
                        try {
                            Thread.sleep(1000);
                            wifiInfo = wifiManager.getConnectionInfo();
                            int wifiSignalStrength = wifiInfo.getRssi();
                            StatisticEntry entry = new StatisticEntry(wifiSignalStrength,
                                    avgLatency, maxLatency, minLatency, previousPacketLatency,
                                    avgJitter, maxJitter, minJitter, previousJitter,
                                    packetLoss, packetDrop);

                            synchronized (entries) {
                                entries.add(entry);
                            }

                            Utils.sendMessageToHandler(handler, ManageListeningFragment.UPDATE_STATISTICS_FIELDS, entry);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    StatisticEntry.resetCounter();
                }
            }).start();
        }

    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public void packetDropped()
    {
        ++packetDrop;
    }

    public void packetArrived(long receiveTimestamp, long packetTimestamp, long packetId) {
        ++numOfObservation;
        Log.d(TAG, "Num of observation : " + numOfObservation);
        int latencyOfPacket = handleLatencies(receiveTimestamp, packetTimestamp);
        handlePacketLoss(packetId);
        Log.d(TAG, "Latency of packet : " +latencyOfPacket);
        handleJitters(latencyOfPacket);
        previousPacketLatency = latencyOfPacket;
    }

    private void handlePacketLoss(long packetId)
    {
        if (packetId == (previousPacketId + 1) || packetId == previousPacketId || packetId == 0) {
            previousPacketId = packetId;
            return;
        } else
        {
            Log.d(TAG, "Packets have been lost");
            Log.d(TAG, "Previous packet ID : " + previousPacketId);
            Log.d(TAG, "Income packet ID : " + packetId);
            packetLoss += packetId - previousPacketId - 1;
        }

        previousPacketId = packetId;
    }

    private int handleLatencies(long receiveTimeStamp, long packetTimestamp) {

        int latencyOfPacket = (int)(receiveTimeStamp - packetTimestamp);

        if (numOfObservation == 1) {
            minLatency = latencyOfPacket;
            maxLatency = latencyOfPacket;
        } else if (latencyOfPacket < minLatency) {
            minLatency = latencyOfPacket;
        } else if (latencyOfPacket > maxLatency) {
            maxLatency = latencyOfPacket;
        }
        sumOfLatencies += latencyOfPacket;
        avgLatency = BigDecimal.valueOf((double)sumOfLatencies / numOfObservation)
                .setScale(2, RoundingMode.HALF_UP).doubleValue();
        Log.d(TAG, "Average latency : " + avgLatency);

        return latencyOfPacket;
    }

    private void handleJitters(int latencyOfPacket) {
        if (numOfObservation > 1) {
            int jitterBetweenPackets = Math.abs(latencyOfPacket - previousPacketLatency);

            if (numOfObservation == 2) {
                minJitter = jitterBetweenPackets;
                maxJitter = jitterBetweenPackets;
            } else if (jitterBetweenPackets < minJitter) {
                minJitter = jitterBetweenPackets;
            } else if (jitterBetweenPackets > maxJitter) {
                maxJitter = jitterBetweenPackets;
            }

            sumOfJitters += jitterBetweenPackets;
            avgJitter = BigDecimal.valueOf(((double)sumOfJitters) / (numOfObservation -1))
                    .setScale(2, RoundingMode.HALF_UP).doubleValue();
            previousJitter = jitterBetweenPackets;

            Log.d(TAG, "Average Jitter : " + avgJitter);
        }
    }
}
