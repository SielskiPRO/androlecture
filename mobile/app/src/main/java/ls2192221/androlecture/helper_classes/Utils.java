package ls2192221.androlecture.helper_classes;

import android.app.Activity;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.os.Handler;
import android.os.Message;

import androidx.appcompat.app.AlertDialog;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.StringJoiner;

import ls2192221.androlecture.R;

public class Utils {
    public static final String TAG = "Utils";
    public enum NotificationType {
        ERROR,
        INFO
    }

    public static void sendMessageToHandler(Handler handler, int what, Object obj) {
        Message msg = new Message();
        msg.what = what;
        if (obj != null) {
            msg.obj = obj;
        }
        handler.sendMessage(msg);
    }

    public static NetworkInterface findWifiNetworkInterface() {

        Enumeration<NetworkInterface> enumeration = null;

        try {
            enumeration = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        NetworkInterface wlan0 = null;

        while (enumeration.hasMoreElements()) {

            wlan0 = enumeration.nextElement();

            if (wlan0.getName().equals("wlan0")) {
                return wlan0;
            }
        }

        return null;
    }

    public static AlertDialog showNotificationDialog(String notificationDetailed, NotificationType type,
                                              Activity context) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        View mView = context.getLayoutInflater().inflate(R.layout.notification_dialog, null);
        ((TextView) mView.findViewById(R.id.notificationTypeTextView)).setText(type.toString());
        ((TextView) mView.findViewById(R.id.notificationDetailedTextView)).setText(notificationDetailed);
        switch (type) {
            case ERROR:
                mView.findViewById(R.id.notificationDialogLayout).setBackgroundColor(Color.parseColor("#E91E63"));
                break;
            case INFO:
                mView.findViewById(R.id.notificationDialogLayout).setBackgroundColor(Color.parseColor("#0000FF"));
                break;
        }
        mBuilder.setView(mView);
        AlertDialog dialog = mBuilder.create();
        dialog.show();
        return dialog;
    }

    public static <E extends Enum<E>>
    String[] getEnumNames(Class<E> e) {
        ArrayList<String> strings = new ArrayList<>();
        for (E en : EnumSet.allOf(e)) {
            strings.add(en.toString());
        }

        Object[] objNames = strings.toArray();
        return Arrays.copyOf(objNames, objNames.length, String[].class);
    }

    public static boolean isSamplingRateSupportedForRecording(int samplingRate) {

        int bufferSize = AudioRecord.getMinBufferSize(samplingRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        if (bufferSize > 0) {
            Log.d(TAG, bufferSize + "");
            return true;
        }
        Log.d(TAG, bufferSize + "");
        return false;
    }

    public static boolean isSamplingRateSupportedForPlaying(int samplingRate) {

        int bufferSize = AudioTrack.getMinBufferSize(samplingRate, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
        if (bufferSize > 0) {
            Log.d(TAG, bufferSize + "");
            return true;
        }
        Log.d(TAG, bufferSize + "");
        return false;
    }

    public static String decimalIpToString(long decimalIpAddress) {

        short firstOctet = (short) ((decimalIpAddress >> 24) & 0xFF);
        short secondOctet = (short) ((decimalIpAddress >> 16) & 0xFF);
        short thirdOctet = (short) ((decimalIpAddress >> 8) & 0xFF);
        short fourthOctet = (short) ((decimalIpAddress) & 0xFF);

        String stringRepresentation;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            StringJoiner joiner = new StringJoiner(".");
            joiner.add(String.valueOf(firstOctet));
            joiner.add(String.valueOf(secondOctet));
            joiner.add(String.valueOf(thirdOctet));
            joiner.add(String.valueOf(fourthOctet));

            stringRepresentation = joiner.toString();
        } else {
            stringRepresentation = "" + firstOctet + "." + secondOctet + "."
                    + thirdOctet + "." + fourthOctet;
        }

        return stringRepresentation;
    }

}
