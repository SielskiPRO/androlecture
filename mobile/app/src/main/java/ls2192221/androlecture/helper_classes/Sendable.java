package ls2192221.androlecture.helper_classes;

public interface Sendable {
    byte[] toByteArray();
}
