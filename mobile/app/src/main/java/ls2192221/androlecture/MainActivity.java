package ls2192221.androlecture;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import androidx.fragment.app.FragmentTransaction;
import android.util.Log;

import ls2192221.androlecture.fragments.AddTranslationFragment;
import ls2192221.androlecture.fragments.ChooseModeFragment;
import ls2192221.androlecture.fragments.ConnectToServerFragment;
import ls2192221.androlecture.fragments.ManageListeningFragment;
import ls2192221.androlecture.fragments.ManageTranslationFragment;
import ls2192221.androlecture.services.ListenerService;
import ls2192221.androlecture.services.TCPService;
import ls2192221.androlecture.services.TranslatorService;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "MainActivity";

    private TCPService mTcpService;
    private TranslatorService mTranslatorService;
    private ListenerService mListenerService;

    private PowerManager.WakeLock wakeLock;
    private WifiManager.WifiLock wifiLock;
    private WifiManager.MulticastLock multicastLock;


    private boolean isTCPServiceBind = false;
    private boolean isTranslatorServiceBind = false;
    private boolean isListenerServiceBind = false;

    private boolean hasBeenMovedToBackground = false;
    private AlertDialog dialog;
    private OnListenerServiceBind onListenerServiceBind;
    private OnTranslatorServiceBind onTranslatorServiceBind;

    public interface OnListenerServiceBind {
        void onServiceBind(ListenerService listenerService);
    }

    public interface  OnTranslatorServiceBind {
        void onServiceBind(TranslatorService translatorService);
    }

    public void registerListenerFragmentCallback(OnListenerServiceBind frag) {
        onListenerServiceBind = frag;
    }

    public void registerTranslatorFragmentCallback(OnTranslatorServiceBind frag) {
        onTranslatorServiceBind = frag;
    }

    private ServiceConnection mTcpConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            TCPService.LocalBinder localBinder = (TCPService.LocalBinder)service;
            mTcpService =  localBinder.getService();
            isTCPServiceBind = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isTCPServiceBind = false;
        }
    };
    private ServiceConnection mTranslatorConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            TranslatorService.LocalBinder localBinder = (TranslatorService.LocalBinder)service;
            mTranslatorService =  localBinder.getService();
            isTranslatorServiceBind = true;
            onTranslatorServiceBind.onServiceBind(mTranslatorService);

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isTranslatorServiceBind = false;
        }
    };

    private ServiceConnection mListenerConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ListenerService.LocalBinder localBinder = (ListenerService.LocalBinder)service;
            mListenerService =  localBinder.getService();
            isListenerServiceBind = true;
            onListenerServiceBind.onServiceBind(mListenerService);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isListenerServiceBind = false;
        }

    };

    public boolean isTCPServiceBind() {
        return isTCPServiceBind;
    }

    public boolean isTranslatorServiceBind() {
        return isTranslatorServiceBind;
    }

    public boolean isListenerServiceBind() {
        return isListenerServiceBind;
    }

    public void setDialog(AlertDialog dialog){
        this.dialog = dialog;
    }

    public void replaceCurrentFragment(Fragment newFragment, String fragmentName, String transactionName){
        FragmentTransaction transaction;
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, newFragment, fragmentName);
        transaction.addToBackStack(transactionName);
        transaction.commit();
    }

    public void replaceCurrentNoBackStack(Fragment newFragment){
        FragmentTransaction transaction;
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, newFragment, null);
        transaction.commit();
    }

    public void backToLogin(){
        FragmentManager fm = getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        replaceCurrentNoBackStack(new ConnectToServerFragment());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PowerManager powerManager = (PowerManager)getApplicationContext().getSystemService(Context.POWER_SERVICE);
        WifiManager wifi = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "power:Lock");
        wakeLock.setReferenceCounted(true);
        wakeLock.acquire();

        wifiLock = wifi.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "wifilock");
        wifiLock.acquire();

        multicastLock = wifi.createMulticastLock("multicastLock");
        multicastLock.setReferenceCounted(true);
        multicastLock.acquire();

        replaceCurrentNoBackStack(new ConnectToServerFragment());
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, TCPService.class);
        startService(intent);
        bindService(intent, mTcpConnection, Context.BIND_AUTO_CREATE);
    }

    public TCPService getTcpService() {
        return mTcpService;
    }
    public TranslatorService getTranslatorService() { return mTranslatorService; }
    public ListenerService getListenerService() { return mListenerService; }

    public ServiceConnection getTranslatorConnection() { return mTranslatorConnection; }
    public ServiceConnection getListenerConnection() { return mListenerConnection; }

    public void destroyTranslatorService() {
        if (isTranslatorServiceBind){
            unbindService(mTranslatorConnection);
            isTranslatorServiceBind = false;
            stopService(new Intent(this, TranslatorService.class));
        }
    }

    public void destroyListenerService() {
        if (isListenerServiceBind) {
            unbindService(mListenerConnection);
            isListenerServiceBind = false;
            stopService(new Intent(this, ListenerService.class));
        }
    }

    public void destroyTcpService() {
        if (isTCPServiceBind) {
            unbindService(mTcpConnection);
            isTCPServiceBind = false;
            stopService(new Intent(this, TCPService.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mTcpService != null && hasBeenMovedToBackground && mTcpService.isConnected())
            replaceCurrentFragment(new ChooseModeFragment(), ChooseModeFragment.TAG, ChooseModeFragment.TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
        destroyTcpService();
        destroyTranslatorService();
        destroyListenerService();
        wakeLock.release();
        wifiLock.release();
        multicastLock.release();
        if (dialog != null)
            dialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        String lastFragmentName = null;
        if (fm.getBackStackEntryCount() > 0){
            lastFragmentName = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
        }
        if (lastFragmentName != null){
            if (fm.findFragmentByTag(AddTranslationFragment.TAG) != null && lastFragmentName.equals(AddTranslationFragment.TAG)){

                fm.popBackStack(ChooseModeFragment.TAG, 0);
                return;

            } else if (lastFragmentName.equals(ChooseModeFragment.TAG)){
                moveTaskToBack (true);
                hasBeenMovedToBackground = true;
                return;
            } else if (lastFragmentName.equals(ManageTranslationFragment.TAG) || lastFragmentName.equals(ManageListeningFragment.TAG)) {
                Log.d(TAG, "translation or listening fragment");
                return;
            }
            super.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}
