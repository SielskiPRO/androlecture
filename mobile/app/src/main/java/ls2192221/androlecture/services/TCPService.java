package ls2192221.androlecture.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

import java.net.UnknownHostException;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;

import ls2192221.androlecture.fragments.ConnectToServerFragment;
import ls2192221.androlecture.helper_classes.Utils;
import ls2192221.androlecture.packet_creation.PacketCreator;
import ls2192221.androlecture.protocol.Command;
import ls2192221.androlecture.protocol.ProtocolSpecification;

public class TCPService extends Service {

    private static final String CHANNEL_ID = "tcpservice";
    private String TAG = "TCPService";
    private Socket socket;

    private OutputStream socketOutput;
    private InputStream socketInput;

    private boolean isConnected = false;
    private final IBinder mBinder = new LocalBinder();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(TAG)
                .setContentText("Tcp service")
                .setSmallIcon(R.drawable.id_android)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() tcp service");
        closeConnection();
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void closeConnection()
    {
        try {
            if (socket != null)
                socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (socketOutput != null)
                socketOutput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (socketInput != null)
                socketInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        isConnected = false;
        Log.d(TAG, "closeConnection()");
    }

    public void connectToServer(String ipAddress, String port, Handler handler){

        InetAddress address;
        try {
            address = InetAddress.getByName(ipAddress);
        } catch(UnknownHostException e){
            Utils.sendMessageToHandler(handler, ConnectToServerFragment.CONNECTION_FAILURE, "IP address of the server could not be determined!");
            return;
        }

        if (null == port || port.equals("")) {
            Utils.sendMessageToHandler(handler, ConnectToServerFragment.CONNECTION_FAILURE, "Please provide port number!");
            return;
        }

        try{
            socket = new Socket();
            socket.connect(new InetSocketAddress(address, Integer.valueOf(port)), 2500);
            socket.setTcpNoDelay(true);
            socket.setReuseAddress(true);
        } catch(IOException e) {
            Utils.sendMessageToHandler(handler, ConnectToServerFragment.CONNECTION_FAILURE, "Failed to connect to the server!");
            return;
        }
        try {
            socketOutput = socket.getOutputStream();
            socketInput = socket.getInputStream();
        } catch (IOException e) {
            Utils.sendMessageToHandler(handler, ConnectToServerFragment.CONNECTION_FAILURE, "Failed to get buffers from socket!");
            return;
        }

        if (socketInput == null) {
            Log.d(TAG, "Stream is null");
            return;
        }

        sendBytes(PacketCreator.createPacket(Command.PING, null));
        byte[] response = receiveBytes();

        if (response != null){
            isConnected = true;
            Utils.sendMessageToHandler(handler, ConnectToServerFragment.CONNECTION_SUCCESSFUL, response[9] == 1);
            return;
        } else {
            closeConnection();
            Utils.sendMessageToHandler(handler, ConnectToServerFragment.CONNECTION_FAILURE, "Did not get response from the server!");
            return;
        }
    }

    public synchronized boolean sendBytes(byte[] bytes){
        try
        {
            socketOutput.write(bytes);
        } catch (IOException e){
            e.printStackTrace();
            closeConnection();
            Log.d(TAG, "Error on bytes send!");
            return false;
        }
        return true;
    }

    public byte[] receiveBytes()
    {
        Log.d(TAG, "Stared receiving packet");

        byte[] headerBuffer = new byte[ProtocolSpecification.TCP_HEADER_SIZE];

        int bytesReceived = 0;
        int bytesReaded = 0;
        int howManyMoreToRead = ProtocolSpecification.TCP_HEADER_SIZE;

        while (bytesReceived != ProtocolSpecification.TCP_HEADER_SIZE) {

            try {
                bytesReaded = socketInput.read(headerBuffer, bytesReceived, howManyMoreToRead);
            } catch (IOException e) {
                closeConnection();
                return null;
            }
            if (bytesReaded < 0){
                closeConnection();
                return null;
            }
            bytesReceived += bytesReaded;
            howManyMoreToRead -= bytesReaded;
        }

        Log.d(TAG, "Received header");

        int payloadSize = 0;
        payloadSize |= headerBuffer[0] & 0xFF;
        payloadSize |= ((headerBuffer[1] & 0xFF) << 8);
        payloadSize |= ((headerBuffer[2] & 0xFF) << 16);
        payloadSize |= ((headerBuffer[3] & 0xFF) << 24);
        payloadSize |= ((headerBuffer[4] & 0xFF) << 32);
        payloadSize |= ((headerBuffer[5] & 0xFF) << 40);
        payloadSize |= ((headerBuffer[6] & 0xFF) << 48);
        payloadSize |= ((headerBuffer[7] & 0xFF) << 56);

        if (payloadSize == 0)
            return headerBuffer;

        byte[] payloadBuffer = new byte[payloadSize];

        bytesReceived = 0;
        bytesReaded = 0;
        howManyMoreToRead = payloadSize;

        while (bytesReceived != payloadSize){
            try {
                bytesReaded = socketInput.read(payloadBuffer, bytesReceived, howManyMoreToRead);
            } catch (IOException e) {
                closeConnection();
                return null;
            }
            if (bytesReaded < 0){
                closeConnection();
                return null;
            }
            bytesReceived += bytesReaded;
            howManyMoreToRead -= bytesReaded;
        }

        Log.d(TAG, "Received payload");

        byte[] packet = new byte[ProtocolSpecification.TCP_HEADER_SIZE + payloadSize];
        Log.d(TAG, "Before copying header!");
        System.arraycopy(headerBuffer, 0, packet, 0, headerBuffer.length);
        Log.d(TAG, "Before copying payload!");
        System.arraycopy(payloadBuffer, 0, packet, headerBuffer.length, payloadBuffer.length);

        Log.d(TAG, "Will return packet!");

        return packet;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public TCPService getService(){
            return TCPService.this;
        }
    }
}
