package ls2192221.androlecture.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;
import ls2192221.androlecture.codecs.Codec;
import ls2192221.androlecture.codecs.G711aLaw;
import ls2192221.androlecture.codecs.G711uLaw;
import ls2192221.androlecture.codecs.Opus;

import ls2192221.androlecture.constants.Constants;

import ls2192221.androlecture.enums.SamplingFrequency;
import ls2192221.androlecture.enums.VoicePacketLength;
import ls2192221.androlecture.helper_classes.Translation;
import ls2192221.androlecture.transactions.GetTimestampTransaction;

public class TranslatorService extends Service {

    public static final String TAG = "TranslatorService";
    public static final String IP_ADDRESS_SERVER = "192.168.4.1";
    private static final String CHANNEL_ID = "translatorservice";

    private final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private final int ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private int samplingFrequency;
    private int frameSize;
    private int minBufferSize;

    private long packetId;

    private final IBinder mBinder = new LocalBinder();
    private Translation translation;
    private Codec codec;

    private ExecutorService executorService;
    private GetTimestampTransaction getTimestampTransaction;

    private AudioRecord recorder = null;

    private boolean isRecording = false;

    public void setGetTimestampTransaction(GetTimestampTransaction getTimestampTransaction) {
        this.getTimestampTransaction = getTimestampTransaction;
    }

    public void startStreamingData() {
        if (!isRecording) {
            isRecording = true;
            startStream();
        }
    }

    public void stopStreamingData() {
        if (isRecording && recorder != null) {
            isRecording = false;
            recorder.release();
        }
    }

    private void startStream() {
        Thread streamThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    DatagramSocket socket = new DatagramSocket();
                    socket.setBroadcast(true);
                    DatagramPacket packet;
                    final InetAddress serverAddress = InetAddress.getByName(IP_ADDRESS_SERVER);

                    short recordedVoice [] = new short [frameSize];
                    byte encodedVoice[] = new byte [recordedVoice.length * 2];
                    byte voicePacket[] = new byte [8 + 8 + encodedVoice.length];

                    recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, samplingFrequency,
                            RECORDER_CHANNELS, ENCODING, minBufferSize);
                    recorder.startRecording();

                    while (isRecording) {

                        int read = recorder.read(recordedVoice, 0, recordedVoice.length);


                        voicePacket[0] = (byte)(packetId);
                        voicePacket[1] = (byte)(packetId >> 8);
                        voicePacket[2] = (byte)(packetId >> 16);
                        voicePacket[3] = (byte)(packetId >> 24);
                        voicePacket[4] = (byte)(packetId >> 32);
                        voicePacket[5] = (byte)(packetId >> 40);
                        voicePacket[6] = (byte)(packetId >> 48);
                        voicePacket[7] = (byte)(packetId >> 56);

                        ++packetId;

                        if (codec != null) {
                            int sizeAfterEncoding = codec.encode(recordedVoice, encodedVoice);

                            System.arraycopy(encodedVoice, 0, voicePacket, 16, sizeAfterEncoding);

                            if (Constants.getInstance().isCollectStatistics())
                            {
                                Future<Long> fut = executorService.submit(getTimestampTransaction);

                                long packetTimestamp = fut.get();

                                voicePacket[8] = (byte)(packetTimestamp);
                                voicePacket[9] = (byte)(packetTimestamp >> 8);
                                voicePacket[10] = (byte)(packetTimestamp >> 16);
                                voicePacket[11] = (byte)(packetTimestamp >> 24);
                                voicePacket[12] = (byte)(packetTimestamp >> 32);
                                voicePacket[13] = (byte)(packetTimestamp >> 40);
                                voicePacket[14] = (byte)(packetTimestamp >> 48);
                                voicePacket[15] = (byte)(packetTimestamp >> 56);
                            }

                            packet = new DatagramPacket(voicePacket, 16 + sizeAfterEncoding, serverAddress, translation.getDestinationPort());
                            socket.send(packet);
                        } else {
                            int i = 0;
                            for (short sample : recordedVoice) {
                                encodedVoice[i++] = (byte)(sample);
                                encodedVoice[i++] = (byte)(sample >> 8);
                            }

                            System.arraycopy(encodedVoice, 0, voicePacket, 16, encodedVoice.length);

                            if (Constants.getInstance().isCollectStatistics())
                            {
                                Future<Long> fut = executorService.submit(getTimestampTransaction);

                                long packetTimestamp = fut.get();

                                voicePacket[8] = (byte)(packetTimestamp);
                                voicePacket[9] = (byte)(packetTimestamp >> 8);
                                voicePacket[10] = (byte)(packetTimestamp >> 16);
                                voicePacket[11] = (byte)(packetTimestamp >> 24);
                                voicePacket[12] = (byte)(packetTimestamp >> 32);
                                voicePacket[13] = (byte)(packetTimestamp >> 40);
                                voicePacket[14] = (byte)(packetTimestamp >> 48);
                                voicePacket[15] = (byte)(packetTimestamp >> 56);
                            }
                            packet = new DatagramPacket(voicePacket, voicePacket.length, serverAddress, translation.getDestinationPort());
                            socket.send(packet);
                        }
                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e);
                }
            }
        });

        streamThread.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        translation = intent.getParcelableExtra(Translation.TAG);

        samplingFrequency = SamplingFrequency.valueFromString(translation.getSamplingRate()).toFrequency();
        double voicePacketLength = VoicePacketLength.valueFromString(translation.getVoicePacketLength()).toVoicePacketLength();
        frameSize = (int)(samplingFrequency * voicePacketLength * 0.001);
        minBufferSize = AudioRecord.getMinBufferSize(samplingFrequency, RECORDER_CHANNELS, ENCODING);

        if (minBufferSize < frameSize * 2) {
            minBufferSize = frameSize * 2;
        }

        ls2192221.androlecture.enums.Codec c = ls2192221.androlecture.enums.Codec.valueFromString(translation.getCodec());

        switch (c) {
            case OPUS:
                codec = new Opus(samplingFrequency, 1, frameSize);
                break;
            case G711_ULAW:
                codec = new G711uLaw();
                break;
            case G711_ALAW:
                codec = new G711aLaw();
                break;
        }

        executorService = Executors.newSingleThreadExecutor();

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(TAG)
                .setContentText("Translator service")
                .setSmallIcon(R.drawable.id_android)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(2, notification);
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (recorder != null) {
            if (recorder.getState() == AudioRecord.STATE_INITIALIZED) {
                recorder.stop();
                recorder.release();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public TranslatorService getService() {
            return TranslatorService.this;
        }
    }
}
