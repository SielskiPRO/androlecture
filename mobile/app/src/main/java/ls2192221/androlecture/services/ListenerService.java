package ls2192221.androlecture.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;

import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import java.util.LinkedList;
import java.util.Queue;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;
import ls2192221.androlecture.codecs.Codec;
import ls2192221.androlecture.codecs.G711aLaw;
import ls2192221.androlecture.codecs.G711uLaw;
import ls2192221.androlecture.codecs.Opus;

import ls2192221.androlecture.constants.Constants;

import ls2192221.androlecture.enums.SamplingFrequency;
import ls2192221.androlecture.enums.VoicePacketLength;
import ls2192221.androlecture.fragments.ManageListeningFragment;
import ls2192221.androlecture.helper_classes.StatisticsCollector;
import ls2192221.androlecture.helper_classes.Translation;
import ls2192221.androlecture.helper_classes.Utils;
import ls2192221.androlecture.transactions.GetTimestampTransaction;
import ls2192221.androlecture.transactions.SendStatisticsTransaction;

public class ListenerService extends Service {

    private static final String TAG = "ListenerService";
    private static final String CHANNEL_ID = "listenerService";
    private final IBinder mBinder = new LocalBinder();
    private Translation translation;
    private AudioTrack audioTrack = null;
    private Codec codec;
    private MulticastSocket multicastSocket;
    private InetAddress group;
    private Handler handler;

    private ExecutorService executorService;

    private GetTimestampTransaction getTimestampTransaction;
    private SendStatisticsTransaction sendStatisticsTransaction;

    private long previousPacketId;
    private StatisticsCollector statisticsCollector;
    private boolean isListening = true;
    private boolean statisticsHaveBeenSended = false;


    private int sendStatisticsTime;


    private Queue<byte[]> byteArrayArrayList = new LinkedList<>();
    private ReentrantLock lock = new ReentrantLock();
    private Condition bufferNotEmpty = lock.newCondition();


    private final int LISTENER_CHANNEL = AudioFormat.CHANNEL_OUT_MONO;
    private final int ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private int minBufferSize;
    private int samplingFrequency;
    private int frameSize;


    Runnable receiveUdpVoicePackets = new Runnable() {
        @Override
        public void run() {
            byte messageBuff[] = new byte[8 + 8 + frameSize * 2];
            DatagramPacket packet = new DatagramPacket(messageBuff, messageBuff.length);
            while (isListening) {
                try {

                    Log.d(TAG, "Waiting for voice packet!");
                    multicastSocket.receive(packet);

                    if (!statisticsHaveBeenSended && Constants.getInstance().isCollectStatistics()) {
                        new Thread(sendStatistics).start();
                        statisticsHaveBeenSended = true;
                    }

                    long receiveTimeStamp = 0;
                    if (Constants.getInstance().isCollectStatistics())
                    {
                        Future<Long> fut = executorService.submit(getTimestampTransaction);
                        receiveTimeStamp = fut.get();
                    }

                    Utils.sendMessageToHandler(handler, ManageListeningFragment.START_CHRONOMETER, null);


                    byte packetContent [] = packet.getData();
                    byte arrivalVoiceData[]  = new byte [packet.getLength() - 16];
                    System.arraycopy(packet.getData(), 16, arrivalVoiceData,
                            0, packet.getLength() - 16);

                    long incomePacketId = 0;

                    incomePacketId |= ((long) packetContent[0] & 0xFF);
                    incomePacketId |= ((long) packetContent[1] & 0xFF) << 8;
                    incomePacketId |= ((long) packetContent[2] & 0xFF) << 16;
                    incomePacketId |= ((long) packetContent[3] & 0xFF) << 24;
                    incomePacketId |= ((long) packetContent[4] & 0xFF) << 32;
                    incomePacketId |= ((long) packetContent[5] & 0xFF) << 40;
                    incomePacketId |= ((long) packetContent[6] & 0xFF) << 48;
                    incomePacketId |= ((long) packetContent[7] & 0xFF) << 56;

                    if (incomePacketId < previousPacketId) {
                        statisticsCollector.packetDropped();
                        continue;
                    }
                    previousPacketId = incomePacketId;

                    if (Constants.getInstance().isCollectStatistics())
                        statisticsCollector.startUpdatingStatistics(incomePacketId);

                    lock.lock();
                    try {
                        byteArrayArrayList.add(arrivalVoiceData);
                        bufferNotEmpty.signal();
                    } finally {
                        if (lock.isHeldByCurrentThread())
                            lock.unlock();
                    }

                    long packetTimestamp = 0;

                    packetTimestamp |= ((long) packetContent[8] & 0xFF);
                    packetTimestamp |= ((long) packetContent[9] & 0xFF) << 8;
                    packetTimestamp |= ((long) packetContent[10] & 0xFF) << 16;
                    packetTimestamp |= ((long) packetContent[11] & 0xFF) << 24;
                    packetTimestamp |= ((long) packetContent[12] & 0xFF) << 32;
                    packetTimestamp |= ((long) packetContent[13] & 0xFF) << 40;
                    packetTimestamp |= ((long) packetContent[14] & 0xFF) << 48;
                    packetTimestamp |= ((long) packetContent[15] & 0xFF) << 56;

                    if (Constants.getInstance().isCollectStatistics())
                        statisticsCollector.packetArrived(receiveTimeStamp, packetTimestamp, incomePacketId);


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    Runnable playAudio = new Runnable() {
        @Override
        public void run() {
            audioTrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL, samplingFrequency,
                    LISTENER_CHANNEL, ENCODING, minBufferSize, AudioTrack.MODE_STREAM);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                audioTrack.setVolume(AudioTrack.getMaxVolume());
            } else {
                audioTrack.setStereoVolume(AudioTrack.getMaxVolume(), AudioTrack.getMaxVolume());
            }
            byte arrivalVoiceData [];
            short decompressedVoiceBuff[] = new short[frameSize];
            audioTrack.play();

            while (isListening) {
                lock.lock();
                try {

                    if (byteArrayArrayList.isEmpty()) {
                        try {
                            Log.d(TAG, "Buffer empty! Awaiting for signal!");
                            bufferNotEmpty.await();
                            arrivalVoiceData = byteArrayArrayList.poll();
                            lock.unlock();

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            lock.unlock();
                            return;
                        }
                    } else {
                        arrivalVoiceData = byteArrayArrayList.poll();
                        lock.unlock();
                    }

                    if (codec == null) {
                        audioTrack.write(arrivalVoiceData, 0, arrivalVoiceData.length);
                    } else {
                        codec.decode(arrivalVoiceData, decompressedVoiceBuff);
                        audioTrack.write(decompressedVoiceBuff, 0, decompressedVoiceBuff.length);
                    }

                } finally {
                    if (lock.isHeldByCurrentThread())
                        lock.unlock();
                }
            }
        }
    };

    private Runnable sendStatistics = new Runnable() {
        @Override
        public void run() {
            try {
                Thread.sleep(sendStatisticsTime * 1000);
                sendStatisticsTransaction.setStatisticsEntries(statisticsCollector.getEntries());
                Executors.newSingleThreadExecutor().submit(sendStatisticsTransaction);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    };



    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Creating Executor Service");
        executorService = Executors.newSingleThreadExecutor();
        Log.d(TAG, "Created executro");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        translation = intent.getParcelableExtra(Translation.TAG);
        sendStatisticsTime = translation.getTimeToSendStatistics();
        statisticsCollector = new StatisticsCollector(getApplicationContext());
        samplingFrequency = SamplingFrequency.valueFromString(translation.getSamplingRate()).toFrequency();
        double voicePacketLength = VoicePacketLength.valueFromString(translation.getVoicePacketLength()).toVoicePacketLength();
        frameSize = (int)(samplingFrequency * voicePacketLength * 0.001);
        minBufferSize = AudioTrack.getMinBufferSize(samplingFrequency, LISTENER_CHANNEL, ENCODING);

        if (minBufferSize < frameSize * 2) {
            minBufferSize = frameSize * 2;
        }

        Log.d(TAG, "Min buffer size listaner : " + minBufferSize);

        ls2192221.androlecture.enums.Codec c = ls2192221.androlecture.enums.Codec.valueFromString(translation.getCodec());

        switch (c) {
            case OPUS:
                codec = new Opus(samplingFrequency, 1, frameSize);
                break;
            case G711_ULAW:
                codec = new G711uLaw();
                break;
            case G711_ALAW:
                codec = new G711aLaw();
                break;
        }

        Log.d(TAG, "Freq : " + samplingFrequency);
        Log.d(TAG, "voice length : " + voicePacketLength);
        Log.d(TAG, "frame size : " + frameSize);
        Log.d(TAG, "codec : " + c.toString());

        try {
            group = InetAddress.getByName(translation.getIpGroupAddress());
            multicastSocket = new MulticastSocket(translation.getReceivePort());
            multicastSocket.joinGroup(group);
        } catch (IOException e) {
            Log.d(TAG, "Did not join group");
            e.printStackTrace();
        }

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(TAG)
                .setContentText("Listener service")
                .setSmallIcon(R.drawable.id_android)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(3, notification);
        return START_NOT_STICKY;
    }

    public void setHandler(Handler handler) {

        this.handler = handler;
        statisticsCollector.setHandler(handler);
    }

    public void setGetTimestampTransaction(GetTimestampTransaction getTimestampTransaction) {
        this.getTimestampTransaction = getTimestampTransaction;
        startListening();
    }

    public void setSendStatisticsTransaction(SendStatisticsTransaction sendStatisticsTransaction) {
        this.sendStatisticsTransaction = sendStatisticsTransaction;
    }

    public void stopListening()
    {
        isListening = false;
    }

    private void startListening() {
        new Thread(receiveUdpVoicePackets).start();
        new Thread(playAudio).start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
        statisticsCollector.stopUpdatingStatistics();
        isListening = false;
        executorService.shutdown();
        try {
            if (multicastSocket != null)
                multicastSocket.leaveGroup(group);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (audioTrack != null) {
            audioTrack.pause();
            audioTrack.release();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class LocalBinder extends Binder {
        public ListenerService getService() {
            return ListenerService.this;
        }
    }
}
