package ls2192221.androlecture.enums;

import java.util.HashMap;
import java.util.Map;

public enum SamplingFrequency {
    S8_kHZ(0),
    S12_kHz(1),
    S16_kHz(2),
    S24_kHz(3),
    S48_kHz(4);

    private int value;

    SamplingFrequency(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static SamplingFrequency valueOf(int codec) {
        return mapInt.get(codec);
    }
    public static SamplingFrequency valueFromString(String codec) {
        return mapStr.get(codec);
    }

    public String toString() {
        return SamplingFrequencyExtensions.toFriendlyString(this);
    }
    public int toFrequency() {
        return SamplingFrequencyExtensions.toIntFrequency(this);
    }

    private static Map<Integer, SamplingFrequency> mapInt = new HashMap<>();
    private static Map<String, SamplingFrequency> mapStr = new HashMap<>();

    static {
        for (SamplingFrequency c : SamplingFrequency.values()) {
            mapInt.put(c.getValue(), c);
            mapStr.put(c.toString(), c);
        }
    }

    public static class SamplingFrequencyExtensions
    {
        public static String toFriendlyString(SamplingFrequency me) {
            switch (me) {
                case S8_kHZ:
                    return "8 kHz";
                case S12_kHz:
                    return "12 kHz";
                case S16_kHz:
                    return "16 kHz";
                case S24_kHz:
                    return "24 kHz";
                case S48_kHz:
                    return "48 kHz";
                    default:
                        return "UNKNOWN";
            }
        }

        public static int toIntFrequency(SamplingFrequency me) {
            switch (me) {
                case S8_kHZ:
                    return 8000;
                case S12_kHz:
                    return 12000;
                case S16_kHz:
                    return 16000;
                case S24_kHz:
                    return 24000;
                case S48_kHz:
                    return 48000;
                default:
                    return 8000;
            }
        }
    }
}


