package ls2192221.androlecture.enums;

import java.util.HashMap;
import java.util.Map;

public enum VoicePacketLength {
    L20ms(3),
    L40ms(4),
    L60ms(5);

    private int value;

    VoicePacketLength(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return VoicePacketLengthExtensions.toFriendlyString(this);
    }
    public double toVoicePacketLength() {
        return VoicePacketLengthExtensions.toIntVoicePacketLength(this);
    }

    public static VoicePacketLength valueOf(int codec) {
        return mapInt.get(codec);
    }
    public static VoicePacketLength valueFromString(String codec) {
        return mapStr.get(codec);
    }

    private static Map<Integer, VoicePacketLength> mapInt = new HashMap<>();
    private static Map<String, VoicePacketLength> mapStr = new HashMap<>();

    static {
        for (VoicePacketLength c : VoicePacketLength.values()) {
            mapInt.put(c.getValue(), c);
            mapStr.put(c.toString(), c);
        }
    }

    public static class VoicePacketLengthExtensions {
        public static String toFriendlyString(VoicePacketLength me) {
            switch (me) {
                case L20ms:
                    return  "20 ms";
                case L40ms:
                    return "40 ms";
                case L60ms:
                    return  "60 ms";
                    default:
                        return "UNKNOWN";
            }
        }

        public static double toIntVoicePacketLength(VoicePacketLength me) {
            switch (me) {
                case L20ms:
                    return  20;
                case L40ms:
                    return 40;
                case L60ms:
                    return  60;
                default:
                    return 20;
            }
        }
    }
}
