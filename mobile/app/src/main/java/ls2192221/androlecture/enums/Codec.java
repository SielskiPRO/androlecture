package ls2192221.androlecture.enums;

import java.util.HashMap;
import java.util.Map;

public enum Codec {
    NONE(0),
    G711_ULAW(1),
    G711_ALAW(2),
    OPUS(3);

    private int value;

    Codec(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Codec valueOf(int codec) {
        return mapInt.get(codec);
    }
    public static Codec valueFromString(String codec) {
        return mapStr.get(codec);
    }

    public String toString() {
        return CodecExtensions.toFriendlyString(this);
    }

    private static Map<Integer, Codec> mapInt = new HashMap<>();
    private static Map<String, Codec> mapStr = new HashMap<>();

    static {
        for (Codec c : Codec.values()) {
            mapInt.put(c.getValue(), c);
            mapStr.put(c.toString(), c);
        }
    }

    public static class CodecExtensions
    {
        public static String toFriendlyString(Codec me) {
            switch (me) {
                case NONE:
                    return "NONE";
                case G711_ULAW:
                    return "G711 ulaw";
                case G711_ALAW:
                    return "G711 alaw";
                case OPUS:
                    return "OPUS";
                    default:
                        return "UNKNOWN";
            }
        }
    }
}
