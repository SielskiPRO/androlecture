package ls2192221.androlecture.protocol;

public enum Command {
    ADD_TRANSLATION         ((byte)0x01),
    DELETE_TRANSLATION      ((byte)0x02),
    GET_TRANSLATIONS        ((byte)0x03),
    TRANSLATIONS_COUNT      ((byte)0x04),
    TRANSLATION             ((byte)0x05),
    REQUEST_TRANSLATION     ((byte)0x06),
    AUTHENTICATE_REQUEST    ((byte)0x07),
    PING                    ((byte)0x08),
    GET_TIMESTAMP           ((byte)0x09),
    SAVE_STATISTICS         ((byte)0x0A),
    RESPONSE_SUCCESS        ((byte)0x0B),
    RESPONSE_FAILURE        ((byte)0x0C);


    Command(byte value) {
        this.value = value;
    }

    private byte value;

    public byte getValue() {
        return value;
    }
}
