package ls2192221.androlecture.protocol;

public class ProtocolSpecification {
    public static final int TCP_HEADER_SIZE = 9;
    public static final int TRANSLATION_LENGTH = 2;
    public static final int CODEC_SIZE = 1;
    public static final int SAMPLING_RATE_SIZE = 1;
    public static final int VOICE_PACKET_LENGTH = 1;
    public static final int TIME_TO_SEND_STATISTICS_SIZE = 4;
    public static final int TRANSLATION_ID_SIZE = 4;
}
