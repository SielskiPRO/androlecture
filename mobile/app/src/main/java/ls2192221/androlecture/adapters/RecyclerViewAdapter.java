package ls2192221.androlecture.adapters;

import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.Executors;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;
import ls2192221.androlecture.helper_classes.Translation;
import ls2192221.androlecture.transactions.RequestTranslationTransaction;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private static final String TAG = "RecyclerViewAdapter";
    private ArrayList<Translation> translations = new ArrayList<>();
    MainActivity mainActivity;
    private Handler mHandler;

    public RecyclerViewAdapter(MainActivity mainActivity, Handler handler) {
        this.mainActivity = mainActivity;
        this.mHandler = handler;
    }

    public void addTranslations(ArrayList<Translation> newTranslations){
        clear();
        int insertIndex = translations.size();
        translations.addAll(insertIndex, newTranslations);
        notifyItemRangeInserted(insertIndex, newTranslations.size());
    }

    public void clear()
    {
        final int size = translations.size();
        translations.clear();
        notifyItemRangeRemoved(0, size);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.translation_listitem, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Log.d(TAG, "onBindViewHolder: called");
        viewHolder.languageTextView
                .setText(translations.get(i).getTranslationLanguage());
        viewHolder.descriptionTextView
                .setText(translations.get(i).getTranslationDescription());

        viewHolder.parentLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                RequestTranslationTransaction requestTranslationTransaction =
                        new RequestTranslationTransaction(mHandler, mainActivity.getTcpService());
                requestTranslationTransaction.setTranslation(translations.get(i));
                Executors.newSingleThreadExecutor().submit(requestTranslationTransaction);
            }
        });
    }

    @Override
    public int getItemCount() {
        return translations.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView languageTextView;
        TextView descriptionTextView;
        ConstraintLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            languageTextView = itemView.findViewById(R.id.languageTextView);
            descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
