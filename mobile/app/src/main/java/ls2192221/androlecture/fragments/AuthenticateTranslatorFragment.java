package ls2192221.androlecture.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.concurrent.Executors;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;
import ls2192221.androlecture.helper_classes.Utils;
import ls2192221.androlecture.transactions.AuthenticateTransaction;

public class AuthenticateTranslatorFragment extends Fragment {

    public final String TAG = "AuthenticateTranslator";
    public static final int AUTHENTICATION_SUCCESSFUL = 0;
    public static final int AUTHENTICATION_FAILURE = 1;
    public static final int COMMUNICATION_ERROR = 2;

    private EditText passwordEditText;
    private Button authenticateButton;
    private AuthenticateTranslatorHandler mHandler;
    private AuthenticateTransaction authenticateTransaction;

    private MainActivity mActivity;

    public class AuthenticateTranslatorHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case AUTHENTICATION_FAILURE:
                    String errorMsg = (String)msg.obj;
                    mActivity.setDialog(Utils.showNotificationDialog(errorMsg, Utils.NotificationType.ERROR, getActivity()));
                    break;
                case AUTHENTICATION_SUCCESSFUL:
                    mActivity.replaceCurrentFragment(new AddTranslationFragment(),
                            AddTranslationFragment.TAG, AddTranslationFragment.TAG);
                    break;
                case COMMUNICATION_ERROR:
                    errorMsg = (String)msg.obj;
                    mActivity.setDialog(Utils.showNotificationDialog(errorMsg, Utils.NotificationType.ERROR, getActivity()));
                    mActivity.backToLogin();
                    break;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.authenticate_translator_fragment, container, false);
        initializeReferences(view);
        return view;
    }



    @Override
    public void onResume() {
        super.onResume();
        mHandler = new AuthenticateTranslatorHandler();
        authenticateTransaction = new AuthenticateTransaction(mHandler, mActivity.getTcpService());
    }

    private void initializeReferences(View view){
        passwordEditText = view.findViewById(R.id.passwordEditText);
        authenticateButton = view.findViewById(R.id.authenticateButton);
        authenticateButton.setOnClickListener((View v) -> {
            String password = passwordEditText.getText().toString();
            authenticateTransaction.setPassword(password);
            Executors.newSingleThreadExecutor().execute(authenticateTransaction);
        });
        mActivity = ((MainActivity)getActivity());
    }
}
