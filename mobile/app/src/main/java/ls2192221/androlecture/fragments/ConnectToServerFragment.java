package ls2192221.androlecture.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.concurrent.Executors;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;

import ls2192221.androlecture.constants.Constants;

import ls2192221.androlecture.helper_classes.Utils;

public class ConnectToServerFragment extends Fragment {

    public static final int CONNECTION_SUCCESSFUL = 1;
    public static final int CONNECTION_FAILURE = 2;

    public static final String TAG = "ConnectToServer";

    private static MainActivity mActivity;
    private EditText serverIpAddressEditText;
    private EditText serverPortEditText;
    private Button connectButton;
    private ConnectToServerHandler mHandler;

    public class ConnectToServerHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case CONNECTION_FAILURE:
                    String errorMsg = (String)msg.obj;
                    connectButton.setEnabled(true);
                    mActivity.setDialog(Utils.showNotificationDialog(errorMsg,
                            Utils.NotificationType.ERROR, getActivity()));

                    break;
                case CONNECTION_SUCCESSFUL:
                    boolean collectStatistics = (boolean)msg.obj;
                    Log.d(TAG, "Collection : " + collectStatistics);
                    connectButton.setEnabled(true);
                    if (collectStatistics)
                        Constants.getInstance().enableCollectionStatistics();

                    mActivity.replaceCurrentFragment(new ChooseModeFragment(), ChooseModeFragment.TAG, ChooseModeFragment.TAG);
                default:
                    break;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.server_connection_fragment, container, false);
        initializeViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler = new ConnectToServerHandler();
    }

    private void initializeViews(View view){
        serverIpAddressEditText = view.findViewById(R.id.serverIpAddressEditText);
        serverPortEditText = view.findViewById(R.id.serverPortEditText);
        connectButton = view.findViewById(R.id.connectButton);

        connectButton.setOnClickListener((View v) ->
            onConnectButtonClick()
        );

        mActivity = ((MainActivity)getActivity());
    }

    public void onConnectButtonClick(){
        connectButton.setEnabled(false);
        String ipAddress = serverIpAddressEditText.getText().toString();
        String port = serverPortEditText.getText().toString();

        Executors.newSingleThreadExecutor().execute(() ->
            mActivity.getTcpService().connectToServer(ipAddress, port, mHandler)
        );
    }

}
