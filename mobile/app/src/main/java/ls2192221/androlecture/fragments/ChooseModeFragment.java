package ls2192221.androlecture.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;

public class ChooseModeFragment extends Fragment {

    private MainActivity mainActivity;

    public static final int REQUEST_RECORD = 1;
    public static final int REQUEST_LOCATION = 2;

    public static final String TAG = "ChooseMode";
    private Button listenerModeButton;
    private Button translatorModeButton;

    private View.OnClickListener onModeButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.listenerModeButton:

                    if (ContextCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_FINE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[] {
                                Manifest.permission.ACCESS_FINE_LOCATION
                        }, REQUEST_LOCATION);
                    } else {
                        mainActivity.replaceCurrentFragment(new ChooseTranslationFragment(), null, null);
                    }
                    break;
                case R.id.translatorModeButton:
                    if (ContextCompat.checkSelfPermission(mainActivity, Manifest.permission.RECORD_AUDIO) !=
                            PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{
                                Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD);

                    } else {
                        mainActivity.replaceCurrentFragment(new AuthenticateTranslatorFragment(), null, null);
                    }
                    break;
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_RECORD:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mainActivity.replaceCurrentFragment(new AuthenticateTranslatorFragment(), null, null);
                } else {
                }
                break;
            case REQUEST_LOCATION:
                Log.d(TAG, "Request location case!");
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "LOCATION ACCESS ALLOWED!!!!!");
                    mainActivity.replaceCurrentFragment(new ChooseTranslationFragment(), null, null);
                } else {
                }
                break;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.choose_mode_fragment, container, false);
        initializeViews(view);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }

    private void initializeViews(View view){
        listenerModeButton = view.findViewById(R.id.listenerModeButton);
        translatorModeButton = view.findViewById(R.id.translatorModeButton);
        listenerModeButton.setOnClickListener(onModeButtonClick);
        translatorModeButton.setOnClickListener(onModeButtonClick);
        mainActivity = ((MainActivity)getActivity());
    }
}
