package ls2192221.androlecture.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.TextView;

import java.util.concurrent.Executors;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;

import ls2192221.androlecture.helper_classes.Translation;
import ls2192221.androlecture.helper_classes.Utils;
import ls2192221.androlecture.services.TranslatorService;
import ls2192221.androlecture.transactions.DeleteTranslationTransaction;
import ls2192221.androlecture.transactions.GetTimestampTransaction;

public class ManageTranslationFragment extends Fragment {
    public static final String TAG = "ManageTranslation";
    public static final int DELETION_SUCCESSFUL = 0;
    public static final int COMMUNICATION_ERROR = 1;
    public static final int UPDATE_COMPRESSION = 2;

    private MainActivity mActivity;
    private Translation translation;
    private TelephonyManager tm;
    private Chronometer chronometer;

    private TextView compressionRateValueTextView;


    private boolean chronometerRunning = false;
    private boolean hasBeenStartedByUser = false;
    private boolean hasBeenStoppedByUser = false;

    private ManageTranslationHandler mHandler;
    private DeleteTranslationTransaction deleteTranslationTransaction;

    public class ManageTranslationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case DELETION_SUCCESSFUL:
                    String message = (String)msg.obj;
                    mActivity.setDialog(Utils.showNotificationDialog(message,
                            Utils.NotificationType.INFO, getActivity()));
                    getFragmentManager().popBackStack();
                    break;
                case COMMUNICATION_ERROR:
                    message = (String)msg.obj;
                    mActivity.setDialog(Utils.showNotificationDialog(message,
                            Utils.NotificationType.ERROR, getActivity()));
                    mActivity.backToLogin();
                    break;
                case UPDATE_COMPRESSION:
                    message = (String)msg.obj;
                    compressionRateValueTextView.setText(message + "%");
                    break;

            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.manage_translation_fragment, container, false);
        initializeReferences(view);

        Intent intent = new Intent(getActivity(), TranslatorService.class);
        intent.putExtra(Translation.TAG, translation);
        mActivity.startService(intent);
        mActivity.bindService(intent, mActivity.getTranslatorConnection(), Context.BIND_AUTO_CREATE);

        mActivity.registerTranslatorFragmentCallback(new MainActivity.OnTranslatorServiceBind() {
            @Override
            public void onServiceBind(TranslatorService translatorService) {
                translatorService.setGetTimestampTransaction(new GetTimestampTransaction(mActivity.getTcpService()));
            }
        });

        tm = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(new PhoneStateListener(){

            @Override
            public void onCallStateChanged(int state, String phoneNumber) {
                super.onCallStateChanged(state, phoneNumber);
                switch (state) {
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        if (hasBeenStartedByUser) {
                            Log.d(TAG, "Stopping because of outgoing call");
                            mActivity.getTranslatorService().stopStreamingData();
                        }
                        break;
                    case TelephonyManager.CALL_STATE_RINGING:
                        if (hasBeenStartedByUser) {
                            Log.d(TAG, "Stopping because incoming call");
                            mActivity.getTranslatorService().stopStreamingData();
                        }
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        if (hasBeenStartedByUser && !hasBeenStoppedByUser) {
                            Log.d(TAG, "Resuming recording and sending");
                            mActivity.getTranslatorService().startStreamingData();
                        }
                        break;
                }
            }
        }, PhoneStateListener.LISTEN_CALL_STATE);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler = new ManageTranslationHandler();
        deleteTranslationTransaction = new DeleteTranslationTransaction(mHandler, mActivity.getTcpService());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
        mActivity.getTranslatorService().stopStreamingData();
        mActivity.destroyTranslatorService();
    }

    private void initializeReferences(View view){
        translation = getArguments().getParcelable(Translation.TAG);
        mActivity = (MainActivity)getActivity();
        setUpTextViewsContent(view, translation);
        setUpButtonsListeners(view);
        chronometer = view.findViewById(R.id.elapsedTimeValueChronometer);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.setText("00:00:00");
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long time = SystemClock.elapsedRealtime() - chronometer.getBase();
                int h = (int)(time / 3600000);
                int m = (int)(time - h*3600000) / 60000;
                int s = (int)(time - h*3600000 - m*60000) / 1000;
                String t = (h < 10 ? "0"+h: h)+":"+(m < 10 ? "0"+m: m)+":"+ (s < 10 ? "0"+s: s);
                chronometer.setText(t);
            }
        });
    }

    private void setUpButtonsListeners(View view) {

        view.findViewById(R.id.startTranslationButton).setOnClickListener(v -> {
            mActivity.getTranslatorService().startStreamingData();
            hasBeenStartedByUser = true;
            if (!chronometerRunning) {
                chronometer.setBase(SystemClock.elapsedRealtime());
                chronometer.start();
                chronometerRunning = true;
            }
        });

        view.findViewById(R.id.pauseTranslationButton).setOnClickListener(v -> {
            mActivity.getTranslatorService().stopStreamingData();
            hasBeenStoppedByUser = true;
        });

        view.findViewById(R.id.stopTranslationButton).setOnClickListener(v -> {
            mActivity.getTranslatorService().stopStreamingData();
            hasBeenStoppedByUser = true;
            Executors.newSingleThreadExecutor().execute(deleteTranslationTransaction);
        });
    }

    private void setUpTextViewsContent(View view, Translation translation){
        ((TextView)(view.findViewById(R.id.translationLangugeTextView))).setText(translation.getTranslationLanguage());
        ((TextView)(view.findViewById(R.id.translationListeningDescriptionTextView))).setText(translation.getTranslationDescription());
    }
}
