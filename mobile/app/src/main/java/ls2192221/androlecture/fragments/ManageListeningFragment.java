package ls2192221.androlecture.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;

import ls2192221.androlecture.constants.Constants;

import ls2192221.androlecture.helper_classes.StatisticEntry;
import ls2192221.androlecture.helper_classes.Translation;
import ls2192221.androlecture.services.ListenerService;
import ls2192221.androlecture.transactions.GetTimestampTransaction;
import ls2192221.androlecture.transactions.SendStatisticsTransaction;

import static android.view.View.GONE;

public class ManageListeningFragment extends Fragment {

    public static final String TAG = "ManageListening";
    public static final int START_CHRONOMETER = 0;
    public static final int UPDATE_STATISTICS_FIELDS = 1;
    public static final int SEND_STATISTICS = 2;

    private MainActivity mActivity;
    private Translation translation;
    private Chronometer chronometer;
    private ManageListeningHandler mHandler;

    private boolean isChronometerStarted = false;

    private TextView wifiSignalValueTextView;
    private TextView packetLossValueTextView;
    private TextView packetDropValueTextView;
    private TextView jitterValueTextView;
    private Button stopListeningButton;

    public class ManageListeningHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case START_CHRONOMETER:
                    if (!isChronometerStarted)
                    {
                        isChronometerStarted = true;
                        chronometer.setBase(SystemClock.elapsedRealtime());
                        chronometer.start();
                    }

                    break;
                case UPDATE_STATISTICS_FIELDS:
                    Log.d(TAG, "UPDATE_STATISTICS_FIELDS");
                    StatisticEntry entry = (StatisticEntry)msg.obj;
                    packetLossValueTextView.setText(String.valueOf(entry.getPacketLoss()));
                    packetDropValueTextView.setText(String.valueOf(entry.getPacketDrop()));
                    jitterValueTextView.setText(String.valueOf(entry.getAvgJitter()));
                    wifiSignalValueTextView.setText(String.valueOf(entry.getWifiSignalStrength()));

                    break;
                case SEND_STATISTICS:
                    Log.d(TAG, "SEND_STATISTICS_FIELDS");
                    break;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.manage_listening_fragment, container, false);
        translation = getArguments().getParcelable(Translation.TAG);
        initializeReferences(view);
        setUpKnownTexts(view);
        mActivity.registerListenerFragmentCallback(new MainActivity.OnListenerServiceBind() {
            @Override
            public void onServiceBind(ListenerService listenerService) {
                listenerService.setHandler(mHandler);
                listenerService.setGetTimestampTransaction(new GetTimestampTransaction(mActivity.getTcpService()));
                listenerService.setSendStatisticsTransaction(new SendStatisticsTransaction(null ,mActivity.getTcpService()));
            }
        });

        Intent intent = new Intent(getActivity(), ListenerService.class);
        intent.putExtra(Translation.TAG, translation);

        mActivity.startService(intent);
        mActivity.bindService(intent, mActivity.getListenerConnection(), Context.BIND_AUTO_CREATE);
        return view;
    }



    private void initializeReferences(View view) {
        chronometer = view.findViewById(R.id.listenChronometer);
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long time = SystemClock.elapsedRealtime() - chronometer.getBase();
                int h = (int)(time / 3600000);
                int m = (int)(time - h*3600000) / 60000;
                int s = (int)(time - h*3600000 - m*60000) / 1000;
                String t = (h < 10 ? "0"+h: h)+":"+(m < 10 ? "0"+m: m)+":"+ (s < 10 ? "0"+s: s);
                chronometer.setText(t);
            }
        });
        chronometer.setText("00:00:00");
        chronometer.setBase(SystemClock.elapsedRealtime());

        mActivity = (MainActivity)getActivity();

        mHandler = new ManageListeningHandler();

        if(!Constants.getInstance().isCollectStatistics())
            view.findViewById(R.id.parametersTranslationLayout).setVisibility(GONE);

        wifiSignalValueTextView = view.findViewById(R.id.wifiSignalValueTextView);
        packetLossValueTextView = view.findViewById(R.id.packetLossValueTextView);
        packetLossValueTextView.setText("N/A");
        packetDropValueTextView = view.findViewById(R.id.packetDropValueTextView);
        jitterValueTextView = view.findViewById(R.id.jitterValueTextView);
        stopListeningButton = view.findViewById(R.id.stopListeningButton);

        stopListeningButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    private void setUpKnownTexts(View view) {
        ((TextView)view.findViewById(R.id.translationListeningLanguageTextView)).setText(translation.getTranslationLanguage());
        ((TextView)view.findViewById(R.id.translationListeningDescriptionTextView)).setText(translation.getTranslationDescription());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mActivity.getListenerService().stopListening();
        mActivity.destroyListenerService();
    }
}
