package ls2192221.androlecture.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.concurrent.Executors;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;

import ls2192221.androlecture.constants.Constants;

import ls2192221.androlecture.enums.Codec;
import ls2192221.androlecture.enums.SamplingFrequency;
import ls2192221.androlecture.enums.VoicePacketLength;
import ls2192221.androlecture.helper_classes.Translation;
import ls2192221.androlecture.helper_classes.Utils;

import ls2192221.androlecture.transactions.AddTranslationTransaction;

public class AddTranslationFragment extends Fragment {

    public static final String TAG = "AddTranslation";
    public static final int TRANSLATION_ADDED = 0;
    public static final int TRANSLATION_REJECTED = 1;
    public static final int COMMUNICATION_ERROR = 2;


    private MainActivity mActivity;
    private EditText translationLanguageEditText;
    private EditText translationDescriptionEditText;
    private EditText statisticsTimeEditText;
    private Button addTranslationButton;

    private ArrayAdapter<String> codecArrayAdapter;
    private ArrayAdapter<String> frequencyArrayAdapter;
    private ArrayAdapter<String> voicePacketLengthAdapter;

    private Spinner codecSpinner;
    private Spinner samplingRateSpinner;
    private Spinner voicePacketLengthSpinner;

    private AddTranslationHandler mHandler;
    private AddTranslationTransaction addTranslationTransaction;

    public class AddTranslationHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case TRANSLATION_ADDED:
                    Translation t = (Translation)msg.obj;
                    Log.d(TAG, "This will go to translation manager: " + t.toString());
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Translation.TAG, t);

                    ManageTranslationFragment manageTranslationFragment = new ManageTranslationFragment();
                    manageTranslationFragment.setArguments(bundle);

                    mActivity.replaceCurrentFragment(manageTranslationFragment, ManageTranslationFragment.TAG, ManageTranslationFragment.TAG);
                    break;
                case TRANSLATION_REJECTED:
                    String errorMsg = (String)msg.obj;
                    mActivity.setDialog(Utils.showNotificationDialog(errorMsg, Utils.NotificationType.INFO,
                            getActivity()));
                    break;
                case COMMUNICATION_ERROR:
                    errorMsg = (String)msg.obj;
                    mActivity.setDialog(Utils.showNotificationDialog(errorMsg, Utils.NotificationType.ERROR,
                            getActivity()));
                    mActivity.backToLogin();
                    break;
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_translation_fragment, container, false);
        initializeViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler = new AddTranslationHandler();
        addTranslationTransaction = new AddTranslationTransaction(mHandler, mActivity.getTcpService());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() AddTranslation");
    }

    private void initializeViews(View view)
    {

        if (!Constants.getInstance().isCollectStatistics())
        {
            view.findViewById(R.id.containerScrollView).setVisibility(View.GONE);
        }

        translationDescriptionEditText = view.findViewById(R.id.translationDescriptionEditText);
        translationLanguageEditText = view.findViewById(R.id.translationLanguageEditText);
        statisticsTimeEditText = view.findViewById(R.id.statisticsTimeEditText);
        addTranslationButton = view.findViewById(R.id.addTranslationButton);
        initializeSpinners(view);
        mActivity = ((MainActivity)getActivity());
        addTranslationButton.setOnClickListener((View v) -> {
            String language = translationLanguageEditText.getText().toString();
            String description = translationDescriptionEditText.getText().toString();
            addTranslationTransaction.setLanguage(language);
            addTranslationTransaction.setDescription(description);
            addTranslationTransaction.setCodec(codecSpinner.getSelectedItem().toString());
            addTranslationTransaction.setSamplingRate(samplingRateSpinner.getSelectedItem().toString());
            addTranslationTransaction.setVoicePacketLength(voicePacketLengthSpinner.getSelectedItem().toString());

            if (statisticsTimeEditText.getText().toString().equals("")) {
                addTranslationTransaction.setTimeToSendStatistics(0);
            } else {
                addTranslationTransaction.setTimeToSendStatistics(
                        Integer.valueOf(statisticsTimeEditText.getText().toString())
                );
            }
            Executors.newSingleThreadExecutor().execute(addTranslationTransaction);
        });
    }

    private void initializeSpinners(View view){
        codecSpinner = view.findViewById(R.id.codecSpinner);
        String[] codecs = Utils.getEnumNames(Codec.class);
        codecArrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, codecs);
        codecArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        codecSpinner.setAdapter(codecArrayAdapter);

        if (!Constants.getInstance().isCollectStatistics())
            codecSpinner.setSelection(3);

        samplingRateSpinner = view.findViewById(R.id.samplingRateValueSpinner);
        String[] samplingRates = Utils.getEnumNames(SamplingFrequency.class);
        frequencyArrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, samplingRates);
        frequencyArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        samplingRateSpinner.setAdapter(frequencyArrayAdapter);

        if (!Constants.getInstance().isCollectStatistics())
        {
            samplingRateSpinner.setSelection(0);
        }

        voicePacketLengthSpinner = view.findViewById(R.id.voicePacketLengthValueSpinner);
        String[] voicePacketsLengths = Utils.getEnumNames(VoicePacketLength.class);
        voicePacketLengthAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, voicePacketsLengths);
        voicePacketLengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        voicePacketLengthSpinner.setAdapter(voicePacketLengthAdapter);
        if (!Constants.getInstance().isCollectStatistics())
        {
            voicePacketLengthSpinner.setSelection(0);
        }


        initializeSpinnersOnItemSelectedListeners();
    }

    private void initializeSpinnersOnItemSelectedListeners()
    {
        codecSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String codecString = (String )codecSpinner.getItemAtPosition(position);
                Codec c = Codec.valueFromString(codecString);
                switch (c) {
                    case G711_ALAW:
                    case G711_ULAW:
                        samplingRateSpinner.setSelection(frequencyArrayAdapter.getPosition(SamplingFrequency.S8_kHZ.toString()));
                        samplingRateSpinner.setEnabled(false);
                        voicePacketLengthSpinner.setSelection(voicePacketLengthAdapter.getPosition(VoicePacketLength.L20ms.toString()));
                        voicePacketLengthSpinner.setEnabled(false);
                        break;
                        default:
                            samplingRateSpinner.setEnabled(true);
                            voicePacketLengthSpinner.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
