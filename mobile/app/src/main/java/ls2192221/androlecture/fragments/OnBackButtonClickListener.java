package ls2192221.androlecture.fragments;

public interface OnBackButtonClickListener {
    void onBackButtonClick();
}
