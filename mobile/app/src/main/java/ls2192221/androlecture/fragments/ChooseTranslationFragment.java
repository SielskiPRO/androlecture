package ls2192221.androlecture.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.concurrent.Executors;

import ls2192221.androlecture.MainActivity;
import ls2192221.androlecture.R;
import ls2192221.androlecture.adapters.RecyclerViewAdapter;
import ls2192221.androlecture.helper_classes.Translation;
import ls2192221.androlecture.helper_classes.Utils;
import ls2192221.androlecture.transactions.DownloadTranslationsTransaction;

public class ChooseTranslationFragment extends Fragment {

    public static final String TAG = "ChooseTranslation";
    public static final int TRANSLATIONS_DOWNLOAD_FAILURE = 0;
    public static final int TRANSLATIONS_DOWNLOAD_SUCCESS = 1;
    public static final int COMMUNICATION_ERROR = 2;
    public static final int TRANSLATION_AVAILABLE = 3;
    public static final int TRANSLATION_NOT_AVAILABLE = 4;
    public static final int TRANSLATION_SAMPLING_RATE_NOT_SUPPORTED = 5;

    private MainActivity mainActivity;
    private RecyclerViewAdapter mTranslationsViewAdapter;
    private ChooseTranslationHandler mHandler;
    private DownloadTranslationsTransaction mDownloadTranslationsTransaction;

    private Button getTranslationsButton;


    public class ChooseTranslationHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case TRANSLATIONS_DOWNLOAD_FAILURE:
                    String message = (String)msg.obj;
                    mainActivity.setDialog(Utils.showNotificationDialog(message,
                            Utils.NotificationType.ERROR, getActivity()));
                    getTranslationsButton.setEnabled(true);
                    mainActivity.backToLogin();
                    break;
                case TRANSLATIONS_DOWNLOAD_SUCCESS:
                    ArrayList<Translation> downloadedTranslations = (ArrayList<Translation>)msg.obj;
                    for (Translation t : downloadedTranslations) {
                        Log.d(TAG, t.toString());
                    }
                    if (downloadedTranslations.isEmpty()){
                        mainActivity.setDialog(Utils.showNotificationDialog("No translations available!",
                                Utils.NotificationType.INFO, getActivity()));
                    }
                    mTranslationsViewAdapter.addTranslations(downloadedTranslations);
                    getTranslationsButton.setEnabled(true);
                    break;
                case COMMUNICATION_ERROR:
                    message = (String)msg.obj;
                    mainActivity.setDialog(Utils.showNotificationDialog(message,
                            Utils.NotificationType.ERROR, getActivity()));
                    mainActivity.backToLogin();
                    break;
                case TRANSLATION_NOT_AVAILABLE:
                    message = (String)msg.obj;
                    mainActivity.setDialog(Utils.showNotificationDialog(message,
                            Utils.NotificationType.INFO, getActivity()));
                    break;
                case TRANSLATION_AVAILABLE:
                    Translation t = (Translation)msg.obj;
                    Log.d(TAG, "This is requested translation" + t.toString());

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Translation.TAG, t);
                    ManageListeningFragment manageListeningFragment = new ManageListeningFragment();
                    manageListeningFragment.setArguments(bundle);
                    mainActivity.replaceCurrentFragment(manageListeningFragment, ManageListeningFragment.TAG, ManageListeningFragment.TAG);
                    break;
                case TRANSLATION_SAMPLING_RATE_NOT_SUPPORTED:
                    message = (String)msg.obj;
                    mainActivity.setDialog(Utils.showNotificationDialog(message,
                            Utils.NotificationType.INFO, getActivity()));
                    break;
            }
        }
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.choose_translation_fragment, container, false);
        initializeReferences(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mDownloadTranslationsTransaction = new DownloadTranslationsTransaction(mHandler, mainActivity.getTcpService());
    }

    private void initializeReferences(View view) {
        mainActivity = ((MainActivity)getActivity());
        getTranslationsButton = view.findViewById(R.id.getTranslationsButton);
        mHandler = new ChooseTranslationHandler();
        getTranslationsButton.setOnClickListener((View v) ->{
            getTranslationsButton.setEnabled(false);
            Executors.newSingleThreadExecutor().execute(mDownloadTranslationsTransaction);
        });
        RecyclerView recyclerView  = view.findViewById(R.id.translationsRecyclerView);
        initializeRecyclerView(recyclerView);
    }

    private void initializeRecyclerView(RecyclerView recyclerView){
        mTranslationsViewAdapter = new RecyclerViewAdapter(mainActivity, mHandler);
        recyclerView.setAdapter(mTranslationsViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
    }
}
