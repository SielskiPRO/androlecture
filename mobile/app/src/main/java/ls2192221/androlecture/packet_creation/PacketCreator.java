package ls2192221.androlecture.packet_creation;

import android.util.Log;

import ls2192221.androlecture.protocol.Command;

public class PacketCreator{

    private static String TAG = "PacketCreator";

    public static byte[] createPacket(Command command, byte[] payload){

        long payloadLength = 0;
        if (payload != null){
            payloadLength = payload.length;
        }


        byte[] packet = new byte[(int)(payloadLength + 9)];

        packet[0] = (byte)(payloadLength & 0xFF);
        packet[1] = (byte)((payloadLength >> 8) & 0xFF);
        packet[2] = (byte)((payloadLength >> 16) & 0xFF);
        packet[3] = (byte)((payloadLength >> 24) & 0xFF);
        packet[4] = (byte)((payloadLength >> 32) & 0xFF);
        packet[5] = (byte)((payloadLength >> 40) & 0xFF);
        packet[6] = (byte)((payloadLength >> 48) & 0xFF);
        packet[7] = (byte)((payloadLength >> 56) & 0xFF);

        packet[8] = command.getValue();

        if (payload != null){
            for (int i = 0; i < payloadLength; ++i){
                packet[9 + i] = payload[i];
            }
        }

        Log.d(TAG, "Created packet has size of: " + packet.length);

        return packet;
    }
}
