package ls2192221.androlecture.transactions;

import android.os.Handler;
import android.util.Log;

import ls2192221.androlecture.enums.SamplingFrequency;
import ls2192221.androlecture.fragments.ChooseTranslationFragment;
import ls2192221.androlecture.helper_classes.Translation;
import ls2192221.androlecture.helper_classes.Utils;
import ls2192221.androlecture.packet_creation.PacketCreator;
import ls2192221.androlecture.protocol.Command;
import ls2192221.androlecture.protocol.ProtocolSpecification;
import ls2192221.androlecture.services.TCPService;

public class RequestTranslationTransaction extends RunnableTransaction {

    public static final String TAG = "RequestTransaction";
    private Translation translation;

    public RequestTranslationTransaction(Handler handler, TCPService tcpService) {
        super(handler, tcpService);
    }

    public void setTranslation(Translation translation) {
        this.translation = translation;
    }

    @Override
    protected void startTransaction() {

        if (!Utils.isSamplingRateSupportedForPlaying(SamplingFrequency.valueFromString(translation.getSamplingRate()).toFrequency())) {
            Utils.sendMessageToHandler(mHandler, ChooseTranslationFragment.TRANSLATION_SAMPLING_RATE_NOT_SUPPORTED,
                    "Translation sampling rate not supported");
            return;
        }

        byte payload[] = new byte[ProtocolSpecification.TRANSLATION_ID_SIZE];


        int translationId = translation.getId();
        Log.d(TAG, "Requested translation ID: " + translationId);
        payload[0] = (byte)(translationId & 0xFF);
        payload[1] = (byte)((translationId & 0xFF) >> 8);
        payload[2] = (byte)((translationId & 0xFF) >> 16);
        payload[3] = (byte)((translationId & 0xFF) >> 24);

        boolean isSended = mTcpService.sendBytes(PacketCreator.createPacket(Command.REQUEST_TRANSLATION, payload));

        if (!isSended) {
            Utils.sendMessageToHandler(mHandler, ChooseTranslationFragment.COMMUNICATION_ERROR, "Request could not be send!");
            return;
        }

        byte[] response = mTcpService.receiveBytes();

        if (response != null) {
            Log.d(TAG, "Response is not null!");
            if (response[8] == Command.RESPONSE_SUCCESS.getValue()) {
                Log.d(TAG, "Translation Exists!");
                Utils.sendMessageToHandler(mHandler, ChooseTranslationFragment.TRANSLATION_AVAILABLE, translation);
                return;
            } else if (response[8] == Command.RESPONSE_FAILURE.getValue()) {
                Log.d(TAG, "Translation does not Exists!");
                Utils.sendMessageToHandler(mHandler, ChooseTranslationFragment.TRANSLATION_NOT_AVAILABLE, "Translation is no longer available!");
                return;
            }
        }

        Utils.sendMessageToHandler(mHandler, ChooseTranslationFragment.COMMUNICATION_ERROR, "Did not get response from the server!");
    }
}
