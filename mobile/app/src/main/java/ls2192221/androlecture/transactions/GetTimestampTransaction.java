package ls2192221.androlecture.transactions;

import ls2192221.androlecture.packet_creation.PacketCreator;
import ls2192221.androlecture.protocol.Command;
import ls2192221.androlecture.services.TCPService;

public class GetTimestampTransaction extends CallableTransaction<Long> {

    public GetTimestampTransaction(TCPService tcpService) {
        super(tcpService);
    }

    @Override
    public Long startTransaction() {
        boolean isSended = mTcpService.sendBytes(
                PacketCreator.createPacket(Command.GET_TIMESTAMP, null));

        if (!isSended) {
            throw new RuntimeException("No send");
        }

        byte[] response = mTcpService.receiveBytes();

        if (response == null) {
            throw new RuntimeException("No response");
        }

        long timestamp = 0;
        timestamp |= ((long) response[9] & 0xFF);
        timestamp |= ((long) response[10] & 0xFF) << 8;
        timestamp |= ((long) response[11] & 0xFF) << 16;
        timestamp |= ((long) response[12] & 0xFF) << 24;
        timestamp |= ((long) response[13] & 0xFF) << 32;
        timestamp |= ((long) response[14] & 0xFF) << 40;
        timestamp |= ((long) response[15] & 0xFF) << 48;
        timestamp |= ((long) response[16] & 0xFF) << 56;
        return timestamp;
    }
}
