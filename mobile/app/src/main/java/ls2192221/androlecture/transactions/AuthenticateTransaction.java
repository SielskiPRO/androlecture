package ls2192221.androlecture.transactions;

import android.os.Handler;

import ls2192221.androlecture.fragments.AuthenticateTranslatorFragment;
import ls2192221.androlecture.helper_classes.Utils;
import ls2192221.androlecture.packet_creation.PacketCreator;
import ls2192221.androlecture.protocol.Command;
import ls2192221.androlecture.services.TCPService;

public class AuthenticateTransaction extends RunnableTransaction {

    String password;

    public AuthenticateTransaction(Handler handler, TCPService tcpService) {
        super(handler, tcpService);
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @Override
    protected void startTransaction() {
        if (password == null || "".equals(password)) {
            Utils.sendMessageToHandler(mHandler, AuthenticateTranslatorFragment.AUTHENTICATION_FAILURE,
                    "Please provide password!");
            return;
        }

        byte[] payload = password.getBytes();

        boolean isSended = mTcpService.sendBytes(
                PacketCreator.createPacket(Command.AUTHENTICATE_REQUEST, payload)
        );

        if (!isSended){
            Utils.sendMessageToHandler(mHandler, AuthenticateTranslatorFragment.COMMUNICATION_ERROR,
                    "Could not send request to the server!");
            return;
        }

        byte[] response = mTcpService.receiveBytes();

        if (response != null) {
            if (response[8] == Command.RESPONSE_SUCCESS.getValue()){
                Utils.sendMessageToHandler(mHandler, AuthenticateTranslatorFragment.AUTHENTICATION_SUCCESSFUL,
                        null);
                return;
            }
            Utils.sendMessageToHandler(mHandler, AuthenticateTranslatorFragment.AUTHENTICATION_FAILURE,
                    "Invalid password!");
            return;
        }
        Utils.sendMessageToHandler(mHandler, AuthenticateTranslatorFragment.COMMUNICATION_ERROR,
                "Did not receive response from the server!");
        return;
    }
}
