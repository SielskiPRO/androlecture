package ls2192221.androlecture.transactions;

import java.util.concurrent.Callable;

import ls2192221.androlecture.services.TCPService;

public abstract class CallableTransaction<T> extends Transaction implements Callable<T> {

    public CallableTransaction(TCPService tcpService) {
        super(tcpService);
    }

    public abstract T startTransaction();

    @Override
    public T call() throws Exception {
        return startTransaction();
    }
}
