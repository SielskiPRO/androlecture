package ls2192221.androlecture.transactions;

import android.os.Handler;

import java.util.regex.Pattern;

import ls2192221.androlecture.constants.Constants;

import ls2192221.androlecture.enums.Codec;
import ls2192221.androlecture.enums.SamplingFrequency;
import ls2192221.androlecture.enums.VoicePacketLength;
import ls2192221.androlecture.fragments.AddTranslationFragment;
import ls2192221.androlecture.helper_classes.Translation;
import ls2192221.androlecture.helper_classes.Utils;
import ls2192221.androlecture.packet_creation.PacketCreator;
import ls2192221.androlecture.protocol.Command;
import ls2192221.androlecture.protocol.ProtocolSpecification;
import ls2192221.androlecture.services.TCPService;

public class AddTranslationTransaction extends RunnableTransaction {

    public static final String TAG = "AddTranslationTrans";
    Pattern zeroAtTheBeginning = Pattern.compile("^0+");

    private String language;
    private String description;
    private String codec;
    private String samplingRate;
    private String voicePacketLength;
    private int timeToSendStatistics;

    public AddTranslationTransaction(Handler handler, TCPService tcpService) {
        super(handler, tcpService);
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCodec(String codec) {
        this.codec = codec;
    }

    public void setSamplingRate(String samplingRate) {
        this.samplingRate = samplingRate;
    }

    public void setVoicePacketLength(String voicePacketLength) {
        this.voicePacketLength = voicePacketLength;
    }

    public void setTimeToSendStatistics(int timeToSendStatistics) {
        this.timeToSendStatistics = timeToSendStatistics;
    }

    @Override
    protected void startTransaction() {

        if (language.length() == 0){
            Utils.sendMessageToHandler(mHandler, AddTranslationFragment.TRANSLATION_REJECTED,
                    "Language must contain at least one character!");
            return;
        }
        if (language.length() > 255 || description.length() > 255){
            Utils.sendMessageToHandler(mHandler, AddTranslationFragment.TRANSLATION_REJECTED,
                    "Language and description can contain at most 255 characters!");
            return;
        }


        if (String.valueOf(timeToSendStatistics).length() == 0 && Constants.getInstance().isCollectStatistics()) {
            Utils.sendMessageToHandler(mHandler, AddTranslationFragment.TRANSLATION_REJECTED,
                    "Time can not be empty!");
        }

        if (zeroAtTheBeginning.matcher(String.valueOf(timeToSendStatistics)).find() && Constants.getInstance().isCollectStatistics()) {
            Utils.sendMessageToHandler(mHandler, AddTranslationFragment.TRANSLATION_REJECTED,
                    "Time cannot start with 0 digit!");
            return;
        }

        if (!Utils.isSamplingRateSupportedForRecording(SamplingFrequency.valueFromString(samplingRate).toFrequency())) {
            Utils.sendMessageToHandler(mHandler, AddTranslationFragment.TRANSLATION_REJECTED,
                    "Choosen sampling rate is not supported!");
            return;
        }

        StringBuilder builder = new StringBuilder();
        builder.append(language);
        builder.append(description);
        String joined = builder.toString();

        int languageLength = language.length();
        int descriptionLength = description.length();

        byte[] payload = new byte[ProtocolSpecification.TRANSLATION_LENGTH
                + joined.getBytes().length
                + ProtocolSpecification.CODEC_SIZE
                + ProtocolSpecification.SAMPLING_RATE_SIZE
                + ProtocolSpecification.VOICE_PACKET_LENGTH
                + ProtocolSpecification.TIME_TO_SEND_STATISTICS_SIZE];

        int i = 0;

        payload[i++] = (byte)(languageLength & 0xFF);
        payload[i++] = (byte)(descriptionLength & 0xFF);

        for (byte b : joined.getBytes())
        {
            payload[i++] = b;
        }

        int codecNum = Codec.valueFromString(codec).getValue();
        payload[i++] = (byte)(codecNum & 0xFF);

        int samplingRateNum = SamplingFrequency.valueFromString(samplingRate).getValue();
        payload[i++] = (byte)(samplingRateNum & 0xFF);

        int voicePacketLengthNum = VoicePacketLength.valueFromString(voicePacketLength).getValue();
        payload[i++] = (byte)(voicePacketLengthNum & 0xFF);

        payload[i++] = (byte)((timeToSendStatistics) & 0xFF);
        payload[i++] = (byte)((timeToSendStatistics >> 8) & 0xFF);
        payload[i++] = (byte)((timeToSendStatistics >> 16) & 0xFF);
        payload[i] = (byte)((timeToSendStatistics >> 24) & 0xFF);
;
        boolean isSended = mTcpService.sendBytes(
                PacketCreator.createPacket(Command.ADD_TRANSLATION, payload
                ));

        if(!isSended){
            Utils.sendMessageToHandler(mHandler, AddTranslationFragment.COMMUNICATION_ERROR,
                    "Could not send translation to the server!");
            return;
        }

        byte[] response = mTcpService.receiveBytes();
        if (response == null){
            Utils.sendMessageToHandler(mHandler, AddTranslationFragment.COMMUNICATION_ERROR,
                    "Did not received confirmation from the server!");
            return;
        }
        if (response[8] == Command.RESPONSE_FAILURE.getValue()){
            Utils.sendMessageToHandler(mHandler, AddTranslationFragment.TRANSLATION_REJECTED,
                    "Translation has been rejected by server!");
        } else if (response[8] == Command.RESPONSE_SUCCESS.getValue())
        {
            int id = 0;
            id |= response[9] & 0xFF;
            id |= (response[10] & 0xFF) << 8;
            id |= (response[11] & 0xFF) << 16;
            id |= (response[12] & 0xFF) << 24;

            int destinationUdpPort = 0;
            destinationUdpPort |= response[13] & 0xFF;
            destinationUdpPort |= (response[14] & 0xFF) << 8;

            Utils.sendMessageToHandler(mHandler, AddTranslationFragment.TRANSLATION_ADDED,
                    new Translation(id, destinationUdpPort, language, description, codec,
                            samplingRate, voicePacketLength,timeToSendStatistics));
        }
    }
}
