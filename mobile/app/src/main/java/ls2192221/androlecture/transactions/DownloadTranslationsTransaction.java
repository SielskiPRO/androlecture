package ls2192221.androlecture.transactions;

import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;

import ls2192221.androlecture.fragments.ChooseTranslationFragment;
import ls2192221.androlecture.helper_classes.Translation;
import ls2192221.androlecture.helper_classes.Utils;
import ls2192221.androlecture.packet_creation.PacketCreator;
import ls2192221.androlecture.protocol.Command;
import ls2192221.androlecture.services.TCPService;

public class DownloadTranslationsTransaction extends RunnableTransaction {

    public static final String TAG = "DownloadTranslations";

    public DownloadTranslationsTransaction(Handler handler, TCPService tcpService) {
        super(handler, tcpService);
    }

    private int getNumOfTranslations(byte payload[]){


        Log.d(TAG, "Received response!");
        Log.d(TAG, "Response size : " + payload.length);
        int numOfTranslations = 0;

        numOfTranslations |= payload[9] & 0xFF;
        numOfTranslations |= (payload[10]  & 0xFF) << 8;
        numOfTranslations |= (payload[11] & 0xFF) << 16;
        numOfTranslations |= (payload[12] & 0xFF) << 24;

        return numOfTranslations;
    }

    private ArrayList<Translation> getTranslations(int numOfTranslations)
    {
        ArrayList<Translation> translations = new ArrayList<>();
        byte response[];

        for (int i = 0; i < numOfTranslations; ++i){
            Log.d(TAG, "Waiting for packet for 1 translation");
            response = mTcpService.receiveBytes();

            if (response == null){
                Utils.sendMessageToHandler(mHandler,
                        ChooseTranslationFragment.TRANSLATIONS_DOWNLOAD_FAILURE,
                        "Error while downloading translations from the server!");
                return null;
            }

            int position = 9;
            int id = 0;
            id |= response[position++] & 0xFF;
            id |= (response[position++] & 0xFF) << 8;
            id |= (response[position++] & 0xFF) << 16;
            id |= (response[position++] & 0xFF) << 24;

            int languageLength = (response[position++] & 0xFF);
            int descriptionLength = (response[position++] & 0xFF);

            Log.d(TAG, "Language length : " + languageLength);
            Log.d(TAG, "Description length : " + descriptionLength);

            String language = new String(response, position, languageLength);
            position += languageLength;
            String description = new String(response, position, descriptionLength);
            position += descriptionLength;

            int codec = 0;
            codec |= response[position++] & 0xFF;

            int samplingRate = 0;
            samplingRate |= response[position++] & 0xFF;

            int voicePacketLength = 0;
            voicePacketLength |= response[position++] & 0xFF;

            int timeToSendStatistics = 0;
            timeToSendStatistics |= response[position++] & 0xFF;
            timeToSendStatistics |= (response[position++] & 0xFF) << 8;
            timeToSendStatistics |= (response[position++] & 0xFF) << 16;
            timeToSendStatistics |= (response[position++] & 0xFF) << 24;

            long decimalIpGroupAddress = 0;
            decimalIpGroupAddress |= response[position++] & 0xFF;
            decimalIpGroupAddress |= (response[position++] & 0xFF) << 8;
            decimalIpGroupAddress |= (response[position++] & 0xFF) << 16;
            decimalIpGroupAddress |= (response[position++] & 0xFF) << 24;

            int receivePort = 0;
            receivePort |= response[position++] & 0xFF;
            receivePort |= (response[position] & 0xFF) << 8;


            translations.add(new Translation(
                    id, receivePort, Utils.decimalIpToString(decimalIpGroupAddress), language,
                    description, codec, samplingRate, voicePacketLength, timeToSendStatistics
            ));
        }

        return translations;
    }

    @Override
    protected void startTransaction() {

        Log.d(TAG, "Before sending request for translations!");
        boolean isSended = mTcpService.sendBytes(PacketCreator.createPacket(Command.GET_TRANSLATIONS, null));
        Log.d(TAG, "after sending request for translations!");
        if (!isSended){
            Utils.sendMessageToHandler(mHandler,
                    ChooseTranslationFragment.TRANSLATIONS_DOWNLOAD_FAILURE,
                    "Could not send a request!");
            return;
        }

        Log.d(TAG, "Before receiving response !");
        byte[] response = mTcpService.receiveBytes();
        if (response == null){
            Log.d(TAG, "Did not received response");
            Utils.sendMessageToHandler(mHandler,
                    ChooseTranslationFragment.TRANSLATIONS_DOWNLOAD_FAILURE,
                    "Could not get response from the server!");
            return;
        }

        int numOfTranslations = getNumOfTranslations(response);

        Log.d(TAG, "Num of translations:" + numOfTranslations);

        ArrayList<Translation> translations = getTranslations(numOfTranslations);

        for (Translation t : translations){
            Log.d(TAG, t.toString());
        }

        Utils.sendMessageToHandler(mHandler,
                ChooseTranslationFragment.TRANSLATIONS_DOWNLOAD_SUCCESS,
                translations);
    }
}
