package ls2192221.androlecture.transactions;

import android.os.Handler;

import ls2192221.androlecture.fragments.ManageTranslationFragment;
import ls2192221.androlecture.helper_classes.Utils;
import ls2192221.androlecture.packet_creation.PacketCreator;
import ls2192221.androlecture.protocol.Command;
import ls2192221.androlecture.services.TCPService;

public class DeleteTranslationTransaction extends RunnableTransaction {

    public DeleteTranslationTransaction(Handler handler, TCPService tcpService) {
        super(handler, tcpService);
    }

    @Override
    protected void startTransaction() {
        boolean isSended = mTcpService.sendBytes(
                PacketCreator.createPacket(Command.DELETE_TRANSLATION, null)
        );

        if (!isSended) {
            Utils.sendMessageToHandler(mHandler, ManageTranslationFragment.COMMUNICATION_ERROR,
                    "Could not send request to the server!");
            return;
        }

        byte[] response = mTcpService.receiveBytes();

        if (response == null) {
            Utils.sendMessageToHandler(mHandler, ManageTranslationFragment.COMMUNICATION_ERROR,
                    "Did not get response from the server!");
            return;
        }

        Utils.sendMessageToHandler(mHandler, ManageTranslationFragment.DELETION_SUCCESSFUL,
                "Translation has been deleted!");
    }
}
