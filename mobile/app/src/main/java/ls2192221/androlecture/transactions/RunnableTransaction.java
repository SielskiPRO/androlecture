package ls2192221.androlecture.transactions;

import android.os.Handler;

import ls2192221.androlecture.services.TCPService;

public abstract class RunnableTransaction extends Transaction implements Runnable{

    protected Handler mHandler;

    RunnableTransaction(Handler handler, TCPService tcpService){
        super(tcpService);
        this.mHandler = handler;
    }

    protected abstract void startTransaction();

    @Override
    public void run() {
        startTransaction();
    }
}
