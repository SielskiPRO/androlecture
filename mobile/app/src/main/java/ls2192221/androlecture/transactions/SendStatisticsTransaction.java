package ls2192221.androlecture.transactions;

import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;

import ls2192221.androlecture.helper_classes.StatisticEntry;
import ls2192221.androlecture.packet_creation.PacketCreator;
import ls2192221.androlecture.protocol.Command;
import ls2192221.androlecture.services.TCPService;

public class SendStatisticsTransaction extends RunnableTransaction {
    public static final String TAG = "StatsTransaction";
    private ArrayList<StatisticEntry> statisticsEntries;

    public SendStatisticsTransaction(Handler handler, TCPService tcpService) {
        super(handler, tcpService);
    }

    public void setStatisticsEntries(ArrayList<StatisticEntry> statisticsEntries) {
        this.statisticsEntries = statisticsEntries;
    }

    @Override
    protected void startTransaction() {
        Log.d(TAG, "Start sending transaction!");
        ArrayList<byte[]> statisticsArray = new ArrayList<>();
        int numOfEntries = statisticsEntries.size();
        int entriesSizeInBytes = 0;
        for (StatisticEntry entry : statisticsEntries) {
            byte entryInBytes [] = entry.toByteArray();
            entriesSizeInBytes += entryInBytes.length;
            statisticsArray.add(entryInBytes);
        }

        byte payload [] = new byte [4 + entriesSizeInBytes];

        int i = 0;

        payload[i++] = (byte)(numOfEntries);
        payload[i++] = (byte)(numOfEntries >> 8);
        payload[i++] = (byte)(numOfEntries >> 16);
        payload[i++] = (byte)(numOfEntries >> 24);

        for (byte[] statisticEntry : statisticsArray) {
            System.arraycopy(statisticEntry, 0, payload, i, statisticEntry.length);
            i += statisticEntry.length;
        }

        Log.d(TAG, "Payload size : " + payload.length);

        boolean isSended = mTcpService.sendBytes(PacketCreator.createPacket(Command.SAVE_STATISTICS, payload));
        if (isSended) {
            Log.d(TAG, "Is sended!");
        }
    }
}
