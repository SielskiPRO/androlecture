package ls2192221.androlecture.constants;

public class Constants {
    private static Constants instance;
    private boolean collectStatistics = false;

    public void enableCollectionStatistics() {
        collectStatistics = true;
    }

    public boolean isCollectStatistics() {
        return collectStatistics;
    }

    private Constants(){}

    public static synchronized Constants getInstance() {
        if (instance == null)
            instance = new Constants();
        return instance;
    }
}
