//
// Created by lukasz on 27.05.19.
//

#ifndef MOBILE_OPUSDECODER_H
#define MOBILE_OPUSDECODER_H

#include <jni.h>

JNIEXPORT jboolean JNICALL Java_ls2192221_androlecture_jniwrappers_OpusDecoder_nativeInitDecoder (JNIEnv *env, jobject obj, jint samplingRate, jint numberOfChannels, jint frameSize);
JNIEXPORT jint JNICALL Java_ls2192221_androlecture_jniwrappers_OpusDecoder_nativeDecodeBytes (JNIEnv *env, jobject obj, jbyteArray in, jshortArray out);
JNIEXPORT jboolean JNICALL Java_ls2192221_androlecture_jniwrappers_OpusDecoder_nativeReleaseDecoder (JNIEnv *env, jobject obj);


#endif //MOBILE_OPUSDECODER_H
