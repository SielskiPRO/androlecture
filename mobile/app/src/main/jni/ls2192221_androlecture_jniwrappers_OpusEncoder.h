//
// Created by lukasz on 27.05.19.
//

#ifndef MOBILE_OPUSENCODER_H
#define MOBILE_OPUSENCODER_H

#include <jni.h>

JNIEXPORT jboolean JNICALL Java_ls2192221_androlecture_jniwrappers_OpusEncoder_nativeInitEncoder (JNIEnv *env, jobject obj, jint samplingRate, jint numberOfChannels, jint frameSize);
JNIEXPORT jint JNICALL Java_ls2192221_androlecture_jniwrappers_OpusEncoder_nativeEncodeBytes (JNIEnv *env, jobject obj, jshortArray in, jbyteArray out);
JNIEXPORT jboolean JNICALL Java_ls2192221_androlecture_jniwrappers_OpusEncoder_nativeReleaseEncoder (JNIEnv *env, jobject obj);


#endif //MOBILE_OPUSENCODER_H
